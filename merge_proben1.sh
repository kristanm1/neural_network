#!/bin/bash

src="$1"
dst="$2"
model="$3"

dbs=("cancer1" "card1" "diabetes1" "gene1" "heart1" "heartc1" "horse1")
methods=('gd' 'adam' 'cgm' 'bfgs')
layers=(3 4 5)

for method in ${methods[@]}
do
    for layer in ${layers[@]}
    do
        for db in ${dbs[@]}
        do
            path="$method/$layer/$db/"
            ps="$src/$path/$model"
            pd="$dst/$path/$model"
            
            cp -r "$ps" "$pd"
        done
    done
done