adam
batch size: 192
consecutive_allowed: 300
alpha: 9.765625e-03
beta1: 8.999023e-01
beta2: 9.987793e-01
epsilon: 2.441406e-04
layers:  8 7 2
tabulated points: 32
