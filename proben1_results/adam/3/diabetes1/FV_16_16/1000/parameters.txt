adam
batch size: 192
consecutive_allowed: 300
alpha: 9.994507e-03
beta1: 8.999939e-01
beta2: 9.989929e-01
epsilon: 1.525879e-05
layers:  8 7 2
tabulated points: 32
