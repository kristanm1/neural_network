adam
batch size: 192
consecutive_allowed: 300
alpha: 7.812500e-03
beta1: 8.984375e-01
beta2: 9.960938e-01
epsilon: 3.906250e-03
layers:  8 7 2
tabulated points: 32
