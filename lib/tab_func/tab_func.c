#include "tab_func.h"


struct tab_func *alloc_tab_func(int size, double low, double high, double (*func) (double)) {
    struct tab_func *tab = (struct tab_func*) malloc(sizeof(struct tab_func));
    tab->size = size;
    tab->x = (double*) malloc(sizeof(double)*(size << 1));
    tab->y = tab->x + size;
    double dx = (high - low)/(size - 1);
    double x = low;
    for(int i = 0; i < size; i++) {
        tab->x[i] = (x + (i*dx + low))/2.0;
        tab->y[i] = func(tab->x[i]);
        x += dx;
    }
    return tab;
}

void dealloc_tab_func(struct tab_func *tab) {
    free(tab->x);
    free(tab);
}

void print_inf_tab_func(struct tab_func *tab) {
    printf("tab_func:\n");
    printf(" size: %d [%g, %g]\n", tab->size, tab->x[0], tab->y[-1]);
}

void print_tab_func(struct tab_func *tab) {
    printf("     x:     |    y:  \n");
    for(int i = 0; i < tab->size; i++) {
        printf("%11.5f | %11.5f\n", tab->x[i], tab->y[i]); 
    }
}

int cls_int_tab_func(double x, struct tab_func *tab) {
    double diff;
    diff = x - tab->x[0];
    if(-TAB_FUNC_EPS < diff && diff < TAB_FUNC_EPS) {
        return 0;
    }
    diff = x - tab->y[-1];
    if(-TAB_FUNC_EPS < diff && diff < TAB_FUNC_EPS) {
        return tab->size - 1;
    }
    int low = 0;
    int mid = 1;
    int high = tab->size;
    while(low + 1 < high) {
        mid = (low + high) >> 1;
        if(tab->x[mid] < x) {
            low = mid;
        } else {
            high = mid;
        }
    }
    return low;
}
