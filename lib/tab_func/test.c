#include "tab_func.h"


double lin(double x) {
    return -4.3*x + 0.24245;
}

double poly(double x) {
    return 3.0*x*x-4.3*x + 0.24245;
}

void test_tab_func(double(*f)(double)) {

    struct tab_func *tab = alloc_tab_func(11, 0.0, 10.0, f);

    print_inf_tab_func(tab);
    print_tab_func(tab);

    double x = 3.7;
    int i = cls_int_tab_func(x, tab);
    printf("%f -> %f | %f -> %f\n", tab->x[i], tab->y[i], x, f(x));

    dealloc_tab_func(tab);

}


int main(int argc, char *argv[]) {

    test_tab_func(poly);

    return 0;
}