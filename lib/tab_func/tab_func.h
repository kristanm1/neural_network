#ifndef _TAB_FUNC
#define _TAB_FUNC

    #include "../utils/utils.h"

    #define TAB_FUNC_EPS 1e-15


    // tabulated function structure
    struct tab_func {
        int size;       // number of points in tabel
        double *x;      // x coordinares of points
        double *y;      // y coordinares of points
    };

    struct tab_func *alloc_tab_func(int size, double low, double high, double (*func) (double));
    void dealloc_tab_func(struct tab_func *tab);
    void print_inf_tab_func(struct tab_func *tab);
    void print_tab_func(struct tab_func *tab);

    // return index of point with nearest smaller x coordinate compared to arg. x
    // cls ... closest smaller
    int cls_int_tab_func(double x, struct tab_func *tab);


#endif