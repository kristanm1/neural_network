#include <string.h>
#include "optimizer.h"
#include "../database/database.h"
#include "../mult_perc/mult_perc.h"
#include "../PROBEN_loader/proben_loader.h"


// test functions and derivatives
// https://en.wikipedia.org/wiki/Test_functions_for_optimization
// sphere
double sphere(int n, double x[]) {
    double y = 0.0;
    for(int i = 0; i < n; i++) {
        y += x[i] * x[i];
    }
    return y;
}

void grad_sphere(int n, double x[], double grad[]) {
    for(int i = 0; i < n; i++) {
        grad[i] = 2.0 * x[i];
    }
}

// Rosenbrock function
double rosenbrock(int n, double x[]) {
    double y = 0.0;
    for(int i = 0; i < n - 1; i++) {
        double t1 = x[i + 1] - x[i] * x[i];
        double t2 = 1.0 - x[i];
        y += 100.0 * t1*t1 + t2*t2;
    }
    return y;
}

void grad_rosenbrock(int n, double x[], double grad[]) {
    grad[0] = 200*(x[1]-x[0]*x[0])*(-2*x[0]) - 2*(1-x[0]);
    for(int i = 1; i < n-1; i++) {
        grad[i] = 200*(x[i] - x[i-1]*x[i-1]) + 200*(x[i+1] - x[i]*x[i])*(-2*x[i]) - 2*(1 - x[i]);
    }
    grad[n - 1] = 200*(x[n-1] - x[n-2]*x[n-2]);
}

double line_search(double (*f)(int n, double x[]), int n, double x[], double dir[], double g0[], double c1, double alpha, double amin) {
    double derphi0 = 0.0;
    for(int i = 0; i < n; i++) {
        derphi0 += dir[i] * g0[i];
    }
    if(derphi0 < 0.0) {
        double phi0 = f(n, x);
        double x_a[n];
        while(alpha > amin) {
            for(int i = 0; i < n; i++) {
                x_a[i] = x[i] + alpha*dir[i];
            }
            double phi_a = f(n, x_a);
            if(phi_a <= phi0 + c1*alpha*derphi0) {
                return alpha;
            }
            alpha *= 0.7;
        }
    }
    return 0.0;
}

void gd(
    int n, double x0[], double (*f)(int n, double x[]), void (*df)(int n, double x[], double grad[]),
    double alpha, int consecutive_allowed, double x_min[]
) {
    double x[n], x_opt[n], grad[n];
    for(int i = 0; i < n; i++) {
        x[i] = x0[i];
        x_opt[i] = x0[i];
    }

    double min_fval = f(n, x);
    int consecutive = 0;
    int iteration = 0;
    int running = 1;

    while(running) {

        // stopping criteria
        if(consecutive >= consecutive_allowed) {
            running = 0;
            break;
        }

        // update
        df(n, x, grad);
        for(int i = 0; i < n; i++) {
            x[i] -= alpha * grad[i];
        }
        iteration++;

        // evaluate
        double fval = f(n, x);
        if(fval < min_fval) {
            for(int i = 0; i < n; i++) {
                x_opt[i] = x[i];
            }
            min_fval = fval;
            consecutive = 0;
        } else {
            consecutive++;
        }

    }
    printf(" gd  iteration: %d\n", iteration);
    for(int i = 0; i < n; i++) {
        x_min[i] = x_opt[i];
    }
}

void adam(
    int n, double x0[], double (*f)(int n, double x[]), void (*df)(int n, double x[], double grad[]),
    double alpha, double beta1, double beta2, double epsilon, int consecutive_allowed, double x_min[]
) {
    double x[n], x_opt[n], grad[n], m[n], v[n], b1 = beta1, b2 = beta2;
    for(int i = 0; i < n; i++) {
        m[i] = 0.0;
        v[i] = 0.0;
        x[i] = x0[i];
        x_opt[i] = x0[i];
    }

    double min_fval = f(n, x);
    int consecutive = 0;
    int iteration = 0;
    int running = 1;

    while(running) {

        // stopping criteria
        if(consecutive >= consecutive_allowed) {
            running = 0;
            break;
        }

        // update
        df(n, x, grad);
        double a = alpha * sqrt(1.0 - b2) / (1.0 - b1);
        for(int i = 0; i < n; i++) {
            m[i] = beta1 * m[i] + (1.0 - beta1) * grad[i];
            v[i] = beta2 * v[i] + (1.0 - beta2) * grad[i] * grad[i];
            x[i] -= a * m[i] / (sqrt(v[i]) + epsilon);
        }
        b1 *= beta1;
        b2 *= beta2;
        iteration++;

        // evaluate
        double fval = f(n, x);
        if(fval < min_fval) {
            for(int i = 0; i < n; i++) {
                x_opt[i] = x[i];
            }
            min_fval = fval;
            consecutive = 0;
        } else {
            consecutive++;
        }
        
    }
    printf("adam iteration: %d\n", iteration);
    for(int i = 0; i < n; i++) {
        x_min[i] = x_opt[i];
    }
}

void cgm(
    int n, double x0[], double (*f)(int n, double x[]), void (*df)(int n, double x[], double grad[]), int consecutive_allowed, double x_min[]
) {
    double x[n], x_opt[n], grad[n], s[n], t, normo, norm, beta;
    int s_upd = 0;

    for(int i = 0; i < n; i++) {
        x[i] = x0[i];
        x_opt[i] = x0[i];
    }

    double min_fval = f(n, x);
    int consecutive = 0;
    int iteration = 0;
    int running = 1;

    while(running) {

        // stopping criteria
        if(consecutive >= consecutive_allowed) {
            running = 0;
            break;
        }

        // update
        df(n, x, grad);
        if(s_upd == 0) {
            // reset conjugate direction
            normo = 0.0;
            for(int i = 0; i < n; i++) {
                s[i] = -grad[i];
                normo += grad[i] * grad[i];
            }
            if(normo == 0.0) {
                s_upd = 0;
            } else {
                t = line_search(f, n, x, s, grad, 1e-4, 1.0, 0.0);
                if(t == 0.0) {
                    s_upd = 0;
                } else {
                    for(int i = 0; i < n; i++) {
                        x[i] += t * s[i];
                    }
                    s_upd = 1;
                }
            }
        } else {
            // update conjugate direction
            norm = 0.0;
            for(int i = 0; i < n; i++) {
                norm += grad[i] * grad[i];
            }
            if(norm == 0.0) {
                s_upd = 0;
            } else {
                beta = norm / normo;
                for(int i = 0; i < n; i++) {
                    s[i] = beta * s[i] - grad[i];
                }
                t = line_search(f, n, x, s, grad, 1e-4, 1.0, 0.0);
                if(t == 0.0) {
                    s_upd = 0;
                } else {
                    for(int i = 0; i < n; i++) {
                        x[i] += t * s[i];
                    }
                    s_upd++;
                    normo = norm;
                }
            }
        }
        iteration++;

        // evaluate
        double fval = f(n, x);
        if(fval < min_fval) {
            for(int i = 0; i < n; i++) {
                x_opt[i] = x[i];
            }
            min_fval = fval;
            consecutive = 0;
        } else {
            consecutive++;
        }
        
    }
    
    printf(" cgm iteration: %d\n", iteration);
    for(int i = 0; i < n; i++) {
        x_min[i] = x_opt[i];
    }
}

void bfgs(
    int n, double x0[], double (*f)(int n, double x[]), void (*df)(int n, double x[], double grad[]), int consecutive_allowed, double x_min[]
) {
    double x[n], x_opt[n], grad[n], gradn[n], s[n], y[n], t, sy;
    struct bfgs_mat *B = alloc_bfgs_mat(n), *Bn = alloc_bfgs_mat(n);
    int B_upd = 0;
    for(int i = 0; i < n; i++) {
        x[i] = x0[i];
        x_opt[i] = x0[i];
    }

    double min_fval = f(n, x);
    int consecutive = 0;
    int iteration = 0;
    int running = 1;

    while(running) {
        
        // update
        if(B_upd == 0) {
            // reset Hessian apx.
            df(n, x, grad);
            set_identity_bfgs_mat(B);
            for(int i = 0; i < n; i++) {
                s[i] = -grad[i];
            }
            t = line_search(f, n, x, s, grad, 1e-4, 1.0, 0.0);
            if(t == 0.0) {
                B_upd = 0;
            } else {
                for(int i = 0; i < n; i++) {
                    s[i] *= t;
                    x[i] += s[i];
                }
                B_upd = 1;
            }
        } else {
            // update Hessian apx.
            df(n, x, gradn);
            sy = 0.0;
            for(int i = 0; i < n; i++) {
                y[i] = gradn[i] - grad[i];
                sy += s[i] * y[i];
            }
            if(sy <= 0.0) {
                B_upd = 0;
            } else {
                update_bfgs_mat(B, s, y, sy, Bn);
                B_upd++;
                for(int i = 0; i < n; i++) {
                    s[i] = 0.0;
                    for(int j = 0; j < n; j++) {
                        s[i] -= Bn->p_row[i][j] * gradn[j];
                    }
                }
                struct bfgs_mat *temp = B;
                B = Bn;
                Bn = temp;
                t = line_search(f, n, x, s, grad, 1e-4, 1.0, 0.0);
                if(t == 0.0) {
                    B_upd = 0;
                } else {
                    for(int i = 0; i < n; i++) {
                        s[i] *= t;
                        x[i] += s[i];
                        grad[i] = gradn[i];
                    }
                }
            }
        }
        iteration++;

        // evaluate
        double fval = f(n, x);
        if(fval < min_fval) {
            for(int i = 0; i < n; i++) {
                x_opt[i] = x[i];
            }
            min_fval = fval;
            consecutive = 0;
        } else {
            consecutive++;
        }
        // stopping criteria
        if(consecutive >= consecutive_allowed) {
            running = 0;
            break;
        }
    }
    dealloc_bfgs_mat(B);
    dealloc_bfgs_mat(Bn);

    printf("bfgs iteration: %d\n", iteration);
    for(int i = 0; i < n; i++) {
        x_min[i] = x_opt[i];
    }
}

void test_optimization_methods() {

    int n = 4;
    double x0[n], x_gd[n], x_adam[n], x_cgm[n], x_bfgs[n];
    srand(clock());
    printf(" x0 :");
    for(int i = 0; i < n; i++) {
        x0[i] = 3.0*(((double) rand()) / RAND_MAX) - 1.5;
        x0[0] = -0.1;
        x0[1] = 2.5;
        x0[2] = 0.5;
        x0[3] = -0.1;
        printf(" %+.8f", x0[i]);
    }
    printf("\n\n");
    
    gd(n, x0, rosenbrock, grad_rosenbrock, 0.00075, 100, x_gd);
    printf(" gd : ");
    for(int i = 0; i < n; i++) {
        printf("%+.8f ", x_gd[i]);
    }
    printf("\n");

    adam(n, x0, rosenbrock, grad_rosenbrock, 0.005, 0.9, 0.999, 1e-8, 100, x_adam);
    printf("adam: ");
    for(int i = 0; i < n; i++) {
        printf("%+.8f ", x_adam[i]);
    }
    printf("\n");

    cgm(n, x0, rosenbrock, grad_rosenbrock, 10, x_cgm);
    printf(" cgm: ");
    for(int i = 0; i < n; i++) {
        printf("%+.8f ", x_cgm[i]);
    }
    printf("\n");

    bfgs(n, x0, rosenbrock, grad_rosenbrock, 10, x_bfgs);
    printf("bfgs: ");
    for(int i = 0; i < n; i++) {
        printf("%+.8f ", x_bfgs[i]);
    }
    printf("\n");

}

void test_backtracing() {

    proben1_number_of_layers = 5;
    proben1_neurons_per_layer = (int*) proben1_5_layer;
    set_sigmoid();

    int db = 0, seed = 1000;
    srand(seed);

    char *db_name = proben1_db_names[db];
    char *db_path = proben1_db_paths[db];
    double c1 = proben1_b_c1;
    double alpha = 10*proben1_gd_learning_rates[db];
    double amin = proben1_b_amin;

    int batch_size = proben1_batch_sizes[db];

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + db*number_of_layers;

    struct database *train, *valid, *test;
    load_proben_database(db_path, &train, &valid, &test);

    struct mult_perc *mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    init_rand_mult_perc(mlp, 1.0);
    struct mult_perc *temp_mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    struct activ_holdr *a = alloc_activ_holdr(number_of_layers, neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(number_of_layers, neurons_per_layer);
    struct deriv_holdr *g = alloc_deriv_holdr(number_of_layers, neurons_per_layer);
    struct deriv_holdr *s = alloc_deriv_holdr(number_of_layers, neurons_per_layer);

    printf("db: %s\n", db_name);
    printf("%f %f %f\n", c1, alpha, amin);

    shuffle_database(train);
    set_to_zero_deriv_holdr(g);
    for(int record = 0; record < batch_size; record++) {
        backpropagation_mult_perc(mlp, a, d, get_input_database(train, record), get_output_database(train, record));
        for(int i = 0; i < g->number_of_weights; i++) {
            g->data[i] += d->data[i];
        }
    }
    for(int i = 0; i < g->number_of_weights; i++) {
        s->data[i] = -g->data[i];
    }
    printf("%f %f\n", 0.0, evaluate_ls(mlp, temp_mlp, a, s, 0.0, train, 0, batch_size));

    double t_min = backtracing(mlp, temp_mlp, a, s, g, train, 0, batch_size, c1, alpha, amin);
    printf("%f %f\n", t_min, evaluate_ls(mlp, temp_mlp, a, s, t_min, train, 0, batch_size));

    double td = 0.01;
    double tend = 1.0 + td;
    FILE *q_vals = fopen("../../visualizer/q.txt", "w");
    for(double t = 0.0; t <= tend; t += td) {
        double q = evaluate_ls(mlp, temp_mlp, a, s, t, train, 0, batch_size);
        fprintf(q_vals, "%f %f\n", t, q);
    }
    fprintf(q_vals, "%f %f\n", t_min, evaluate_ls(mlp, temp_mlp, a, s, t_min, train, 0, batch_size));
    fclose(q_vals);


    dealloc_mult_perc(mlp);
    dealloc_mult_perc(temp_mlp);
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_deriv_holdr(g);
    dealloc_deriv_holdr(s);

    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

}

void test_backtracing_mnist() {

    set_sigmoid();

    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(1000);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");

    struct mult_perc *mlp = alloc_mult_perc(mnist_number_of_layers, mnist_neurons_per_layer);
    struct mult_perc *temp_mlp = alloc_mult_perc(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc(mlp, mnist_init_weights);

    
    struct activ_holdr *a = alloc_activ_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    struct deriv_holdr *g = alloc_deriv_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    struct deriv_holdr *s = alloc_deriv_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    set_to_zero_deriv_holdr(g);

    shuffle_database(train);
    for(int record = 0; record < mnist_batch_size; record++) {
        backpropagation_mult_perc(mlp, a, d, get_input_database(train, record), get_output_database(train, record));
        for(int i = 0; i < g->number_of_weights; i++) {
            g->data[i] += d->data[i];
        }
    }
    for(int i = 0; i < s->number_of_weights; i++) {
        s->data[i] = -g->data[i];
    }

    double t_min = backtracing(mlp, temp_mlp, a, s, g, train, mnist_b_c1, mnist_cgm_b_alpha, mnist_b_amin, 0, mnist_batch_size);
    double q_min = evaluate_ls(mlp, temp_mlp, a, s, t_min, train, 0, mnist_batch_size);
    printf("%f: %f\n", 0.0, evaluate_ls(mlp, temp_mlp, a, g, 0.0, train, 0, mnist_batch_size));
    printf("%f: %f\n", t_min, q_min);

    double tend = 1.0, td = 0.01;
    FILE *q_vals = fopen("../../visualizer/q.txt", "w");
    for(double t = 0.0; t <= tend; t += td) {
        double q = evaluate_ls(mlp, temp_mlp, a, s, t, train, 0, mnist_batch_size);
        fprintf(q_vals, "%f %f\n", t, q);
    }
    fprintf(q_vals, "%f %f\n", t_min, q_min);
    fclose(q_vals);
    
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_deriv_holdr(g);
    dealloc_deriv_holdr(s);

    dealloc_mult_perc(mlp);
    dealloc_mult_perc(temp_mlp);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

void optimize_proben1_gd(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);

    srand(seed);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc *mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    init_rand_mult_perc(mlp, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/gd"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    double min_valid_error = proben1_min_valid_error[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    double learning_rate = proben1_gd_learning_rates[db];
    struct optim *opt = alloc_gd_optim(min_valid_error, batch_size, consecutive_allowed, learning_rate);
    if(seed <= 3000) {
        opt->write_to_files = 1;
        opt->path = temp;
        opt->path_offset = p_temp;
    }

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(number_of_layers, neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_activ_holdr(a);

    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

}

void optimize_proben1_adam(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);

    srand(seed);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc *mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    init_rand_mult_perc(mlp, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/adam"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    double min_valid_error = proben1_min_valid_error[db];
    double alpha = proben1_adam_alphas[db];
    double beta1 = proben1_adam_beta1;
    double beta2 = proben1_adam_beta2;
    double epsilon = proben1_adam_epsilon;
    struct optim *opt = alloc_adam_optim(min_valid_error, batch_size, consecutive_allowed, alpha, beta1, beta2, epsilon);
    if(seed <= 3000) {
        opt->write_to_files = 1;
        opt->path = temp;
        opt->path_offset = p_temp;
    }

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(number_of_layers, neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_activ_holdr(a);

    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

}

void optimize_proben1_cgm(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc *mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    srand(seed);
    init_rand_mult_perc(mlp, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/cgm"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    double min_valid_error = proben1_min_valid_error[db];
    double c1 = proben1_b_c1;
    double alpha = proben1_cgm_b_alpha[db];
    double amin = proben1_b_amin;
    struct optim *opt = alloc_cgm_optim(min_valid_error, batch_size, consecutive_allowed, c1, alpha, amin);
    if(seed <= 3000) {
        opt->write_to_files = 1;
        opt->path = temp;
        opt->path_offset = p_temp;
    }

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(number_of_layers, neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_activ_holdr(a);

    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

}

void optimize_proben1_bfgs(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc *mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    srand(seed);
    init_rand_mult_perc(mlp, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/bfgs"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    double min_valid_error = proben1_min_valid_error[db];
    double c1 = proben1_b_c1;
    double alpha = proben1_cgm_b_alpha[db];
    double amin = proben1_b_amin;
    struct optim *opt = alloc_bfgs_optim(min_valid_error, batch_size, consecutive_allowed, c1, alpha, amin);
    if(seed <= 3000) {
        opt->write_to_files = 1;
        opt->path = temp;
        opt->path_offset = p_temp;
    }

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(number_of_layers, neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_activ_holdr(a);

    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

void optimize_mnist_gd(int seed) {
    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(seed);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");

    struct mult_perc *mlp = alloc_mult_perc(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc(mlp, mnist_init_weights);

    char path[] = "../../mnist_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/gd"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", mnist_neurons_per_layer[1]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = mnist_batch_size;
    double min_valid_error = mnist_min_valid_error;
    int consecutive_allowed = mnist_consecutive_allowed;
    double learning_rate = mnist_gd_learning_rate;

    struct optim *opt = alloc_gd_optim(min_valid_error, batch_size, consecutive_allowed, learning_rate);
    opt->write_to_files = 1;
    opt->path = temp;
    opt->path_offset = p_temp;

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(opt->path, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);
    dealloc_activ_holdr(a);
    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
}

void optimize_mnist_adam(int seed) {
    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(seed);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");

    struct mult_perc *mlp = alloc_mult_perc(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc(mlp, mnist_init_weights);

    char path[] = "../../mnist_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/adam"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", mnist_neurons_per_layer[1]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = mnist_batch_size;
    double min_valid_error = mnist_min_valid_error;
    int consecutive_allowed = mnist_consecutive_allowed;
    double alpha = mnist_adam_alpha;
    double beta1 = mnist_adam_beta1;
    double beta2 = mnist_adam_beta2;
    double epsilon = mnist_adam_epsilon;

    struct optim *opt = alloc_adam_optim(min_valid_error, batch_size, consecutive_allowed, alpha, beta1, beta2, epsilon);
    opt->write_to_files = 1;
    opt->path = temp;
    opt->path_offset = p_temp;

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(opt->path, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);
    dealloc_activ_holdr(a);
    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
}

void optimize_mnist_cgm(int seed) {
    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(seed);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");

    struct mult_perc *mlp = alloc_mult_perc(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc(mlp, mnist_init_weights);

    char path[] = "../../mnist_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/cgm"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", mnist_neurons_per_layer[1]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/PV"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = mnist_batch_size;
    double min_valid_error = mnist_min_valid_error;
    int consecutive_allowed = mnist_consecutive_allowed;
    double c1 = mnist_b_c1;
    double alpha = mnist_cgm_b_alpha;
    double amin = mnist_b_amin;

    struct optim *opt = alloc_cgm_optim(min_valid_error, batch_size, consecutive_allowed, c1, alpha, amin);
    opt->write_to_files = 1;
    opt->path = temp;
    opt->path_offset = p_temp;

    opt->optimizer(mlp, train, valid, opt);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", mult_stat[0], mult_stat[1]);
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr *a = alloc_activ_holdr(mnist_number_of_layers, mnist_neurons_per_layer);
    evaluate_mult_perc(mlp, a, test, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(opt->path, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt->iterations, opt->epochs, error, ((double) misses)/test->size, opt->duration, opt->multiplications);
    fclose(result_file);
    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);
    dealloc_activ_holdr(a);
    dealloc_optim(opt);
    dealloc_mult_perc(mlp);
}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

void proben1_args_parser(int argc, char *argv[]) {
    int db = atoi(argv[2]);
    int method = atoi(argv[3]);
    int seed = atoi(argv[4]);
    int layers = atoi(argv[5]);

    if(layers == 3) {
        proben1_number_of_layers = 3;
        proben1_neurons_per_layer = (int*) proben1_3_layer;
    } else if(layers == 4) {
        proben1_number_of_layers = 4;
        proben1_neurons_per_layer = (int*) proben1_4_layer;
    } else if(layers == 5) {
        proben1_number_of_layers = 5;
        proben1_neurons_per_layer = (int*) proben1_5_layer;
    } else {
        return;
    }

    printf("db: %s\n", proben1_db_names[db]);
    printf("seed: %d\n", seed);
    printf("layers: %d\n", layers);

    set_sigmoid();

    if(method == 0) {
        printf("gd\n");
        optimize_proben1_gd(db, seed);
    } else if(method == 1) {
        printf("adam\n");
        optimize_proben1_adam(db, seed);
    } else if(method == 2) {
        printf("cgm\n");
        optimize_proben1_cgm(db, seed);
    } else if(method == 3) {
        printf("bfgs\n");
        optimize_proben1_bfgs(db, seed);
    }
}

void mnist_args_parser(int argc, char *argv[]) {
    int seed = atoi(argv[2]);
    int hidden_neurons = atoi(argv[3]);
    int method = atoi(argv[4]);

    set_sigmoid();
    mnist_neurons_per_layer[1] = hidden_neurons;

    printf("seed: %d\n", seed);
    printf("hidden neurons: %d\n", hidden_neurons);
    printf("method: %d\n", method);

    if(method == 0) {
        printf("gd\n");
        optimize_mnist_gd(seed);
    } else if(method == 1) {
        printf("adam\n");
        optimize_mnist_adam(seed);
    } else if(method == 2) {
        printf("cgm\n");
        optimize_mnist_cgm(seed);
    }
}

void args_parser(int argc, char *argv[]) {
    if(argc >= 1) {
        int db = atoi(argv[1]);

        if(db == 0) {
            if(argc > 5) {
                proben1_args_parser(argc, argv);
            }
        } else if(db == 1) {
            if(argc > 4) {
                mnist_args_parser(argc, argv);
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

char *itoa(int i, char *a) {
    if(a) {
        int len = 0;
        int temp = i;
        while(temp > 0) {
            temp /= 10;
            len++;
        }
        a[len] = '\0';
        temp = i;
        for(int j = len-1; j >= 0; j--) {
            a[j] = temp%10 + '0';
            temp /= 10;
        }
    }
    return a;
}

int main(int argc, char *argv[]) {

    //test_optimization_methods();

    //test_backtracing();
    //test_backtracing_mnist();

    /*
    proben1_number_of_layers = 5;
    proben1_neurons_per_layer = (int*) proben1_5_layer;
    set_sigmoid();
    int db = 0, seed = 1000;
    //optimize_proben1_gd(db, seed);
    //optimize_proben1_adam(db, seed);
    optimize_proben1_cgm(db, seed);
    //optimize_proben1_bfgs(db, seed);
    */

    /*
    set_sigmoid();
    int seed = 1000;
    mnist_neurons_per_layer[1] = 30;
    //optimize_mnist_gd(seed);
    //optimize_mnist_adam(seed);
    optimize_mnist_cgm(seed);
    */

    // ./test 0 db method seed layers
    // ./test 1 seed hidden_layers method
    args_parser(argc, argv);

    return 0;
}