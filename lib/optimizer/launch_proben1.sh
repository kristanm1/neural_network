#!/bin/bash

for ((method=0; method<=3; method++))
do
    for ((db=2; db<=4; db++))
    do
        for ((seed=1000; seed<=30000; seed+=1000))
        do
            sbatch run_proben1.sh $db $method $seed 3
            sbatch run_proben1.sh $db $method $seed 4
            sbatch run_proben1.sh $db $method $seed 5
        done
    done
done
