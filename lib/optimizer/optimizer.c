#include "optimizer.h"


double evaluate_ls(
    struct mult_perc *mlp, struct mult_perc *temp_mlp, struct activ_holdr *a, struct deriv_holdr *d, double t,
    struct database *valid_data, int batch_start, int batch_size
) {
    for(int i = 0; i < mlp->weights->number_of_weights; i++) {
        temp_mlp->weights->data[i] = mlp->weights->data[i] + mul(t, d->data[i]);
    }
    double error;
    int misses;
    evaluate_batch_mult_perc(temp_mlp, a, valid_data, batch_start, batch_size, &error, &misses);
    return error;
}

double backtracing(
    struct mult_perc *mlp, struct mult_perc *temp_mlp, struct activ_holdr *a, struct deriv_holdr *dir, struct deriv_holdr *grad,
    struct database *valid, int batch_start, int batch_size,
    double c1, double alpha, double amin
) {
    double derphi0 = 0.0;
    for(int i = 0; i < dir->number_of_weights; i++) {
        derphi0 += mul(dir->data[i], grad->data[i]);
    }
    if(derphi0 < 0.0) {
        double phi0;
        int misses;
        evaluate_batch_mult_perc(mlp, a, valid, batch_start, batch_size, &phi0, &misses);
        while(alpha > amin) {
            double phi_a = evaluate_ls(mlp, temp_mlp, a, dir, alpha, valid, batch_start, batch_size);
            if(phi_a <= phi0 + mul(mul(c1, alpha), derphi0)) {
                return alpha;
            }
            alpha = mul(alpha, BACKTRACING_ALPHA_SHRINK);
        }
    }
    return 0.0;
}

void dealloc_optim(struct optim *opt) {
    free(opt);
}

void gd_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt) {
    struct activ_holdr *a = alloc_activ_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *g = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct mult_perc *opt_mlp = alloc_mult_perc(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    reset_mult_stat();
    double min_valid_error;
    int min_valid_misses;
    evaluate_mult_perc(mlp, a, valid_data, &min_valid_error, &min_valid_misses);
    copy_weights_mult_perc(mlp, opt_mlp);
    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    if(opt->write_to_files) {
        char mode[] = "w";
        sprintf(opt->path + opt->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt->path, mode);
        fprintf(parameters_file, "gradient descent\n");
        fprintf(parameters_file, "batch size: %d\n", opt->batch_size);
        fprintf(parameters_file, "min valid error: %e\n", opt->min_valid_error);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt->consecutive_allowed);
        fprintf(parameters_file, "learning rate: %e\n", opt->alpha);
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp->weights->neurons_per_layer[i]);
        }
        fclose(parameters_file);
        sprintf(opt->path + opt->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt->path, mode);
        reset_statistics_deriv_holdr(mlp->weights);
        update_statistics_deriv_holdr(mlp->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            min_valid_error, ((double) min_valid_misses)/valid_data->size, mlp->weights->p_stat[0],
            sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
        );
        sprintf(opt->path + opt->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt->path, mode);
    }
    
    int consecutive = 0;
    double error;
    int misses;
    clock_t start_time = clock();

    long iteration = 0;
    int epochs = 0;

    while(consecutive < opt->consecutive_allowed) {

        reset_statistics_activ_holdr(a);
        reset_statistics_deriv_holdr(d);
        reset_statistics_deriv_holdr(mlp->weights);

        // epoch
        shuffle_database(train_data);
        for(int batch_end = opt->batch_size; batch_end <= train_data->size; batch_end += opt->batch_size) {
            // compute batch gradient
            set_to_zero_deriv_holdr(g);
            for(int record = batch_end-opt->batch_size; record < batch_end; record++) {
                backpropagation_mult_perc(mlp, a, d, get_input_database(train_data, record), get_output_database(train_data, record));
                update_statistics_activ_holdr(a);
                update_statistics_deriv_holdr(d);
                for(int i = 0; i < g->number_of_weights; i++) {
                    g->data[i] -= d->data[i];
                }
            }
            // update
            for(int i = 0; i < mlp->weights->number_of_weights; i++) {
                mlp->weights->data[i] += mul(opt->alpha, g->data[i]);
            }
            update_statistics_deriv_holdr(mlp->weights);
            iteration++;
        }

        // evaluate
        evaluate_mult_perc(mlp, a, valid_data, &error, &misses);
        if(opt->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a->p_stat[0], sqrt(a->p_stat[1]/a->number_of_updates), a->p_stat[2], a->p_stat[3],
                d->p_stat[0], sqrt(d->p_stat[1]/d->number_of_updates), d->p_stat[2], d->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                error, ((double) misses)/valid_data->size, mlp->weights->p_stat[0],
                sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
            );
            fflush(act_der_file);
            fflush(evaluations_file);
        }
        if(min_valid_error - error > opt->min_valid_error) {
            consecutive = 0;
            copy_weights_mult_perc(mlp, opt_mlp);
            min_valid_error = error;
            min_valid_misses = misses;
        } else {
            consecutive++;
        }
        //printf("%6ld %4d %3d %f %f %f\n", iteration, epochs, consecutive, min_valid_error, error, ((double) misses)/valid_data->size);
        epochs++;
    }
    opt->iterations = iteration;
    opt->epochs = epochs;
    opt->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt->multiplications = mult_counter;
    copy_weights_mult_perc(opt_mlp, mlp);
    if(opt->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
    }
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_deriv_holdr(g);
    dealloc_mult_perc(opt_mlp);
}

struct optim *alloc_gd_optim(double min_valid_error, int batch_size, int consecutive_allowed, double learning_rate) {
    struct optim *opt = (struct optim*) malloc(sizeof(struct optim));
    opt->optimizer = gd_optim;
    opt->min_valid_error = min_valid_error;
    opt->batch_size = batch_size;
    opt->consecutive_allowed = consecutive_allowed;
    opt->alpha = learning_rate;
    opt->write_to_files = 0;
    return opt;
}

void adam_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt) {
    struct activ_holdr *a = alloc_activ_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *g = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *m = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *v = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct mult_perc *opt_mlp = alloc_mult_perc(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    reset_mult_stat();
    set_to_zero_deriv_holdr(m);
    set_to_zero_deriv_holdr(v);
    double min_valid_error;
    int min_valid_misses;
    evaluate_mult_perc(mlp, a, valid_data, &min_valid_error, &min_valid_misses);
    copy_weights_mult_perc(mlp, opt_mlp);
    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    FILE *betas_file = 0;
    if(opt->write_to_files) {
        char mode[] = "w";
        sprintf(opt->path + opt->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt->path, mode);
        fprintf(parameters_file, "adam\n");
        fprintf(parameters_file, "batch size: %d\n", opt->batch_size);
        fprintf(parameters_file, "min valid error: %e\n", opt->min_valid_error);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt->consecutive_allowed);
        fprintf(parameters_file, "alpha: %e\n", opt->alpha);
        fprintf(parameters_file, "beta1: %e\n", opt->beta);
        fprintf(parameters_file, "beta2: %e\n", opt->gamma);
        fprintf(parameters_file, "epsilon: %e\n", opt->delta);
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp->weights->neurons_per_layer[i]);
        }
        fclose(parameters_file);
        sprintf(opt->path + opt->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt->path, mode);
        reset_statistics_deriv_holdr(mlp->weights);
        update_statistics_deriv_holdr(mlp->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            min_valid_error, ((double) min_valid_misses)/valid_data->size, mlp->weights->p_stat[0],
            sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
        );
        sprintf(opt->path + opt->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt->path, mode);
        sprintf(opt->path + opt->path_offset, "/betas.txt"); betas_file = fopen(opt->path, mode);
        fprintf(betas_file, "%e %e\n", opt->beta, opt->gamma);
    }
    int consecutive = 0;
    double error;
    int misses;
    double beta1 = opt->beta;
    double beta2 = opt->gamma;
    clock_t start_time = clock();

    long iteration = 0;
    int epochs = 0;

    while(consecutive < opt->consecutive_allowed) {

        reset_statistics_activ_holdr(a);
        reset_statistics_deriv_holdr(d);
        reset_statistics_deriv_holdr(mlp->weights);

        // epoch
        shuffle_database(train_data);
        for(int batch_end = opt->batch_size; batch_end <= train_data->size; batch_end += opt->batch_size) {
            // compute batch gradient
            set_to_zero_deriv_holdr(g);
            for(int record = batch_end - opt->batch_size; record < batch_end; record++) {
                backpropagation_mult_perc(mlp, a, d, get_input_database(train_data, record), get_output_database(train_data, record));
                if(opt->write_to_files) {
                    update_statistics_activ_holdr(a);
                    update_statistics_deriv_holdr(d);
                }
                for(int i = 0; i < d->number_of_weights; i++) {
                    g->data[i] += d->data[i];
                }
            }

            // update
            double alpha = mul(opt->alpha, sqrt(1.0 - beta2) / (1.0 - beta1));
            for(int i = 0; i < mlp->weights->number_of_weights; i++) {
                m->data[i] = mul(opt->beta, m->data[i]) + mul((1.0 - opt->beta), g->data[i]);
                v->data[i] = mul(opt->gamma, v->data[i]) + mul((1.0 - opt->gamma), mul(g->data[i], g->data[i]));
                mlp->weights->data[i] -= mul(alpha, m->data[i] / (sqrt(v->data[i]) + opt->delta));
            }
            beta1 = mul(beta1, opt->beta);
            beta2 = mul(beta2, opt->gamma);
            update_statistics_deriv_holdr(mlp->weights);
            iteration++;
        }

        // evaluate
        evaluate_mult_perc(mlp, a, valid_data, &error, &misses);
        if(opt->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a->p_stat[0], sqrt(a->p_stat[1]/a->number_of_updates), a->p_stat[2], a->p_stat[3],
                d->p_stat[0], sqrt(d->p_stat[1]/d->number_of_updates), d->p_stat[2], d->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                error, ((double) misses)/valid_data->size, mlp->weights->p_stat[0],
                sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
            );
            fprintf(betas_file, "%e %e\n", beta1, beta2);
            fflush(act_der_file);
            fflush(evaluations_file);
        }
        if(min_valid_error - error > opt->min_valid_error) {
            copy_weights_mult_perc(mlp, opt_mlp);
            min_valid_error = error;
            min_valid_misses = misses;
            consecutive = 0;
        } else {
            consecutive++;
        }
        //printf("%6ld %2d %3d %f %f %f\n", iteration, epochs, consecutive, min_valid_error, error, ((double) misses)/valid_data->size);
        epochs++;
    }
    opt->iterations = iteration;
    opt->epochs = epochs;
    opt->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt->multiplications = mult_counter;
    copy_weights_mult_perc(opt_mlp, mlp);
    if(opt->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
        fclose(betas_file);
    }
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_deriv_holdr(g);
    dealloc_deriv_holdr(m);
    dealloc_deriv_holdr(v);
    dealloc_mult_perc(opt_mlp);
}

struct optim *alloc_adam_optim(double min_valid_error, int batch_size, int consecutive_allowed, double alpha, double beta1, double beta2, double epsilon) {
    struct optim *opt = (struct optim*) malloc(sizeof(struct optim));
    opt->optimizer = adam_optim;
    opt->min_valid_error = min_valid_error;
    opt->batch_size = batch_size;
    opt->consecutive_allowed = consecutive_allowed;
    opt->alpha = alpha;
    opt->beta = beta1;
    opt->gamma = beta2;
    opt->delta = epsilon;
    opt->write_to_files = 0;
    return opt;
}

void cgm_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt) {
    struct activ_holdr *a = alloc_activ_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *g = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *s = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct mult_perc *opt_mlp = alloc_mult_perc(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct mult_perc *temp_mlp = alloc_mult_perc(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    reset_mult_stat();
    double min_valid_error;
    int min_valid_misses;
    evaluate_mult_perc(mlp, a, valid_data, &min_valid_error, &min_valid_misses);
    //printf("init %g %g\n", min_valid_error, ((double)min_valid_misses)/valid_data->size);
    copy_weights_mult_perc(mlp, opt_mlp);
    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    if(opt->write_to_files) {
        char mode[] = "w";
        sprintf(opt->path + opt->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt->path, mode);
        fprintf(parameters_file, "conjugate gradient descent\n");
        fprintf(parameters_file, "batch size: %d\n", opt->batch_size);
        fprintf(parameters_file, "min valid error: %e\n", opt->min_valid_error);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt->consecutive_allowed);
        fprintf(parameters_file, "b c1: %e\n", opt->alpha);
        fprintf(parameters_file, "b alpha: %e\n", opt->beta);
        fprintf(parameters_file, "b amin: %e\n", opt->gamma);
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp->weights->neurons_per_layer[i]);
        }
        fclose(parameters_file);
        sprintf(opt->path + opt->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt->path, mode);
        reset_statistics_deriv_holdr(mlp->weights);
        update_statistics_deriv_holdr(mlp->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            min_valid_error, ((double) min_valid_misses)/valid_data->size, mlp->weights->p_stat[0],
            sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
        );
        sprintf(opt->path + opt->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt->path, mode);
    }

    int consecutive = 0;
    double error;
    int misses;
    clock_t start_time = clock();

    long iteration = 0;
    int epochs = 0;
    double normo;
    int s_reset = 1;

    while(consecutive < opt->consecutive_allowed) {

        reset_statistics_activ_holdr(a);
        reset_statistics_deriv_holdr(d);
        reset_statistics_deriv_holdr(mlp->weights);

        // epoch
        shuffle_database(train_data);
        for(int batch_end = opt->batch_size; batch_end <= train_data->size; batch_end += opt->batch_size) {
            // compute batch gradient
            set_to_zero_deriv_holdr(g);
            for(int record = batch_end-opt->batch_size; record < batch_end; record++) {
                backpropagation_mult_perc(mlp, a, d, get_input_database(train_data, record), get_output_database(train_data, record));
                if(opt->write_to_files) {
                    update_statistics_activ_holdr(a);
                    update_statistics_deriv_holdr(d);
                }
                for(int i = 0; i < g->number_of_weights; i++) {
                    g->data[i] += d->data[i];
                }
            }
            // update
            if(s_reset == 1) {
                // reset conjugate direction
                normo = 0.0;
                for(int i = 0; i < s->number_of_weights; i++) {
                    s->data[i] = -g->data[i];
                    normo += mul(g->data[i], g->data[i]);
                }
                if(normo > 0.0) {
                    double t = backtracing(mlp, temp_mlp, a, s, g, train_data, batch_end-opt->batch_size, opt->batch_size, opt->alpha, opt->beta, opt->gamma);
                    if(t > 0.0) {
                        for(int i = 0; i < s->number_of_weights; i++) {
                            mlp->weights->data[i] += mul(t, s->data[i]);
                        }
                        s_reset = 0;
                    }
                }
            } else {
                // update conjugate direction
                double norm = 0.0;
                for(int i = 0; i < s->number_of_weights; i++) {
                    norm += mul(g->data[i], g->data[i]);
                }
                if(norm == 0.0) {
                    s_reset = 1;
                } else {
                    double beta = norm / normo;
                    for(int i = 0; i < s->number_of_weights; i++) {
                        s->data[i] = mul(beta, s->data[i]) - g->data[i];
                    }
                    double t = backtracing(mlp, temp_mlp, a, s, g, train_data, batch_end-opt->batch_size, opt->batch_size, opt->alpha, opt->beta, opt->gamma);
                    if(t == 0.0) {
                        s_reset = 1;
                    } else {
                        for(int i = 0; i < s->number_of_weights; i++) {
                            mlp->weights->data[i] += mul(t, s->data[i]);
                        }
                        normo = norm;
                    }
                }
            }
            update_statistics_deriv_holdr(mlp->weights);
            iteration++;

            //printf("%d %d\n", batch_end, train_data->size);
        }

        // evaluate
        evaluate_mult_perc(mlp, a, valid_data, &error, &misses);
        if(opt->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a->p_stat[0], sqrt(a->p_stat[1]/a->number_of_updates), a->p_stat[2], a->p_stat[3],
                d->p_stat[0], sqrt(d->p_stat[1]/d->number_of_updates), d->p_stat[2], d->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                error, ((double) misses)/valid_data->size, mlp->weights->p_stat[0],
                sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
            );
            fflush(act_der_file);
            fflush(evaluations_file);
        }
        if(min_valid_error - error > opt->min_valid_error) {
            copy_weights_mult_perc(mlp, opt_mlp);
            min_valid_error = error;
            min_valid_misses = misses;
            consecutive = 0;
        } else {
            consecutive++;
        }
        //printf("%6ld %2d %3d %f %f %f\n", iteration, epochs, consecutive, min_valid_error, error, ((double) misses)/valid_data->size);
        epochs++;
    }

    opt->iterations = iteration;
    opt->epochs = epochs;
    opt->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt->multiplications = mult_counter;
    copy_weights_mult_perc(opt_mlp, mlp);
    if(opt->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
    }
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_deriv_holdr(g);
    dealloc_deriv_holdr(s);
    dealloc_mult_perc(opt_mlp);
    dealloc_mult_perc(temp_mlp);
}

struct optim *alloc_cgm_optim(double min_valid_error, int batch_size, int consecutive_allowed, double b_c1, double b_alpha, double b_amin) {
    struct optim *opt = (struct optim*) malloc(sizeof(struct optim));
    opt->optimizer = cgm_optim;
    opt->min_valid_error = min_valid_error;
    opt->batch_size = batch_size;
    opt->consecutive_allowed = consecutive_allowed;
    opt->alpha = b_c1;
    opt->beta = b_alpha;
    opt->gamma = b_amin;
    opt->write_to_files = 0;
    return opt;
}

struct bfgs_mat *alloc_bfgs_mat(int number_of_weights) {
    struct bfgs_mat *mat = (struct bfgs_mat*) malloc(sizeof(struct bfgs_mat));
    mat->number_of_weights = number_of_weights;
    mat->number_of_elements = number_of_weights * number_of_weights;
    mat->data = (double*) malloc(sizeof(double)*(mat->number_of_elements + 4));
    mat->p_stat = mat->data + mat->number_of_elements;
    mat->p_row = (double**) malloc(sizeof(double)*number_of_weights);
    for(int i = 0; i < number_of_weights; i++) {
        mat->p_row[i] = mat->data + i * number_of_weights;
    }
    return mat;
}

void set_identity_bfgs_mat(struct bfgs_mat *mat) {
    for(int i = 0; i < mat->number_of_weights; i++) {
        for(int j = 0; j < mat->number_of_weights; j++) {
            mat->p_row[i][j] = (i == j) ? 1.0 : 0.0;
        }
    }
}

void dealloc_bfgs_mat(struct bfgs_mat *mat) {
    free(mat->data);
    free(mat->p_row);
    free(mat);
}

void print_bfgs_mat(struct bfgs_mat *mat) {
    for(int i = 0; i < mat->number_of_weights; i++) {
        for(int j = 0; j < mat->number_of_weights; j++) {
            printf("%11.4e ", mat->p_row[i][j]);
        }
        printf("\n");
    }
}

void update_bfgs_mat(struct bfgs_mat *src, double *s, double *y, double sy, struct bfgs_mat *dst) {
    double sy_sq = sy*sy;
    double yBy = 0.0;
    double By[src->number_of_weights], yB[src->number_of_weights];
    for(int i = 0; i < src->number_of_weights; i++) {
        By[i] = 0.0;
        yB[i] = 0.0;
        for(int j = 0; j < src->number_of_weights; j++) {
            By[i] += mul(src->p_row[i][j], y[j]);
            yB[i] += mul(y[j], src->p_row[j][i]);
        }
        yBy += mul(y[i], By[i]);
    }
    double sy_plus_yBy = sy + yBy;
    for(int i = 0; i < src->number_of_weights; i++) {
        for(int j = 0; j < src->number_of_weights; j++) {
            dst->p_row[i][j] = src->p_row[i][j] + mul(sy_plus_yBy, (mul(s[i], s[j])))/sy_sq - (mul(By[i], s[j]) + mul(s[i], yB[j]))/sy;
        }
    }
}

void reset_statistics_bfgs_mat(struct bfgs_mat *mat) {
    mat->number_of_updates = 0L;
    mat->p_stat[0] = 0.0;
    mat->p_stat[1] = 0.0;
    mat->p_stat[2] = 1.0/0.0;
    mat->p_stat[3] = -1.0/0.0;
}

void update_statistics_bfgs_mat(struct bfgs_mat *mat) {
    for(int i = 0; i < mat->number_of_elements; i++) {
        statistics(mat->p_stat, ++mat->number_of_updates, mat->data[i]);
    }
}

void bfgs_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt) {

    struct activ_holdr *a = alloc_activ_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *g = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *gn = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *s = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct deriv_holdr *y = alloc_deriv_holdr(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct bfgs_mat *B = alloc_bfgs_mat(mlp->weights->number_of_weights);
    struct bfgs_mat *Bn = alloc_bfgs_mat(mlp->weights->number_of_weights);
    struct mult_perc *opt_mlp = alloc_mult_perc(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    struct mult_perc *temp_mlp = alloc_mult_perc(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    set_identity_bfgs_mat(B);
    
    reset_mult_stat();

    double min_valid_error;
    int min_valid_misses;
    evaluate_mult_perc(mlp, a, valid_data, &min_valid_error, &min_valid_misses);
    copy_weights_mult_perc(mlp, opt_mlp);

    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    if(opt->write_to_files) {
        char mode[] = "w";
        sprintf(opt->path + opt->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt->path, mode);
        fprintf(parameters_file, "bfgs\n");
        fprintf(parameters_file, "batch size: %d\n", opt->batch_size);
        fprintf(parameters_file, "min valid error: %e\n", opt->min_valid_error);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt->consecutive_allowed);
        fprintf(parameters_file, "b c1: %e\n", opt->alpha);
        fprintf(parameters_file, "b amin: %e\n", opt->beta);
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp->weights->neurons_per_layer[i]);
        }
        fclose(parameters_file);
        sprintf(opt->path + opt->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt->path, mode);
        reset_statistics_deriv_holdr(mlp->weights);
        update_statistics_deriv_holdr(mlp->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            min_valid_error, ((double) min_valid_misses)/valid_data->size, mlp->weights->p_stat[0],
            sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
        );
        sprintf(opt->path + opt->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt->path, mode);
    }

    int consecutive = 0;
    double error;
    int misses;
    clock_t start_time = clock();

    int B_reset = 1;
    long iteration = 0;
    int epochs = 0;

    while(consecutive < opt->consecutive_allowed) {

        reset_statistics_activ_holdr(a);
        reset_statistics_deriv_holdr(d);
        reset_statistics_deriv_holdr(mlp->weights);

        // epoch
        shuffle_database(train_data);
        for(int batch_end = opt->batch_size; batch_end <= train_data->size; batch_end += opt->batch_size) {
            // update
            if(B_reset == 1) {
                // reset Hessian apx.
                // compute batch gradient
                set_to_zero_deriv_holdr(g);
                for(int record = 0; record < opt->batch_size; record++) {
                    backpropagation_mult_perc(mlp, a, d, get_input_database(train_data, record), get_output_database(train_data, record));
                    update_statistics_activ_holdr(a);
                    update_statistics_deriv_holdr(d);
                    for(int i = 0; i < d->number_of_weights; i++) {
                        g->data[i] += d->data[i];
                    }
                }

                set_identity_bfgs_mat(B);
                for(int i = 0; i < s->number_of_weights; i++) {
                    s->data[i] = -g->data[i];
                }
                double t = backtracing(mlp, temp_mlp, a, s, g, train_data, batch_end-opt->batch_size, opt->batch_size, opt->alpha, opt->beta, opt->gamma);
                if(t > 0.0) {
                    for(int i = 0; i < s->number_of_weights; i++) {
                        s->data[i] = mul(t, s->data[i]);
                        mlp->weights->data[i] += s->data[i];
                    }
                    B_reset = 0;
                }
            } else {
                // update Hessian apx.
                // compute batch gradient
                set_to_zero_deriv_holdr(gn);
                for(int record = 0; record < opt->batch_size; record++) {
                    backpropagation_mult_perc(mlp, a, d, get_input_database(train_data, record), get_output_database(train_data, record));
                    update_statistics_activ_holdr(a);
                    update_statistics_deriv_holdr(d);
                    for(int i = 0; i < d->number_of_weights; i++) {
                        gn->data[i] += d->data[i];
                    }
                }

                double sy = 0.0;
                for(int i = 0; i < s->number_of_weights; i++) {
                    y->data[i] = gn->data[i] - g->data[i];
                    sy += mul(s->data[i], y->data[i]);
                }
                if(sy <= 0.0) {
                    B_reset = 1;
                } else {
                    update_bfgs_mat(B, s->data, y->data, sy, Bn);
                    struct bfgs_mat *temp_B = B;
                    B = Bn;
                    Bn = temp_B;
                    for(int i = 0; i < s->number_of_weights; i++) {
                        s->data[i] = 0.0;
                        for(int j = 0; j < s->number_of_weights; j++) {
                            s->data[i] -= mul(B->p_row[i][j], gn->data[i]);
                        }
                    }
                    double t = backtracing(mlp, temp_mlp, a, s, g, train_data, batch_end-opt->batch_size, opt->batch_size, opt->alpha, opt->beta, opt->gamma);
                    if(t == 0.0) {
                        B_reset = 1;
                    } else {
                        for(int i = 0; i < s->number_of_weights; i++) {
                            s->data[i] = mul(t, s->data[i]);
                            mlp->weights->data[i] += s->data[i];
                        }
                        struct deriv_holdr *temp_g = g;
                        g = gn;
                        gn = temp_g;
                    }
                }   
            }
            update_statistics_deriv_holdr(mlp->weights);
            iteration++;
        }

        // evaluate
        evaluate_mult_perc(mlp, a, valid_data, &error, &misses);
        if(opt->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a->p_stat[0], sqrt(a->p_stat[1]/a->number_of_updates), a->p_stat[2], a->p_stat[3],
                d->p_stat[0], sqrt(d->p_stat[1]/d->number_of_updates), d->p_stat[2], d->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                error, ((double) misses)/valid_data->size, mlp->weights->p_stat[0],
                sqrt(mlp->weights->p_stat[1]/mlp->weights->number_of_updates), mlp->weights->p_stat[2], mlp->weights->p_stat[3]
            );
            fflush(act_der_file);
            fflush(evaluations_file);
        }
        if(min_valid_error - error > opt->min_valid_error) {
            copy_weights_mult_perc(mlp, opt_mlp);
            min_valid_error = error;
            min_valid_misses = misses;
            consecutive = 0;
        } else {
            consecutive++;
        }
        //printf("%6ld %2d %3d %f %f %f\n", iteration, epochs, consecutive, min_valid_error, error, ((double) misses)/valid_data->size);
        epochs++;
    }

    opt->iterations = iteration;
    opt->epochs = epochs;
    opt->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt->multiplications = mult_counter;
    
    copy_weights_mult_perc(opt_mlp, mlp);
    if(opt->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
    }

    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_deriv_holdr(g);
    dealloc_deriv_holdr(gn);
    dealloc_deriv_holdr(s);
    dealloc_deriv_holdr(y);
    dealloc_bfgs_mat(B);
    dealloc_bfgs_mat(Bn);
    dealloc_mult_perc(opt_mlp);
    dealloc_mult_perc(temp_mlp);
}

struct optim *alloc_bfgs_optim(double min_valid_error, int batch_size, int consecutive_allowed, double b_c1, double b_alpha, double b_amin) {
    struct optim *opt = (struct optim*) malloc(sizeof(struct optim));
    opt->min_valid_error = min_valid_error;
    opt->optimizer = bfgs_optim;
    opt->batch_size = batch_size;
    opt->consecutive_allowed = consecutive_allowed;
    opt->alpha = b_c1;
    opt->beta = b_alpha;
    opt->gamma = b_amin;
    opt->write_to_files = 0;
    return opt;
}
