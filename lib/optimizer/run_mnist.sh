#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --job-name=MNIST
#SBATCH --mem-per-cpu=4096
#SBATCH --time=5-00

./test 1 $1 $2 $3 $4
