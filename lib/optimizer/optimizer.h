#ifndef _OPTIMIZER
#define _OPTIMIZER

    #include <time.h>
    #include "../database/database.h"
    #include "../mult_perc/mult_perc.h"


// --------------------------------------------------------------------------------------------------------------------------------------------------

    //#define BACKTRACING_ALPHA_SHRINK 0.9 // Proben1
    #define BACKTRACING_ALPHA_SHRINK 0.5 // MNIST

    extern long mult_counter;

// --------------------------------------------------------------------------------------------------------------------------------------------------

    double evaluate_ls(
        struct mult_perc *mlp, struct mult_perc *temp_mlp, struct activ_holdr *a, struct deriv_holdr *d, double t,
        struct database *valid_data, int batch_start, int batch_size
    );

    // backtracing Armijo
    double backtracing(
        struct mult_perc *mlp, struct mult_perc *temp_mlp, struct activ_holdr *a, struct deriv_holdr *dir, struct deriv_holdr *grad,
        struct database *valid, int batch_start, int batch_size,
        double c1, double alpha, double amin
    );

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct optim {
        // optimization algorithm
        void (*optimizer) (struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt);

        // common parameters
        double min_valid_error;
        int batch_size;
        int consecutive_allowed;

        // specific parameters
        double alpha;
        double beta;
        double gamma;
        double delta;

        char write_to_files;
        char *path;
        int path_offset;

        long iterations;
        int epochs;
        double duration;
        long multiplications;
    };

    // deallocate memory
    void dealloc_optim(struct optim *opt);


// --------------------------------------------------------------------------------------------------------------------------------------------------

    // gradient descent
    void gd_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt);
    struct optim *alloc_gd_optim(double min_valid_error, int batch_size, int consecutive_allowed, double learning_rate);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // adam
    void adam_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt);
    struct optim *alloc_adam_optim(double min_valid_error, int batch_size, int consecutive_allowed, double alpha, double beta1, double beta2, double epsilon);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // cgm - Polak-Ribiere
    void cgm_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt);
    struct optim *alloc_cgm_optim(double min_valid_error, int batch_size, int consecutive_allowed, double b_c1, double b_alpha, double b_amin);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct bfgs_mat {
        int number_of_weights;
        int number_of_elements;
        double *data;
        double **p_row;

        // statistics
        long number_of_updates;
        double *p_stat;
    };

    struct bfgs_mat *alloc_bfgs_mat(int number_of_weights);
    void set_identity_bfgs_mat(struct bfgs_mat *mat);
    void dealloc_bfgs_mat(struct bfgs_mat *mat);
    void print_bfgs_mat(struct bfgs_mat *mat);
    void update_bfgs_mat(struct bfgs_mat *src, double *s, double *y, double sy, struct bfgs_mat *dst);

    void reset_statistics_bfgs_mat(struct bfgs_mat *mat);
    void update_statistics_bfgs_mat(struct bfgs_mat *mat);

    // bfgs
    void bfgs_optim(struct mult_perc *mlp, struct database *train_data, struct database *valid_data, struct optim *opt);
    struct optim *alloc_bfgs_optim(double min_valid_error, int batch_size, int consecutive_allowed, double b_c1, double b_alpha, double b_amin);

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif