#!/bin/bash

method=$1

for ((seed=1000; seed<=30000; seed+=1000))
do
    sbatch run_mnist.sh $seed 30 $method
    sbatch run_mnist.sh $seed 60 $method
    sbatch run_mnist.sh $seed 100 $method
done
