#include "mult_perc.h"



double (*sigma)(double);
double (*dsigma)(double);

void set_sigmoid() {
    sigma = logistic_function;
    dsigma = logistic_function_derivative;
}

void set_ReLU() {
    sigma = ReLU_function;
    dsigma = ReLU_function_derivative;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

struct activ_holdr *alloc_activ_holdr(int number_of_layers, int neurons_per_layer[]) {
    struct activ_holdr *a = (struct activ_holdr*) malloc(sizeof(struct activ_holdr));
    a->number_of_layers = number_of_layers;
    a->neurons_per_layer = (int*) malloc(sizeof(int) * number_of_layers);
    a->number_of_neurons = 0;
    for(int i = 0; i < number_of_layers; i++) {
        a->neurons_per_layer[i] = neurons_per_layer[i];
        a->number_of_neurons += neurons_per_layer[i];
    }
    a->data = (double*) malloc(sizeof(double) * (2 * a->number_of_neurons - neurons_per_layer[0] + 4));
    a->p_stat = a->data + 2 * a->number_of_neurons - neurons_per_layer[0];
    a->p_act = (double**) malloc(sizeof(double*) * (2 * number_of_layers - 1));
    a->p_sig = a->p_act + number_of_layers - 1;
    a->p_sig[0] = a->data + a->number_of_neurons - neurons_per_layer[0];
    int offset = 0;
    for(int i = 1; i < number_of_layers; i++) {
        a->p_act[i - 1] = a->data + offset;
        a->p_sig[i] = a->data + a->number_of_neurons + offset;
        offset += neurons_per_layer[i];
    }
    return a;
}

void dealloc_activ_holdr(struct activ_holdr *a) {
    free(a->neurons_per_layer);
    free(a->data);
    free(a->p_act);
    free(a);
}

void print_activ_holdr(struct activ_holdr *a) {
    printf(".--------------------------------------------.\n");
    printf("| layer | neuron |     act     |     sig     |\n");
    printf("|-------|--------|-------------|-------------|\n");
    for(int i = 0; i < a->number_of_layers; i++) {
        if(i > 0) {
            for(int j = 0; j < a->neurons_per_layer[i]; j++) {
                printf("|  %2d   |  %3d   | %11.4e | %11.4e |\n", i, j, a->p_act[i - 1][j], a->p_sig[i][j]);
            }
        } else {
            for(int j = 0; j < a->neurons_per_layer[i]; j++) {
                printf("|  %2d   |  %3d   |             | %11.4e |\n", i, j, a->p_sig[i][j]);
            }
        }
        if(i < a->number_of_layers-1) {
            printf("|--------------------------------------------|\n");
        } else {
            printf("'--------------------------------------------'\n");
        }
    }
    printf("\n");
}

void reset_statistics_activ_holdr(struct activ_holdr *a) {
    a->number_of_updates = 0L;
    a->p_stat[0] = 0.0;
    a->p_stat[1] = 0.0;
    a->p_stat[2] = 1.0/0.0;
    a->p_stat[3] = -1.0/0.0;
}

void update_statistics_activ_holdr(struct activ_holdr *a) {
    for(int i = 0; i < a->number_of_neurons - a->neurons_per_layer[0]; i++) {
        statistics(a->p_stat, ++a->number_of_updates, a->p_act[0][i]);
    }
}

void print_statistics_activ_holdr(struct activ_holdr *a, FILE *file) {
    fprintf(file, "%e %e %e %e\n", a->p_stat[0], sqrt(a->p_stat[1]/a->number_of_updates), a->p_stat[2], a->p_stat[3]);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

struct deriv_holdr *alloc_deriv_holdr(int number_of_layers, int neurons_per_layer[]) {
    struct deriv_holdr *d = (struct deriv_holdr *) malloc(sizeof(struct deriv_holdr));
    d->number_of_layers = number_of_layers;
    d->neurons_per_layer = (int*) malloc(sizeof(int) * number_of_layers);
    d->number_of_neurons = 0;
    d->number_of_weights = 0;
    for(int i = 0; i < number_of_layers; i++) {
        d->neurons_per_layer[i] = neurons_per_layer[i];
        d->number_of_neurons += neurons_per_layer[i];
        if(i > 0) {
            d->number_of_weights += (neurons_per_layer[i - 1] + 1) * neurons_per_layer[i];
        }
    }
    d->data = (double*) malloc(sizeof(double) * (d->number_of_weights + 4));
    d->p_row = (double**) malloc(sizeof(double*) * (d->number_of_neurons - neurons_per_layer[0]));
    d->p_mat = (double***) malloc(sizeof(double**) * (number_of_layers - 1));
    d->p_stat = d->data + d->number_of_weights;
    int number_of_neurons = 0, number_of_rows = 0, number_of_weights = 0;
    for(int i = 1; i < number_of_layers; i++) {
        int weights_in_layer = 0;
        for(int j = 0; j < neurons_per_layer[i]; j++) {
            d->p_row[number_of_rows++] = d->data + number_of_weights + weights_in_layer;
            weights_in_layer += neurons_per_layer[i - 1] + 1;
        }
        d->p_mat[i - 1] = d->p_row + number_of_neurons;
        number_of_neurons += neurons_per_layer[i];
        number_of_weights += weights_in_layer;
    }
    return d;
}

void dealloc_deriv_holdr(struct deriv_holdr *d) {
    free(d->neurons_per_layer);
    free(d->data);
    free(d->p_row);
    free(d->p_mat);
    free(d);
}

void print_deriv_holdr(struct deriv_holdr *d) {
    printf(".---------------------------------------.\n");
    printf("| layer | neuron | weight |    value    |\n");
    printf("|-------|--------|--------|-------------|\n");
    for(int i = 1; i < d->number_of_layers; i++) {
        for(int j = 0; j < d->neurons_per_layer[i]; j++) {
            for(int k = 0; k < d->neurons_per_layer[i-1]+1; k++) {
                printf("|  %2d   |  %3d   |  %3d   | %11.4e |\n", i, j, k, d->p_mat[i-1][j][k]);
            }
            if(j < d->neurons_per_layer[i]-1) {
                printf("|---------------------------------------|\n");
            } else if(i < d->number_of_layers-1) {
                printf("|:::::::::::::::::::::::::::::::::::::::|\n");
            } else {
                printf("'---------------------------------------'\n");
            }
        }
    }
    printf("\n");
}

void init_rand_deriv_holdr(struct deriv_holdr *d, double scale) {
    for(int i = 0; i < d->number_of_weights; i++) {
        d->data[i] = scale*(2.0*(((double) rand())/RAND_MAX) - 1.0);
    }
}

void set_to_zero_deriv_holdr(struct deriv_holdr *d) {
    for(int i = 0; i < d->number_of_weights; i++) {
        d->data[i] = 0.0;
    }
}

void copy_weights_deriv_holdr(struct deriv_holdr *src, struct deriv_holdr *dst) {
    for(int i = 0; i < dst->number_of_weights; i++) {
        dst->data[i] = src->data[i];
    }
}

void reset_statistics_deriv_holdr(struct deriv_holdr *d) {
    d->number_of_updates = 0L;
    d->p_stat[0] = 0.0;
    d->p_stat[1] = 0.0;
    d->p_stat[2] = 1.0/0.0;
    d->p_stat[3] = -1.0/0.0;
}

void update_statistics_deriv_holdr(struct deriv_holdr *d) {
    for(int i = 0; i < d->number_of_weights; i++) {
        statistics(d->p_stat, ++d->number_of_updates, d->data[i]);
    }
}

void print_statistics_deriv_holdr(struct deriv_holdr *d, FILE *file) {
    fprintf(file, "%e %e %e %e\n", d->p_stat[0], sqrt(d->p_stat[1]/d->number_of_updates), d->p_stat[2], d->p_stat[3]);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

struct mult_perc *alloc_mult_perc(int size, int layers[]) {
    struct mult_perc *mlp = (struct mult_perc*) malloc(sizeof(struct mult_perc));
    mlp->weights = alloc_deriv_holdr(size, layers);
    return mlp;
}

void dealloc_mult_perc(struct mult_perc *mlp) {
    dealloc_deriv_holdr(mlp->weights);
    free(mlp);
}

void print_mult_perc(struct mult_perc *mlp) {
    print_deriv_holdr(mlp->weights);
}

void init_rand_mult_perc(struct mult_perc *mlp, double scale) {
    init_rand_deriv_holdr(mlp->weights, scale);
}

void copy_weights_mult_perc(struct mult_perc *src, struct mult_perc *dst) {
    copy_weights_deriv_holdr(src->weights, dst->weights);
}

void evaluate_example_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, double in[], double out[], double *error, int *misses) {
    int out_size = a->neurons_per_layer[a->number_of_layers - 1];
    forward_mult_perc(mlp, a, in);
    *misses = double_pointer_argmax(out, out_size) == double_pointer_argmax(a->p_sig[a->number_of_layers - 1], out_size) ? 0 : 1;
    double cost = 0.0;
    for(int i = 0; i < out_size; i++) {
        double temp = a->p_sig[a->number_of_layers - 1][i] - out[i];
        cost += mul(temp, temp);
    }
    *error = cost;
    mult_counter += (long) out_size; // count multiplications
}

void evaluate_batch_mult_perc(
    struct mult_perc *mlp, struct activ_holdr *a, struct database *data, int batch_start, int batch_size, double *error, int *misses
) {
    double cost = 0.0;
    int fn = 0;
    int batch = 0;
    for(int record = batch_start; record < data->size && batch < batch_size; record++, batch++) {
        double *in = get_input_database(data, record);
        double *out = get_output_database(data, record);
        double temp_cost;
        int temp_fn;
        evaluate_example_mult_perc(mlp, a, in, out, &temp_cost, &temp_fn);
        cost += temp_cost;
        fn += temp_fn;
    }
    *error = cost;
    *misses = fn;
}

void evaluate_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, struct database *data, double *error, int *misses) {
    evaluate_batch_mult_perc(mlp, a, data, 0, data->size, error, misses);
}

void forward_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, double *in) {
    for(int i = 0; i < mlp->weights->neurons_per_layer[0]; i++) {
        a->p_sig[0][i] = in[i];
    }
    long counter = 0L;
    for(int i = 1; i < mlp->weights->number_of_layers; i++) {
        for(int j = 0; j < mlp->weights->neurons_per_layer[i]; j++) {
            a->p_act[i-1][j] = mlp->weights->p_mat[i-1][j][0];
            for(int k = 1; k < mlp->weights->neurons_per_layer[i-1]+1; k++) {
                //a->p_act[i-1][j] += mlp->weights->p_mat[i-1][j][k]*a->p_sig[i-1][k-1];
                a->p_act[i-1][j] += mul(mlp->weights->p_mat[i-1][j][k], a->p_sig[i-1][k-1]);
            }
            counter += mlp->weights->neurons_per_layer[i-1];
            a->p_sig[i][j] = sigma(a->p_act[i-1][j]);
        }
    }
    mult_counter += counter; // count multiplications
}

double numeric_grad_mult_perc(
    struct mult_perc *mlp, struct activ_holdr *a, int layer_idx, int neuron_idx, int weight_idx, double dweig, double in[], double out[]
) {
    double weig = mlp->weights->p_mat[layer_idx][neuron_idx][weight_idx];
    mlp->weights->p_mat[layer_idx][neuron_idx][weight_idx] -= dweig;
    double error1, error2;
    int misses;
    evaluate_example_mult_perc(mlp, a, in, out, &error1, &misses);
    mlp->weights->p_mat[layer_idx][neuron_idx][weight_idx] = weig + dweig;
    evaluate_example_mult_perc(mlp, a, in, out, &error2, &misses);
    return (error2 - error1)/(2.0 * dweig);
}

void backpropagation_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, struct deriv_holdr *d, double in[],  double out[]) {
    forward_mult_perc(mlp, a, in);
    int l = mlp->weights->number_of_layers - 1;
    for(int i = 0; i < mlp->weights->neurons_per_layer[l]; i++) {
        d->p_mat[l-1][i][0] = mul((a->p_sig[l][i] - out[i]), dsigma(a->p_act[l-1][i]));
        for(int j = 1; j < mlp->weights->neurons_per_layer[l-1]+1; j++) {
            d->p_mat[l-1][i][j] = mul(d->p_mat[l-1][i][0], a->p_sig[l-1][j-1]);
        }
    }
    for(l--; l > 0; l--) {
        for(int i = 0; i < mlp->weights->neurons_per_layer[l]; i++) {
            double sum = 0.0;
            for(int j = 0; j < mlp->weights->neurons_per_layer[l+1]; j++) {
                sum += mul(d->p_mat[l][j][0], mlp->weights->p_mat[l][j][i+1]);
            }
            d->p_mat[l-1][i][0] = mul(sum, dsigma(a->p_act[l-1][i]));
            for(int j = 1; j < mlp->weights->neurons_per_layer[l-1]+1; j++) {
                d->p_mat[l-1][i][j] = mul(d->p_mat[l-1][i][0], a->p_sig[l-1][j-1]);
            }
        }
    }
}
