#ifndef _MULT_PERC
#define _MULT_PERC

    #include "../utils/utils.h"
    #include "../database/database.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct mult_perc;
    struct activ_holdr;
    struct deriv_holdr;

    // number of multiplications during training
    extern long mult_counter;

    // activation function
    extern double (*sigma)(double);
    extern double (*dsigma)(double);

    void set_sigmoid();
    void set_ReLU();

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // neurons activation and sigma(activation)
    struct activ_holdr {
        int number_of_layers;
        int number_of_neurons;
        int *neurons_per_layer;
        double *data;
        double **p_act;
        double **p_sig;

        // statistics
        long number_of_updates;
        double *p_stat;
    };

    struct activ_holdr *alloc_activ_holdr(int number_of_layers, int neurons_per_layer[]);
    void dealloc_activ_holdr(struct activ_holdr *a);
    void print_activ_holdr(struct activ_holdr *a);

    void reset_statistics_activ_holdr(struct activ_holdr *a);
    void update_statistics_activ_holdr(struct activ_holdr *a);
    void print_statistics_activ_holdr(struct activ_holdr *a, FILE *file);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // gradient
    struct deriv_holdr {
        int number_of_layers;
        int number_of_neurons;
        int number_of_weights;
        int *neurons_per_layer;
        double *data;
        double **p_row;
        double ***p_mat;
        
        // statistics
        long number_of_updates;
        double *p_stat;
    };

    struct deriv_holdr *alloc_deriv_holdr(int number_of_layers, int neurons_per_layer[]);
    void dealloc_deriv_holdr(struct deriv_holdr *d);
    void print_deriv_holdr(struct deriv_holdr *d);
    void init_rand_deriv_holdr(struct deriv_holdr *d, double scale);
    void set_to_zero_deriv_holdr(struct deriv_holdr *d);
    void copy_weights_deriv_holdr(struct deriv_holdr *src, struct deriv_holdr *dst);

    void reset_statistics_deriv_holdr(struct deriv_holdr *d);
    void update_statistics_deriv_holdr(struct deriv_holdr *d);
    void print_statistics_deriv_holdr(struct deriv_holdr *d, FILE *file);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // multilayer perceptron
    struct mult_perc {
        struct deriv_holdr *weights;        // weights and biases
    };

    struct mult_perc *alloc_mult_perc(int size, int layers[]);
    void dealloc_mult_perc(struct mult_perc *mlp);
    void print_mult_perc(struct mult_perc *mlp);
    void init_rand_mult_perc(struct mult_perc *mlp, double scale);
    void copy_weights_mult_perc(struct mult_perc *src, struct mult_perc *dst);

    void evaluate_example_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, double in[], double out[], double *error, int *misses);

    // calculate SE (squared error) and TPR (true positive rate: only for classification) on dataset batch
    void evaluate_batch_mult_perc(
        struct mult_perc *mlp, struct activ_holdr *a, struct database *data,
        int batch_start, int batch_size, double *error, int *misses
    );

    // calculate SE (squared error) and TPR (true positive rate: only for classification) on dataset
    void evaluate_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, struct database *data, double *error, int *misses);

    // forward pass:
    // arguments
    // - struct mult_perc *mlp: multilayer perceptron
    // - struct activ_holdr *a: memory to save neurons actiovations and sigma (activations)
    // - double in[]: input vector
    void forward_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, double in[]);

    // numerical derivative:
    double numeric_grad_mult_perc(
        struct mult_perc *mlp, struct activ_holdr *a, int layer_idx, int neuron_idx, int weight_idx, double dweig, double in[], double out[]
    );

    // backpropagation:
    // arguments
    // - struct mult_perc *mlp: multilayer perceptron
    // - struct activ_holdr *a: memory to save neurons actiovations and sigma(activations)
    // - struct deriv_holdr *d: memory to save neurons partial derivatives
    // - double in[]: input vector
    // - double out[]: output vector
    void backpropagation_mult_perc(struct mult_perc *mlp, struct activ_holdr *a, struct deriv_holdr *d, double in[], double out[]);

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif