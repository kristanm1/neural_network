#include "mult_perc.h"


void test_activ_holdr(int size, int *layers) {
    struct activ_holdr *a = alloc_activ_holdr(size, layers);
    for(int i = 0; i < 2*a->number_of_neurons - a->neurons_per_layer[0]; i++) {
        a->data[i] = i + 1;
    }
    print_activ_holdr(a);
    dealloc_activ_holdr(a);
}

void test_activ_holdr_stat(int size, int *layers) {
    struct activ_holdr *a = alloc_activ_holdr(size, layers);
    reset_statistics_activ_holdr(a);
    for(int i = 0; i < a->number_of_neurons - a->neurons_per_layer[0]; i++) {
        a->p_act[0][i] = i + 1;
    }
    update_statistics_activ_holdr(a);
    print_activ_holdr(a);
    print_statistics_activ_holdr(a, stdout);
    dealloc_activ_holdr(a);
}

void test_mult_perc(int size, int *layers) {
    struct mult_perc *mlp = alloc_mult_perc(size, layers);
    for(int i = 0; i < mlp->weights->number_of_weights; i++) {
        mlp->weights->data[i] = i+1;
    }
    print_mult_perc(mlp);
    dealloc_mult_perc(mlp);
}

void test_mult_perc_stat() {
    int size = 2, layers[] = {4, 2};
    struct mult_perc *mlp = alloc_mult_perc(size, layers);
    reset_statistics_deriv_holdr(mlp->weights);
    for(int i = 0; i < mlp->weights->number_of_weights; i++) {
        mlp->weights->data[i] = i + 1;
    }
    update_statistics_deriv_holdr(mlp->weights);
    print_statistics_deriv_holdr(mlp->weights, stdout);
    dealloc_mult_perc(mlp);
}

// https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
// w5+ = 0.35891648     w1+ = 0.14978072
// w6+ = 0.40866619     w2+ = 0.19956143
// w7+ = 0.51130127     w3+ = 0.24975114
// w8+ = 0.56137012     w4+ = 0.29950229
void test_mattmazur_mult_perc() {
    int size = 3;
    int layers[] = {2, 2, 2};
    double x[] = {0.05, 0.1};
    double y[] = {0.01, 0.99};

    struct mult_perc *mlp = alloc_mult_perc(size, layers);
    struct activ_holdr *a = alloc_activ_holdr(size, layers);
    struct deriv_holdr *d = alloc_deriv_holdr(size, layers);

    mlp->weights->p_mat[0][0][0] = 0.35;
    mlp->weights->p_mat[0][0][1] = 0.15;
    mlp->weights->p_mat[0][0][2] = 0.2;

    mlp->weights->p_mat[0][1][0] = 0.35;
    mlp->weights->p_mat[0][1][1] = 0.25;
    mlp->weights->p_mat[0][1][2] = 0.3;

    mlp->weights->p_mat[1][0][0] = 0.6;
    mlp->weights->p_mat[1][0][1] = 0.4;
    mlp->weights->p_mat[1][0][2] = 0.45;

    mlp->weights->p_mat[1][1][0] = 0.6;
    mlp->weights->p_mat[1][1][1] = 0.5;
    mlp->weights->p_mat[1][1][2] = 0.55;

    backpropagation_mult_perc(mlp, a, d, x, y);
    
    double learning_rate = 0.5;
    for(int i = 0; i < mlp->weights->number_of_weights; i++) {
        //mlp->weights->data[i] -= learning_rate*d->data[i];
        mlp->weights->data[i] -= mul(learning_rate, d->data[i]);
        mult_counter++;
    }

    printf("w5: %.8f\n", mlp->weights->p_mat[1][0][1]);
    printf("w6: %.8f\n", mlp->weights->p_mat[1][0][2]);
    printf("w7: %.8f\n", mlp->weights->p_mat[1][1][1]);
    printf("w8: %.8f\n\n", mlp->weights->p_mat[1][1][2]);

    printf("w1: %.8f\n", mlp->weights->p_mat[0][0][1]);
    printf("w2: %.8f\n", mlp->weights->p_mat[0][0][2]);
    printf("w3: %.8f\n", mlp->weights->p_mat[0][1][1]);
    printf("w4: %.8f\n", mlp->weights->p_mat[0][1][2]);
    
    dealloc_mult_perc(mlp);
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
}

void test_evaluate_example_mult_perc() {
    int number_of_layers = 3, neurons_per_layer[] = {5, 5, 3};

    struct activ_holdr *a = alloc_activ_holdr(number_of_layers, neurons_per_layer);
    struct deriv_holdr *d = alloc_deriv_holdr(number_of_layers, neurons_per_layer);
    struct mult_perc *mlp = alloc_mult_perc(number_of_layers, neurons_per_layer);
    init_rand_mult_perc(mlp, 0.2);

    double in[] = {0.2, -0.3, 0.4, 0.1, -0.4};
    double out[] = {1.0, 0.0, 0.0};

    backpropagation_mult_perc(mlp, a, d, in, out);
    
    double dweig = 1e-5;
    for(int i = 1; i < number_of_layers; i++) {
        printf("layer %d\n", i);
        for(int j = 0; j < neurons_per_layer[i]; j++) {
            printf("neuron %d\n", j);
            for(int k = 0; k < neurons_per_layer[i - 1]; k++) {
                double temp = numeric_grad_mult_perc(mlp, a, i-1, j, k, dweig, in, out)/2.0;
                printf("%8.5f - %8.5f = %f\n", d->p_mat[i-1][j][k], temp, d->p_mat[i-1][j][k] - temp);
            }
        }
    }

    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);
    dealloc_mult_perc(mlp);

}

int main(int argc, char *argv[]) {

    //int size = 3, layers[] = {4, 6, 4};
    //test_activ_holdr(size, layers);
    //test_activ_holdr_stat(size, layers);

    //test_mult_perc(size, layers);
    //test_mult_perc_stat();

    //test_mult_perc(size, layers);


    set_sigmoid();
    //set_ReLU();
    test_mattmazur_mult_perc();
    //test_evaluate_example_mult_perc();

    
    return 0;
}