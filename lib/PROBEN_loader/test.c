#include "proben_loader.h"


void test_load_proben() {
    char *filename = "../../db/proben1/glass/glass1.dt";

    struct proben *const data = load_proben(filename);
    printf("bool in: %d\n", data->bool_in);
    printf("real in: %d\n", data->real_in);
    printf("bool out: %d\n", data->bool_out);
    printf("real in: %d\n", data->real_out);
    printf("training_examples: %d\n", data->training_examples);
    printf("validation_examples: %d\n", data->validation_examples);
    printf("test_examples: %d\n", data->test_examples);
    
    int p = 200;
    int rows = data->training_examples + data->validation_examples + data->test_examples;
    int cols = data->bool_in + data->real_in + data->bool_out + data->real_out;
    if(p < rows) {
        for(int i = 0; i < data->bool_in + data->real_in; i++) {
            printf("%f ", data->data[p*cols + i]);
        }
        printf(" ->  ");
        for(int i = 0; i < data->bool_out + data->real_out; i++) {
            printf("%f ", data->data[p*cols + data->bool_in + data->real_in + i]);
        }
        printf("\n");
    }
    
    dealloc_proben(data);

}

int main(int argc, char *argv[]) {

    test_load_proben();

    return 0;
}