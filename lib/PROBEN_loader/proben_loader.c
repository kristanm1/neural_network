#include "proben_loader.h"

int proben1_number_of_layers;
char *proben1_db_paths[] = {
    "../../db/proben1/cancer/cancer1.dt",       // 0
    "../../db/proben1/card/card1.dt",           // 1
    "../../db/proben1/diabetes/diabetes1.dt",   // 2
    "../../db/proben1/gene/gene1.dt",           // 3
    "../../db/proben1/heart/heart1.dt",         // 4
    "../../db/proben1/heart/heartc1.dt",        // 5
    "../../db/proben1/horse/horse1.dt"          // 6
};

char *proben1_db_names[] = {"cancer1", "card1", "diabetes1", "gene1", "heart1", "heartc1", "horse1"};
double proben1_init_weights = 1.0;
double proben1_min_valid_error[] = {
    0.001,     // cancer1
    0.001,     // card1
    0.001,     // diabetes1
    0.001,     // gene1
    0.001,     // heart1
    0.001,     // heartc1
    0.001      // horse1
};
int proben1_consecutive_allowed[] = {
    200,      // cancer1
    200,      // card1
    300,      // diabetes1
    100,      // gene1
    200,      // heart1
    100,      // heartc1
    100       // horse1
};
int proben1_batch_sizes[] = {
    50,       // cancer1
    50,       // card1
    192,      // diabetes1
    500,      // gene1
    92,       // heart1
    38,       // heartc1
    30        // horse1
};

int *proben1_neurons_per_layer;

int proben1_3_layer[][3] = {
    {9, 6, 2},      // cancer1
    {51, 6, 2},     // card1
    {8, 7, 2},      // diabetes1
    {120, 8, 3},    // gene1
    {35, 8, 2},     // heart1
    {35, 8, 2},     // heartc1
    {58, 12, 3}     // horse1
};


int proben1_4_layer[][4] = {
    {9, 6, 6, 2},      // cancer1
    {51, 6, 6, 2},     // card1
    {8, 7, 7, 2},      // diabetes1
    {120, 8, 8, 3},    // gene1
    {35, 8, 8, 2},     // heart1
    {35, 8, 8, 2},     // heartc1
    {58, 12, 12, 3}    // horse1
};


int proben1_5_layer[][5] = {
    {9, 6, 6, 6, 2},      // cancer1
    {51, 6, 6, 6, 2},     // card1
    {8, 7, 7, 7, 2},      // diabetes1
    {120, 8, 8, 8, 3},    // gene1
    {35, 8, 8, 8, 2},     // heart1
    {35, 8, 8, 8, 2},     // heartc1
    {58, 12, 12, 12, 3}   // horse1
};


double proben1_gd_learning_rates[] = {
    0.01,      // cancer1
    0.01,      // card1
    0.02,      // diabetes1
    0.005,     // gene1
    0.002,     // heart1
    0.02,      // heartc1
    0.005      // horse1
};

double proben1_adam_alphas[] = {
    0.01,      // cancer1
    0.01,      // card1
    0.01,      // diabetes1
    0.005,     // gene1
    0.002,     // heart1
    0.002,     // heartc1
    0.002      // horse1
};
double proben1_adam_beta1 = 0.9;
double proben1_adam_beta2 = 0.999;
double proben1_adam_epsilon = 1e-8;

double proben1_cgm_b_alpha[] = {
    0.1,       // cancer1
    0.1,       // card1
    0.4,       // diabetes1
    0.02,      // gene1
    0.04,      // heart1
    0.1,       // heartc1
    0.05       // horse1
};

double proben1_b_c1 = 0.5;
double proben1_b_amin = 0.0001;

// --------------------------------------------------------------------------------------------------------------------------------------------------

struct proben *const load_proben(const char *const filename) {
    FILE *file = fopen(filename, "r");
    struct proben *const data = (struct proben *const) malloc(sizeof(struct proben));
    if(fscanf(file,
        "bool_in=%d\nreal_in=%d\nbool_out=%d\nreal_out=%d\ntraining_examples=%d\nvalidation_examples=%d\ntest_examples=%d\n",
        &data->bool_in, &data->real_in, &data->bool_out, &data->real_out, &data->training_examples, &data->validation_examples, &data->test_examples
    )) {
        int rows = data->training_examples + data->validation_examples + data->test_examples;
        int cols = data->bool_in + data->real_in + data->bool_out + data->real_out;
        data->size = rows*cols;
        data->data = (double*) malloc(sizeof(double)*data->size);

        if(data->data) {
            for(int i = 0; i < data->size; i++) {
                if(fscanf(file, "%lf", data->data + i) != 1) {
                    dealloc_proben(data);
                    fclose(file);
                    return 0;
                }
            }
            fclose(file);
            return data;
        }
    }
    return 0;
}

int dealloc_proben(struct proben *const data) {
    if(data) {
        free(data->data);
        free(data);
        return 0;
    }
    printf("ERROR: proben_loader | dealloc_proben\n");
    return 1;
}

