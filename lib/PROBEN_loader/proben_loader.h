#ifndef _PROBEN_LOADER
#define _PROBEN_LOADER

    #include <stdio.h>
    #include <stdlib.h>

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // parameters used in PROBEN1 optimization
    #define proben1_num_dbs 7
    
    extern int proben1_number_of_layers;
    extern char *proben1_db_paths[];
    extern char *proben1_db_names[];

    extern double proben1_init_weights;
    extern double proben1_min_valid_error[];
    extern int proben1_batch_sizes[];
    extern int proben1_consecutive_allowed[];

    extern int *proben1_neurons_per_layer;
    extern int proben1_3_layer[][3];
    extern int proben1_4_layer[][4];
    extern int proben1_5_layer[][5];

    extern double proben1_gd_learning_rates[];

    extern double proben1_adam_alphas[];
    extern double proben1_adam_beta1;
    extern double proben1_adam_beta2;
    extern double proben1_adam_epsilon;
    
    extern double proben1_cgm_b_alpha[];

    extern double proben1_b_c1;
    extern double proben1_b_amin;

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct proben {
        int bool_in;                // number of boolean inputs
        int real_in;                // number of floating point inputs (either of bool_in or real_in is zero)
        int bool_out;               // number of boolean outputs
        int real_out;               // number of floating point outputs (either of bool_in or real_in is zero)
        int training_examples;      // number of training examples
        int validation_examples;    // number of validation examples
        int test_examples;          // number of test examples
        int size;                   // number of elements in data array
        double *data;               // array of elements
    };

    // allocate and load proben data to memory
    struct proben *const load_proben(const char *const filename);

    // deallocate memory
    int dealloc_proben(struct proben *const data);

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif