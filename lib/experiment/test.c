#include "exprm.h"


void hidden_to_layers(struct database *const base, int id, int layers[]) {
    layers[0] = base->in_size;
    layers[proben1_hidden_sizes[id]+1] = base->out_size;
    for(int i = 0; i < proben1_hidden_sizes[i]; i++) {
        layers[i+1] = proben1_hidden_layers[id][i];
    }
}

void experiment_proben1(int db_id) {

    if(db_id < proben1_num_dbs) {

        FILE *file = 0;
        char *base_path = "../../rezultati4/methods", temp[256];

        struct database *train, *valid, *test;
        load_proben_database(proben1_db_paths[db_id], &train, &valid, &test);

        int size = proben1_hidden_sizes[db_id] + 2;
        int layers[size];
        hidden_to_layers(train, db_id, layers);

        struct mult_perc *const mlp = alloc_mult_perc(size, layers);
        struct activ_holdr *const a = alloc_activ_holdr(size, layers);

        int seed_count = 10, seeds[] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};

//        struct optim *const opt_gd = alloc_gd_optim(
//            proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
//            proben1_gd_learning_rates[db_id]
//        );      
//        sprintf(temp, "%s/gd/%s/float.out", base_path, proben1_db_names[db_id]);
//        file = fopen(temp, "w");
//        run_optimizer(seed_count, seeds, mlp, a, train, valid, test, opt_gd, file);
//        fclose(file);
//        dealloc_optim(opt_gd);

//        struct optim *const opt_agd = alloc_agd_optim(
//            proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
//            proben1_gss_init_step_sizes[db_id], proben1_gss_convr_tol
//        );
//        sprintf(temp, "%s/agd/%s/float.out", base_path, proben1_db_names[db_id]);
//        file = fopen(temp, "w");
//        run_optimizer(seed_count, seeds, mlp, a, train, valid, test, opt_agd, file);
//        fclose(file);
//        dealloc_optim(opt_agd);

        struct optim *const opt_adam = alloc_adam_optim(
            proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
            proben1_adam_alphas[db_id], proben1_adam_beta1, proben1_adam_beta2, proben1_adam_epsilon
        );
        sprintf(temp, "%s/adam/%s/float.out", base_path, proben1_db_names[db_id]);
        file = fopen(temp, "w");
        run_optimizer(seed_count, seeds, mlp, a, train, valid, test, opt_adam, file);
        fclose(file);
        dealloc_optim(opt_adam);

        struct optim *const opt_cgm = alloc_cgm_optim(
            proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
            proben1_gss_init_step_sizes[db_id], proben1_gss_convr_tol
        );
        sprintf(temp, "%s/cgm/%s/float.out", base_path, proben1_db_names[db_id]);
        file = fopen(temp, "w");
        run_optimizer(seed_count, seeds, mlp, a, train, valid, test, opt_cgm, file);
        fclose(file);
        dealloc_optim(opt_cgm);

        struct optim *const opt_bfgs = alloc_bfgs_optim (
            proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
            proben1_gss_init_step_sizes[db_id], proben1_gss_convr_tol
        );
        sprintf(temp, "%s/bfgs/%s/float.out", base_path, proben1_db_names[db_id]);
        file = fopen(temp, "w");
        run_optimizer(seed_count, seeds, mlp, a, train, valid, test, opt_bfgs, file);
        fclose(file);
        dealloc_optim(opt_bfgs);
        
        dealloc_mult_perc(mlp);
        dealloc_activ_holdr(a);
        dealloc_database(train);
        dealloc_database(valid);
        dealloc_database(test);

    }

}

int main(int argc, char *argv[]) {
    set_num_preserved_bits(52);
    //set_tab_func(10000, -32.0, 32.0);

    //for(int db_id = 0; db_id < proben1_num_dbs; db_id++) {
    //    experiment_proben1(db_id);
    //    printf("DONE: %s\n", proben1_db_names[db_id]);
    //}
    experiment_proben1(8);

    // not tested yet..... takes too much time...
    //"../../db/proben1/mushroom/mushroom1.dt"
    //"../../db/proben1/thyroid/thyroid1.dt"

    //free_tab_func();
    return 0;
}