#ifndef _EXPRM
#define _EXPRM

    #include "../optimizer/optimizer.h"
    #include "../database/database.h"
    #include "../mult_perc/mult_perc.h"


// --------------------------------------------------------------------------------------------------------------------------------------------------

    // seeds: pseudo random generator seeds <- random initializations of mlp
    // mlp: multilayer perceptron
    // databases: training, validation and test set
    // opt: optimizer
    void run_optimizer(
        int seed_count, int seeds[],
        struct mult_perc *const mlp, struct activ_holdr *const a,
        struct database *const train, struct database *const valid, struct database *const test,
        struct optim *const opt,
        FILE *file
    );

// --------------------------------------------------------------------------------------------------------------------------------------------------


#endif