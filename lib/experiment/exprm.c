#include "exprm.h"


void run_optimizer(
    int seed_count, int seeds[],
    struct mult_perc *const mlp, struct activ_holdr *const a,
    struct database *const train, struct database *const valid, struct database *const test,
    struct optim *const opt,
    FILE *file
) {
    char mse_avg[32], mse_std[32];
    char fnr_avg[32], fnr_std[32];
    int num_digits = 4;
    double test_mse[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double test_fnr[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double tot_updates[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double count_mult[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double error;
    int misses;
    for(int i = 0; i < seed_count; i++) {
        srand(seeds[i]);
        init_rand_mult_perc(mlp, 0.1);
        opt->optimizer(mlp, train, valid, opt);
        evaluate_mult_perc(mlp, a, test, &error, &misses);
        statistics(test_mse, i+1, error/test->size);
        statistics(test_fnr, i+1, 100.0*(((double) misses)/test->size));
        statistics(tot_updates, i+1, (double) opt->total_updates);
        statistics(count_mult, i+1, (double) mult_counter);
        printf("%d ", i);
    }
    printf("\n");
    double_format(test_mse[0], mse_avg, num_digits);
    double_format(sqrt(test_mse[1]/(seed_count-1)), mse_std, num_digits);
    double_format(test_fnr[0], fnr_avg, num_digits);
    double_format(sqrt(test_fnr[1]/(seed_count-1)), fnr_std, num_digits);
    fprintf(file, "%s %s %s %s %.1f %.1f %.1f %.1f\n",
        mse_avg, mse_std,
        fnr_avg, fnr_std,
        tot_updates[0], sqrt(tot_updates[1]/(seed_count-1)),
        count_mult[0], sqrt(count_mult[1]/(seed_count-1))
    );
    fflush(file);
}
