#include "mnist_loader.h"


double mnist_init_weights = 1.0;
double mnist_min_valid_error = 0.01;
int mnist_batch_size = 100;
int mnist_consecutive_allowed = 5;

int mnist_number_of_layers = 3;
int mnist_neurons_per_layer[] = {784, 30, 10};

double mnist_gd_learning_rate = 0.03;

double mnist_adam_alpha = 0.005;
double mnist_adam_beta1 = 0.9;
double mnist_adam_beta2 = 0.999;
double mnist_adam_epsilon = 1e-8;

double mnist_b_c1 = 0.05;
double mnist_b_amin = 0.00001;
double mnist_cgm_b_alpha = 0.03;


struct mnist_images *const load_mnist_images(const char *const filename) {
    if(filename) {
        FILE *file = fopen(filename, "rb");
        if(file) {
            struct mnist_images *images = (struct mnist_images*) malloc(sizeof(struct mnist_images));
            if(images) {
                unsigned char buffer[16];
                if(fread(buffer, sizeof(int), 4, file) == 4) {
                    if(2051 == (((int) buffer[0]) << 24 | ((int) buffer[1]) << 16 | ((int) buffer[2]) << 8 | ((int) buffer[3]))) {
                        images->size = ((int) buffer[4]) << 24 | ((int) buffer[5]) << 16 | ((int) buffer[6]) << 8 | ((int) buffer[7]);
                        images->cols = ((int) buffer[8]) << 24 | ((int) buffer[9]) << 16 | ((int) buffer[10]) << 8 | ((int) buffer[11]);
                        images->rows = ((int) buffer[12]) << 24 | ((int) buffer[13]) << 16 | ((int) buffer[14]) << 8 | ((int) buffer[15]);
                        int num_elements = images->size*images->rows*images->cols;
                        images->data = (unsigned char*) malloc(sizeof(unsigned char)*num_elements);
                        if(images->data) {
                            if(num_elements == fread(images->data, sizeof(unsigned char), num_elements, file)) {
                                fclose(file);
                                return images;
                            }
                            free(images->data);
                        }
                    }
                }
                free(images);
            }
            fclose(file);
        }
    }
    printf("ERROR: mnist_loader.c | load_mnist_images\n");
    return 0;
}

int dealloc_mnist_images(struct mnist_images *const images) {
    if(images) {
        free(images->data);
        free(images);
        return 0;
    }
    printf("ERROR: mnist_loader.c | dealloc_mnist_images\n");
    return 1;
}

struct mnist_labels *const load_mnist_labels(const char *const filename) {
    if(filename) {
        FILE *file = fopen(filename, "rb");
        if(file) {
            struct mnist_labels *labels = (struct mnist_labels*) malloc(sizeof(struct mnist_labels));
            if(labels) {
                unsigned char buffer[8];
                if(fread(buffer, sizeof(int), 2, file) == 2) {
                    if(2049 == (((int) buffer[0]) << 24 | ((int) buffer[1]) << 16 | ((int) buffer[2]) << 8 | ((int) buffer[3]))) {
                        labels->size = ((int) buffer[4]) << 24 | ((int) buffer[5]) << 16 | ((int) buffer[6]) << 8 | ((int) buffer[7]);
                        labels->data = (unsigned char*) malloc(sizeof(unsigned char)*labels->size);
                        if(labels->data) {
                            if(labels->size == fread(labels->data, sizeof(unsigned char), labels->size, file)) {
                                fclose(file);
                                return labels;
                            }
                            free(labels->data);
                        }
                    }
                }
                free(labels);
            }
            fclose(file);
        }
    }
    printf("ERROR: mnist_loader.c | load_mnist_labels\n");
    return 0;
}

int dealloc_mnist_labels(struct mnist_labels *const labels) {
    if(labels) {
        free(labels->data);
        free(labels);
        return 0;
    }
    printf("ERROR: mnist_loader.c | dealloc_mnist_labels\n");
    return 1;
}
