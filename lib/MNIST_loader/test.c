#include "mnist_loader.h"


void test_mnist_loader(struct mnist_images *const images, struct mnist_labels *const labels, int n) {

    if(images && labels && n < images->size && n < labels->size) {
        char filename[64];
        sprintf(filename, "record_%d_label_%d.pgm", n, labels->data[n]);
        FILE *file = fopen(filename, "w");
        if(file) {
            int num_px = images->rows*images->cols;
            char buffer[(num_px << 2) + 1], f1[] = "%3u\n", f2[] = "%3u ";
            for(int i = 0; i < num_px; i++) {
                sprintf(buffer + (i << 2), (i%images->cols == (images->cols - 1) ? f1 : f2), images->data[n*num_px + i]);
            }
            fprintf(file, "P2\n%d %d\n255\n%s", images->cols, images->rows, buffer);
            printf("write to file: %s\n", filename);
            fclose(file);
        }
    } else {
        printf("ERROR: test.c | test_mnist_loader\n");
    }
    
}

int main(int argc, char *argv[]) {

    char images_filename[] = "../../db/MNIST_unzipped/train-images.idx3-ubyte";
    char labels_filename[] = "../../db/MNIST_unzipped/train-labels.idx1-ubyte";

    struct mnist_images *const images = load_mnist_images(images_filename);
    struct mnist_labels *const labels = load_mnist_labels(labels_filename);

    if(images && labels) {
        printf("mnist_images:\n");
        printf(" size: %d\n", images->size);
        printf(" rows: %d\n", images->rows);
        printf(" cols: %d\n", images->cols);

        printf("mnist_labels:\n");
        printf(" size: %d\n", labels->size);    

        for(int i = 1; i < argc; i++) {
            test_mnist_loader(images, labels, atoi(argv[i]));
        }

        dealloc_mnist_images(images);
        dealloc_mnist_labels(labels);
    } else {
        printf("ERROR: test.c | main\n");
    }

    return 0;
}