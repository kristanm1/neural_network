#ifndef _MNIST_LOADER
#define _MNIST_LOADER

    #include <stdio.h>
    #include <stdlib.h>

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // parameters used in PROBEN1 optimization
    extern double mnist_init_weights;
    extern double mnist_min_valid_error;
    extern int mnist_batch_size;
    extern int mnist_consecutive_allowed;

    extern int mnist_number_of_layers;
    extern int mnist_neurons_per_layer[];

    extern double mnist_gd_learning_rate;

    extern double mnist_adam_alpha;
    extern double mnist_adam_beta1;
    extern double mnist_adam_beta2;
    extern double mnist_adam_epsilon;

    extern double mnist_b_c1;
    extern double mnist_b_amin;
    extern double mnist_cgm_b_alpha;

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct mnist_images {
        int size;               // number of records
        int rows;               // number of rows per records image
        int cols;               // number of columns per records image
        unsigned char *data;    // mnist images
    };

    // allocate and load mnist images to memory
    struct mnist_images *const load_mnist_images(const char *const filename);

    // deallocate memory
    int dealloc_mnist_images(struct mnist_images *const images);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct mnist_labels {
        int size;               // number of records
        unsigned char *data;    // mnist labels
    };

    // allocate and load mnist labels to memory
    struct mnist_labels *const load_mnist_labels(const char *const filename);

    // deallocate memory
    int dealloc_mnist_labels(struct mnist_labels *const labels);

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif
