#include "utils.h"


void test_dec_to_hex_bin(int argc, char *argv[]) {
    for(int i = 1; i < argc; i++) {
        int dec = atoi(argv[i]);
        char *const hex = dec_to_hex(dec);
        char *const bin = dec_to_bin(dec);

        printf("i: %d\n", i);
        printf("dec: %d\n", dec);
        printf("hex: %s\n", hex);
        printf("bin: %s\n", bin);
        printf("\n");

        free(hex);
        free(bin);
    }
}

void test_ilm(int argc, char *argv[]) {
    if(argc > 2) {
        unsigned long a = (unsigned long) atoi(argv[1]);
        unsigned long b = (unsigned long) atoi(argv[2]);

        printf("%lu, %lu\n", a*b, ilm_1corr(a, b));
        printf("%lu, %lu\n", a*b, ilm_2corr(a, b));
    }
}

void test_double_pointer_argmax(int argc, char *argv[]) {
    int size = argc - 1;
    printf("size: %d\n", size);
    if(size > 0) {
        double array[size];
        for(int i = 0; i < size; i++) {
            array[i] = atof(argv[i+1]);
            printf("%g ", array[i]);
        }
        int argmax = double_pointer_argmax(array, size);
        printf("\nargmax: %d [%g]\n", argmax, array[argmax]);
    }
}

void test_logistic_function() {
    double x;
    x = -1.0;
    printf("f(%g): %g\n", x, logistic_function(x));
    x = 0.0;
    printf("f(%g): %g\n", x, logistic_function(x));
    x = 1.0;
    printf("f(%g): %g\n", x, logistic_function(x));
}

void test_gen_proben_paths() {
    char *doc_name = "../../db/proben1";
    char *db_name = "diabetes";
    char *dst = (char*) malloc(sizeof(char)*3*MAX_FILE_PATH_LENGTH);
    gen_proben_paths(doc_name, db_name, dst);
    printf("%s\n", dst);
    printf("%s\n", dst + MAX_FILE_PATH_LENGTH);
    printf("%s\n", dst + 2*MAX_FILE_PATH_LENGTH);
    free(dst);
}

void test_statistics() {

    int n = 8;
    double a[] = {10, 12, 23, 23, 16, 23, 21, 16}, temp;
    double stat_a[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};

    temp = 0;
    for(int i = 0; i < n; i++) {
        temp += a[i];
        statistics(stat_a, i+1, a[i]);
    }
    double avg = temp/n;
    temp = 0;
    for(int i = 0; i < n; i++) {
        double temp2 = a[i] - avg;
        temp += temp2*temp2;
    }
    double std = sqrt(temp/(n-1));
    printf("avg: %f | %f\n", avg, stat_a[0]);
    printf("std: %f | %f\n", std, sqrt(stat_a[1]/(n-1)));
    
}

int main(int argc, char *argv[]) {

    //test_dec_to_hex_bin(argc, argv);
    //test_ilm(argc, argv);
    //test_double_pointer_argmax(argc, argv);
    //test_logistic_function();
    //test_gen_proben_paths();
    //test_stat();

    return 0;
}