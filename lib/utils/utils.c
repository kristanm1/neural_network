#include "utils.h"


// number of multiplication during optimization
long mult_counter;
double mult_stat[2];

void reset_mult_stat() {
    mult_counter = 0L;
    mult_stat[0] = 1.0/0.0;
    mult_stat[1] = -1.0/0.0;
}

double mul(double a, double b) {
    double c = a*b;
    if(c < mult_stat[0]) {
        mult_stat[0] = c;
    }
    if(mult_stat[1] < c) {
        mult_stat[1] = c;
    }
    mult_counter++;
    return c;
}

char *const dec_to_hex(unsigned long num) {
    char *const hex = (char*) malloc(sizeof(char)*19);
    if(hex) {
        hex[0] = '0';
        hex[1] = 'x';
        hex[18] = '\0';
        for(int i = 17; i >= 2; i--) {
            int temp = num & 0xf;
            hex[i] = temp + ((temp > 9) ? 'A' - 10 : '0');
            num >>= 4;
        }    
        return hex;
    }
    printf("ERROR: utils.c | dec_to_bin\n");
    return 0;
}

char *const dec_to_bin(unsigned long num) {
    char *const bin = (char*) malloc(sizeof(char)*65);
    if(bin) {
        bin[64] = '\0';
        for(int i = 63; i >= 0; i--) {
            bin[i] = (num&1) + '0';
            num >>= 1;
        }
        return bin;
    }
    printf("ERROR: utils.c | dec_to_bin\n");
    return 0;
}

int gen_proben_paths(char *doc_name, char *db_name, char *dst) {
    if(doc_name && db_name && dst) {
        sprintf(dst, "%s/%s/%s1.dt", doc_name, db_name, db_name);
        sprintf(dst + MAX_FILE_PATH_LENGTH, "%s/%s/%s2.dt", doc_name, db_name, db_name);
        sprintf(dst + 2*MAX_FILE_PATH_LENGTH, "%s/%s/%s3.dt", doc_name, db_name, db_name);
        return 1;
    }
    printf("ERROR: utils.c | gen_proben_file_paths\n");
    return 0;
}

void statistics(double *a, int n, double x) {
    double temp = a[0] + (x - a[0])/n;
    a[1] += (x - temp)*(x - a[0]);
    a[0] = temp;
    if(x < a[2]) {
        a[2] = x;
    }
    if(a[3] < x) {
        a[3] = x;
    }
}

int mkdir_ifnex(char *path) {
    struct stat buf;
    if(stat(path, &buf) != 0) {
        mkdir(path, S_IRWXU | S_IRGRP | S_IXGRP);
        return 1;
    }
    return -1;
}

void double_format(double x, char *a, int num_digits) {
    int sign;
    if(x < 0.0) {
        sign = -1;
        x = -x;
    } else if(x == 0.0) {
        sprintf(a, "0.0");
        return;
    } else {
        sign = 1;
    }
    int n = sprintf(a, "%.8f", x);
    int found_dot = 0, found_nonzero = 0, fraction_part = 0, number_count = 0;
    for(int i = 0; i < n; i++) {
        if(found_nonzero == 0 && a[i] > '0' && a[i] <= '9') {
            found_nonzero = 1;
        }
        if(found_dot == 0 && a[i] == '.') {
            found_dot = 1;
        } else {
            if(found_nonzero == 1) {
                number_count++;
            }
            if(found_dot == 1) {
                fraction_part++;
            }
        }
        if(number_count == num_digits) {
            break;
        }
    }
    if(sign == 1) {
        sprintf(a, "%.*f", fraction_part, x);
    } else {
        sprintf(a, "-%.*f", fraction_part, x);
    }
}

unsigned long ma(unsigned long n1, unsigned long n2) {
    if(n1 == 0L || n2 == 0L) {
        return 0L;
    }
    int k1, k2;
    unsigned long temp;
    // approximate n1*n2
    k1 = 0; temp = n1;
    while((temp >>= 1) > 0L) {
        k1++;
    }
    k2 = 0; temp = n2;
    while((temp >>= 1) > 0L) {
        k2++;
    }
    n1 <<= (64 - k1);
    n2 <<= (64 - k2);
    temp = n1 + n2;
    if(temp > n1) {
        return (1L << (k1 + k2)) | (temp >> (64 - k1 - k2));
    } else {
        return (1L << (k1 + k2 + 1)) | (temp >> (63 - k1 - k2));
    }
}

unsigned long ilm_1corr(unsigned long n1, unsigned long n2) {
    if(n1 == 0L || n2 == 0L) {
        return 0L;
    }
    int k1, k2;
    unsigned long p, temp;
    // approximate n1*n2
    k1 = 0; temp = n1;
    while((temp >>= 1) > 0L) {
        k1++;
    }
    k2 = 0; temp = n2;
    while((temp >>= 1) > 0L) {
        k2++;
    }
    n1 ^= (1L << k1);
    n2 ^= (1L << k2);
    p = (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);
    // 1st correction iteration
    if(n1 > 0L && n2 > 0L) {
        k1 = 0; temp = n1;
        while((temp >>= 1) > 0L) {
            k1++;
        }
        k2 = 0; temp = n2;
        while((temp >>= 1) > 0L) {
            k2++;
        }
        n1 ^= (1L << k1);
        n2 ^= (1L << k2);
        p += (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);
    }
    return p;
}

unsigned long ilm_2corr(unsigned long n1, unsigned long n2) {
    if(n1 == 0L || n2 == 0L) {
        return 0L;
    }
    // approximate n1*n2
    int k1, k2;
    unsigned long p, temp;
    k1 = 0; temp = n1;
    while((temp >>= 1) > 0L) {
        k1++;
    }
    k2 = 0; temp = n2;
    while((temp >>= 1) > 0L) {
        k2++;
    }
    n1 ^= (1L << k1);
    n2 ^= (1L << k2);
    p = (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);
    // 1st correction iteration
    if(n1 > 0L && n2 > 0L) {
        k1 = 0; temp = n1;
        while((temp >>= 1) > 0L) {
            k1++;
        }
        k2 = 0; temp = n2;
        while((temp >>= 1) > 0L) {
            k2++;
        }
        n1 ^= (1L << k1);
        n2 ^= (1L << k2);
        p += (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);
        // 2nd correction iteration
        if(n1 > 0L && n2 > 0L) {
            k1 = 0; temp = n1;
            while((temp >>= 1) > 0L) {
                k1++;
            }
            k2 = 0; temp = n2;
            while((temp >>= 1) > 0L) {
                k2++;
            }
            n1 ^= (1L << k1);
            n2 ^= (1L << k2);
            p += (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);
        }
    }
    return p;
}

int double_pointer_argmax(double array[], int size) {
    int max = 0;
    double val = array[0];
    for(int i = 1; i < size; i++) {
        if(val < array[i]) {
            max = i;
            val = array[i];
        }
    }
    return max;
}

int long_pointer_argmax(long array_q[], int size) {
    int max = 0;
    long val = array_q[0];
    for(int i = 1; i < size; i++) {
        if(val < array_q[i]) {
            max = i;
            val = array_q[i];
        }
    }
    return max;
}

double logistic_function(double x) {
    return 1/(1 + exp(-x));
}

double logistic_function_derivative(double x) {
    double y = 1/(1 + exp(-x));
    return y*(1 - y);
}

double ReLU_function(double x) {
    return (x > 0.0) ? x : 0.0;
}

double ReLU_function_derivative(double x) {
    return (x > 0.0) ? 1.0 : 0.0;
}
