#ifndef _UTILS
#define _UTILS

    #include <math.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <sys/stat.h>


    extern long mult_counter2;
    extern double mult_stat[2];
    void reset_mult_stat();
    double mul(double a, double b);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // return pointer to string containing hexadecimal value of integer
    char *const dec_to_hex(unsigned long num);

    // return pointer to string containing binary value of integer
    char *const dec_to_bin(unsigned long num);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    #define MAX_FILE_PATH_LENGTH 120

    int gen_proben_paths(char *doc_name, char *db_name, char *dst);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // a is pointer to double[4]
    // a[0] <- avg
    // a[1] <- ((n-1)*std)^2 ... after last update: sqrt(a[1]/(n-1))
    // a[2] <- min
    // a[3] <- max
    void statistics(double *a, int n, double x);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // make directory in folder path with dir_name if dir_name does not exist
    // path: ./folder1/
    // dir_name: container/
    // creates directory container in folder ./folder1/
    int mkdir_ifnex(char *path);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    void double_format(double x, char *a, int num_digits);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // Mitchell algoritm multiplication
    unsigned long ma(unsigned long n1, unsigned long n2);

    // iterative logarithmic multiplication with one correction term
    unsigned long ilm_1corr(unsigned long n1, unsigned long n2);

    // iterative logarithmic multiplication with two correction terms: TO-DO!
    unsigned long ilm_2corr(unsigned long n1, unsigned long n2);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // return index of maximum element in array
    int double_pointer_argmax(double array[], int size);

    // return index of maximum element in array
    int long_pointer_argmax(long array_q[], int size);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    double logistic_function(double x);
    double logistic_function_derivative(double x);

    double ReLU_function(double x);
    double ReLU_function_derivative(double x);

#endif
