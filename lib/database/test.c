#include "database.h"

void test_mnist_database(int argc, char *argv[]) {
    char images_filename[] = "../../db/MNIST_unzipped/train-images.idx3-ubyte";
    char labels_filename[] = "../../db/MNIST_unzipped/train-labels.idx1-ubyte";
    struct database *const data = load_mnist_database(images_filename, labels_filename);
    print_inf_database(data);
    
    for(int i = 1; i < argc; i++) {
        int idx = atoi(argv[i]);
        double *x = (double*) get_input_database(data, idx);
        int class = double_pointer_argmax(get_output_database(data, idx), data->out_size);
        printf("class: %d\n", class);
        for(int j = 0; j < 28; j++) {
            for(int k = 0; k < 28; k++) {
                printf("%3d ", (int) (255.0*x[k] + 0.5));
            }
            printf("\n");
            x += 28;
        }
    }

    dealloc_database(data);
}

void test_xor_database() {
    struct database *const data = load_xor_database();

    shuffle_database(data);
    print_rec_database(data);

    dealloc_database(data);
}

struct database *const initialize_test_database(int size, int in_size, int out_size) {
    struct database *const data = alloc_database(size, in_size, out_size);
    int q = size / out_size;
    int r = size % out_size;
    int record = 0;
    for(int i = 0; i < out_size; i++) {
        for(int j = 0; j < ((r > 0 && i < r) ? q + 1  : q); j++) {
            for(int k = 0; k < in_size; k++) {
                data->input[record][k] = record;
            }
            for(int k = 0; k < out_size; k++) {
                data->output[record][k] = 0.0;
            }
            data->output[record][i] = 1.0;
            record++;
        }
    }
    return data;
}

void test_alloc_uniform_subset_database() {
    struct database *const data = initialize_test_database(20, 5, 3);
    shuffle_database(data);
    struct database *const subset = alloc_uniform_class_subset_database(data, 7);
    
    print_rec_database(data);
    printf("\n");
    print_rec_database(subset);

    dealloc_database(data);
    dealloc_database(subset);
}

void test_split_uniform_class_subset_database() {

    struct database *const data = initialize_test_database(123457, 5, 3);
    shuffle_database(data);

    struct database *subset1 = 0;
    struct database *subset2 = 0;
    split_uniform_class_subset_database(data, &subset1, &subset2, 0.7);
    
    int flags[data->size];
    for(int i = 0; i < data->size; i++) {
        flags[i] = 0;
    }

    int count_class[data->out_size][2];
    for(int i = 0; i < data->out_size; i++) {
        count_class[i][0] = 0;
        count_class[i][1] = 0;
    }

    for(int i = 0; i < subset1->size; i++) {
        const double *const x = get_input_database(subset1, i);
        const double *const y = get_output_database(subset1, i);
        flags[(int) x[0]]++;
        int class = double_pointer_argmax(y, subset1->out_size);
        count_class[class][0]++;
    }

    for(int i = 0; i < subset2->size; i++) {
        const double *const x = get_input_database(subset2, i);
        const double *const y = get_output_database(subset2, i);
        flags[(int) x[0]]++;
        int class = double_pointer_argmax(y, subset2->out_size);
        count_class[class][1]++;
    }

    for(int i = 0; i < data->out_size; i++) {
        printf("%d: %3d %3d\n", i, count_class[i][0], count_class[i][1]);
    }
    printf("\n");

    for(int i = 0; i < data->size; i++) {
        if(flags[i] != 1) {
            printf("%d: not found\n", i);
        }
    }


    dealloc_database(data);
    dealloc_database(subset1);
    dealloc_database(subset2);

}

void test_split_mnist_database() {
    char images_filename[] = "../../db/MNIST_unzipped/train-images.idx3-ubyte";
    char labels_filename[] = "../../db/MNIST_unzipped/train-labels.idx1-ubyte";

    //char images_filename[] = "../../db/MNIST_unzipped/t10k-images.idx3-ubyte";
    //char labels_filename[] = "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte";

    struct database *const data = load_mnist_database(images_filename, labels_filename);
    shuffle_database(data);
    struct database *const subset = alloc_uniform_class_subset_database(data, 8000);
    dealloc_database(data);

    struct database *subset1 = 0;
    struct database *subset2 = 0;
    shuffle_database(subset);
    split_uniform_class_subset_database(subset, &subset1, &subset2, 0.8);
    dealloc_database(subset);


    int record = 0;
    const double *const x = get_input_database(subset2, record);
    const double *const y = get_output_database(subset2, record);
    int k = 0;
    for(int i = 0; i < 28; i++) {
        for(int j = 0; j < 28; j++) {
            printf("%3d ", (int) (255.0*x[k] + 0.5));
            k++;
        }
        printf("\n");
    }
    printf("class: %d\n", double_pointer_argmax(y, 10));

    print_class_database(subset1);
    print_class_database(subset2);
    
    dealloc_database(subset1);
    dealloc_database(subset2);

}

void test_proben() {
    //char *filename = "../../db/proben1/glass/glass1.dt"; // bool out
    char *filename = "../../db/proben1/flare/flare1.dt"; // real out
    struct database *train_data, *valid_data, *test_data;
    load_proben_database(filename, &train_data, &valid_data, &test_data);

    struct database *data = train_data;

    printf("train data size: %d\n", train_data->size);
    printf("valid data size: %d\n", valid_data->size);
    printf(" test data size: %d\n", test_data->size);

    int p = 2;
    const double *const x = get_input_database(data, p);
    const double *const y = get_output_database(data, p);
    for(int i = 0; i < data->in_size; i++) {
        printf("%f ", x[i]);
    }
    printf(" ->  ");
    for(int i = 0; i < data->out_size; i++) {
        printf("%f ", y[i]);
    }
    printf("\n");

    dealloc_database(train_data);
    dealloc_database(valid_data);
    dealloc_database(test_data);
}

int main(int argc, char *argv[]) {

    //test_mnist_database(argc, argv);
    //test_xor_database();
    //test_alloc_uniform_subset_database();
    //test_split_uniform_class_subset_database();
    //test_split_mnist_database();
    //test_proben();

    return 0;
}