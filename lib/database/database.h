#ifndef _DATABASE
#define _DATABASE

    #include "../utils/utils.h"
    #include "../MNIST_loader/mnist_loader.h"
    #include "../PROBEN_loader/proben_loader.h"


    struct database {
        int size;           // number of records (record is pair of vectors: [input vector, output vector])
        int in_size;        // size of input vector
        int out_size;       // size of output vector
        int *order;         // record order
        double *array;      // data
        double **input;     // input vectors (pointers to data)
        double **output;    // output vectors (pointers to data)
    };

    struct database *alloc_database(int size, int in_size, int out_size);
    struct database *load_mnist_database(char images_filename[], char labels_filename[]);
    void load_proben_database(char filename[], struct database **train_data, struct database **valid_data, struct database **test_data);
    struct database *load_xor_database();
    struct database *alloc_uniform_class_subset_database(struct database *data, int subset_size);
    void split_uniform_class_subset_database(struct database *data, struct database **subset1, struct database **subset2, double subset1_subset2_ratio);
    void dealloc_database(struct database *data);
    double *get_input_database(struct database *data, int rec);
    double *get_output_database(struct database *data, int rec);
    void print_inf_database(struct database *data);
    void print_class_database(struct database *data);
    void print_rec_database(struct database *data);
    void shuffle_database(struct database *data);

#endif