#include "database.h"


struct database *alloc_database(int size, int in_size, int out_size) {
    struct database *data = (struct database*) malloc(sizeof(struct database));
    data->size = size;
    data->in_size = in_size;
    data->out_size = out_size;
    data->order = (int*) malloc(sizeof(int)*size);
    data->array = (double*) malloc(sizeof(double)*size*(in_size + out_size));
    data->input = (double**) malloc(sizeof(double*)*2*size);
    data->output = data->input + size;
    int offset_in = 0;
    int offset_out = size * in_size;
    for(int i = 0; i < size; i++) {
        data->order[i] = i;
        data->input[i] = data->array + offset_in;
        data->output[i] = data->array + offset_out;
        offset_in += in_size;
        offset_out += out_size;
    }
    return data;
}

struct database *load_mnist_database(char images_filename[], char labels_filename[]) {
    struct mnist_images *images = load_mnist_images(images_filename);
    struct mnist_labels *labels = load_mnist_labels(labels_filename);
    struct database *data = alloc_database(images->size, images->rows*images->cols, 10);
    for(int i = 0; i < data->size; i++) {
        unsigned char *input = images->data + i*data->in_size;
        for(int j = 0; j < data->in_size; j++) {
            data->input[i][j] = ((double) input[j]) / 255.0;
        }
        int label = (int) labels->data[i];
        for(int j = 0; j < data->out_size; j++) {
            data->output[i][j] = (label == j) ? 0.9 : 0.1;
        }
    }
    free(images);
    free(labels);
    return data;
}

void load_proben_database(char filename[], struct database **train_data, struct database **valid_data, struct database **test_data) {
    struct proben *const pdata = load_proben(filename);
    int in_size = pdata->bool_in + pdata->real_in;
    int out_size = pdata->bool_out + pdata->real_out;
    *train_data = alloc_database(pdata->training_examples, in_size, out_size);
    *valid_data = alloc_database(pdata->validation_examples, in_size, out_size);
    *test_data = alloc_database(pdata->test_examples, in_size, out_size);
    int p = 0;
    for(int i = 0; i < pdata->training_examples; i++) {
        for(int j = 0; j < in_size; j++) {
            (*train_data)->input[i][j] = pdata->data[p++];
        }
        if(pdata->bool_out > 0) {
            for(int j = 0; j < out_size; j++) {
                (*train_data)->output[i][j] = (pdata->data[p++] == 1.0) ? 0.9 : 0.1;
            }
        } else {
            for(int j = 0; j < out_size; j++) {
                (*train_data)->output[i][j] = pdata->data[p++];
            }
        }
    }
    for(int i = 0; i < pdata->validation_examples; i++) {
        for(int j = 0; j < in_size; j++) {
            (*valid_data)->input[i][j] = pdata->data[p++];
        }
        if(pdata->bool_out > 0) {
            for(int j = 0; j < out_size; j++) {
                (*valid_data)->output[i][j] = (pdata->data[p++] == 1.0) ? 0.9 : 0.1;
            }
        } else {
            for(int j = 0; j < out_size; j++) {
                (*valid_data)->output[i][j] = pdata->data[p++];
            }
        }
    }
    for(int i = 0; i < pdata->test_examples; i++) {
        for(int j = 0; j < in_size; j++) {
            (*test_data)->input[i][j] = pdata->data[p++];
        }
        if(pdata->bool_out > 0) {
            for(int j = 0; j < out_size; j++) {
                (*test_data)->output[i][j] = (pdata->data[p++] == 1.0) ? 0.9 : 0.1;
            }
        } else {
            for(int j = 0; j < out_size; j++) {
                (*test_data)->output[i][j] = pdata->data[p++];
            }
        }
    }
}

struct database *load_xor_database() {
    struct database *const data = alloc_database(4, 2, 1);
    if(data) {
        data->input[0][0] = 0.0; data->input[0][1] = 0.0; data->output[0][0] = 0.1;
        data->input[1][0] = 0.0; data->input[1][1] = 1.0; data->output[1][0] = 0.9;
        data->input[2][0] = 1.0; data->input[2][1] = 0.0; data->output[2][0] = 0.9;
        data->input[3][0] = 1.0; data->input[3][1] = 1.0; data->output[3][0] = 0.1;
        return data;
    }
    printf("ERROR: database.c | load_xor_database\n");
    return 0;
}

struct database *alloc_uniform_class_subset_database(struct database *data, int subset_size) {
    if(data && subset_size > 0 && subset_size < data->size) {
        int distribution[data->out_size][2];
        int q = subset_size / data->out_size;
        int r = subset_size % data->out_size;
        for(int i = 0; i < data->out_size; i++) {
            distribution[i][0] = (r > 0 && i < r) ? q + 1 : q;
            distribution[i][1] = 0;
        }
        for(int i = 0; i < data->size; i++) {
            double *y = get_output_database(data, i);
            int class = double_pointer_argmax(y, data->out_size);
            distribution[class][1]++;
        }
        int count_records = 0;
        for(int i = 0; i < data->out_size; i++) {
            if(distribution[i][0] > distribution[i][1]) {
                distribution[i][0] = distribution[i][1];
            }
            count_records += distribution[i][0];
        }
        struct database *subset = alloc_database(count_records, data->in_size, data->out_size);
        if(subset) {
            int data_record = 0;
            int subset_record = 0;
            while(subset_record < subset->size && data_record < data->size) {
                double *y = get_output_database(data, data_record);
                int class = double_pointer_argmax(y, data->out_size);
                if(distribution[class][0] > 0) {
                    double *x = get_input_database(data, data_record);
                    for(int i = 0; i < data->in_size; i++) {
                        subset->input[subset_record][i] = x[i];
                    }
                    for(int i = 0; i < data->out_size; i++) {
                        subset->output[subset_record][i] = y[i];
                    }
                    subset_record++;
                    distribution[class][0]--;
                }
                data_record++;
            }
            return subset;
        }
    }
    printf("ERROR: database.c | alloc_uniform_class_subset_database\n");
    return 0;
}

void split_uniform_class_subset_database(struct database *data, struct database **subset1, struct database **subset2, double subset1_subset2_ratio) {
    *subset1 = (struct database*) alloc_uniform_class_subset_database(data, (int) ((data->size * subset1_subset2_ratio) + 0.5));
    int count_class[data->out_size];
    for(int i = 0; i < data->out_size; i++) {
        count_class[i] = 0;
    }
    for(int i = 0; i < (*subset1)->size; i++) {
        double *y = get_output_database(*subset1, i);
        int class = double_pointer_argmax(y, data->out_size);
        count_class[class]++;
    }
    *subset2 = (struct database*) alloc_database(data->size - (*subset1)->size, data->in_size, data->out_size);
    int record = 0;
    for(int i = 0; i < data->size; i++) {
        double *y = get_output_database(data, i);
        int class = double_pointer_argmax(y, data->out_size);
        if(count_class[class] > 0) {
            count_class[class]--;
        } else {
            double *x = get_input_database(data, i);
            for(int j = 0; j < data->in_size; j++) {
                (*subset2)->input[record][j] = x[j];
            }
            for(int j = 0; j < data->out_size; j++) {
                (*subset2)->output[record][j] = y[j];
            }
            record++;
        }
    }
}

void dealloc_database(struct database *data) {
    free(data->order);
    free(data->array);
    free(data->input);
    free(data);
}

double *get_input_database(struct database *data, int rec) {
    return data->input[data->order[rec]];
}

double *get_output_database(struct database *data, int rec) {
    return data->output[data->order[rec]];
}

void print_inf_database(struct database *data) {
    printf("database info:\n size: %d\n in_size: %d\n out_size: %d\n", data->size, data->in_size, data->out_size);
}

void print_class_database(struct database *data) {
    int count[data->out_size];
    for(int i = 0; i < data->out_size; i++) {
        count[i] = 0;
    }
    for(int i = 0; i < data->size; i++) {
        count[double_pointer_argmax(get_output_database(data, i), data->out_size)]++;
    }
    printf("class: number of records\n");
    for(int i = 0; i < data->out_size; i++) {
        printf("%2d: %d\n", i, count[i]);
    }
}

void print_rec_database(struct database *data) {
    double *temp;
    for(int i = 0; i < data->size; i++) {
        printf("%2d: %2d | ", i, data->order[i]);
        temp = get_input_database(data, i);
        for(int j = 0; j < data->in_size; j++) {
            printf("%5.2f ", temp[j]);
        }
        printf("| ");
        temp = get_output_database(data, i);
        for(int j = 0; j < data->out_size; j++) {
            printf("%5.2f ", temp[j]);
        }
        printf("\n");
    }
}

void shuffle_database(struct database *data) {
    for(int i = data->size-1; i > 0; i--) {
        int j = rand() % (i + 1);
        if(j < i) {
            int temp = data->order[i];
            data->order[i] = data->order[j];
            data->order[j] = temp;
        }
    }
}
