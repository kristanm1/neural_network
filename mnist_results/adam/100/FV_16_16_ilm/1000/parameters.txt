adam
batch size: 100
consecutive_allowed: 5
alpha: 4.989624e-03
beta1: 8.999939e-01
beta2: 9.989929e-01
epsilon: 1.525879e-05
layers:  784 100 10
tabulated points: 32
