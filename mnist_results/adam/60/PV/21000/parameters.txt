adam
batch size: 100
min valid error: 1.000000e-02
consecutive_allowed: 5
alpha: 5.000000e-03
beta1: 9.000000e-01
beta2: 9.990000e-01
epsilon: 1.000000e-08
layers:  784 60 10
