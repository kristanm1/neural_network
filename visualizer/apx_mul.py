import numpy as np
from matplotlib import pyplot as plt


def ma(n1, n2):
    """
        Mitchell’s algorithm
    """
    n1, n2 = int(n1), int(n2)
    if n1 == 0 or n2 == 0:
        return 0
    k1, k2 = 0, 0
    temp = n1
    while temp > 0:
        temp >>= 1
        k1 += 1 if temp > 0 else 0
    temp = n2
    while temp > 0:
        temp >>= 1
        k2 += 1 if temp > 0 else 0
    n1 = ((n1 ^ (1 << k1)) << (64 - k1))
    n2 = ((n2 ^ (1 << k2)) << (64 - k2))
    temp = n1 + n2
    if temp < (1 << 64):
        return (1 << (k1 + k2)) | (temp >> (64 - k1 - k2))
    else:
        temp ^= (1 << 64)
        return (1 << (k1 + k2 + 1)) | (temp >> (63 - k1 - k2))


def ilm(n1, n2):
    """
        iterative logarithmic multiplier with 1 correction term
    """
    n1, n2 = int(n1), int(n2)
    if n1 == 0 or n2 == 0:
        return 0
    k1, k2 = 0, 0
    temp = n1
    while temp > 0:
        temp >>= 1
        k1 += 1 if temp > 0 else 0
    temp = n2
    while temp > 0:
        temp >>= 1
        k2 += 1 if temp > 0 else 0
    n1 ^= (1 << k1)
    n2 ^= (1 << k2)
    # apx
    p = (1 << (k1 + k2)) + (n1 << k2) + (n2 << k1)
    # 1st corr. term
    if n1 > 0 and n2 > 0:
        k1, k2 = 0, 0
        temp = n1
        while temp > 0:
            temp >>= 1
            k1 += 1 if temp > 0 else 0
        temp = n2
        while temp > 0:
            temp >>= 1
            k2 += 1 if temp > 0 else 0
        n1 ^= (1 << k1)
        n2 ^= (1 << k2)
        p += (1 << (k1 + k2)) + (n1 << k2) + (n2 << k1);
    return p


class Q:

    integer_portion = -1
    fraction_portion = -1
    round_value = -1
    saturation_min = -1
    saturation_max = -1

    def set_portions(integer_portion, fraction_portion):
        if integer_portion > 0 and fraction_portion >= 0 and integer_portion + fraction_portion <= 32:
            Q.integer_portion = integer_portion
            Q.fraction_portion = fraction_portion
            Q.round_value = int(1 << (fraction_portion - 1) if fraction_portion > 0 else 0)
            Q.saturation_max = int(0xFFFFFFFF >> (32 - (integer_portion + fraction_portion - 1)))
            Q.saturation_min = ~Q.saturation_max
        else:
            Q.integer_portion = -1
            Q.fraction_portion = -1
            Q.round_value = -1
            Q.saturation_min = -1
            Q.saturation_max = -1

    def float_to_q(value):
        q = Q(int(value * (1 << Q.fraction_portion)))
        if q.value > Q.saturation_max:
            q.value = Q.saturation_max
        elif q.value < Q.saturation_min:
            q.value = Q.saturation_min
        return q

    def __init__(self, value):
        self.value = value
    
    def to_float(self):
        return float(self.value) / (1 << Q.fraction_portion)

    def mul(self, other):
        t1, s1 = self.value, 1
        t2, s2 = other.value, 1
        if t1 < 0:
            t1, s1 = -t1, -1
        if t2 < 0:
            t2, s2 = -t2, -1
        pro = ((s1*s2) * (t1*t2 + Q.round_value)) >> Q.fraction_portion
        if pro > Q.saturation_max:
            pro = Q.saturation_max
        elif pro < Q.saturation_min:
            pro = Q.saturation_min
        return Q(pro)
    
    def mul_ma(self, other):
        t1, s1 = self.value, 1
        t2, s2 = other.value, 1
        if t1 < 0:
            t1, s1 = -t1, -1
        if t2 < 0:
            t2, s2 = -t2, -1
        pro = ((s1*s2) * (ma(t1, t2) + Q.round_value)) >> Q.fraction_portion
        if pro > Q.saturation_max:
            pro = Q.saturation_max
        elif pro < Q.saturation_min:
            pro = Q.saturation_min
        return Q(pro)
    
    def mul_ilm(self, other):
        t1, s1 = self.value, 1
        t2, s2 = other.value, 1
        if t1 < 0:
            t1, s1 = -t1, -1
        if t2 < 0:
            t2, s2 = -t2, -1
        pro = ((s1*s2) * (ilm(t1, t2) + Q.round_value)) >> Q.fraction_portion
        if pro > Q.saturation_max:
            pro = Q.saturation_max
        elif pro < Q.saturation_min:
            pro = Q.saturation_min
        return Q(pro)


def generate_sequence(init_value, length, multiplier):
    x, y = [1], [init_value]
    q_init, q = Q.float_to_q(init_value), Q.float_to_q(init_value)
    for i in range(1, length):
        x.append(i+1)
        q = multiplier(q, q_init)
        y.append(q.to_float())
    return np.array([x, y])


def subplot_gen_seq(init_value, length, ax):
    d = generate_sequence(init_value, length, Q.mul)
    d_ma = generate_sequence(init_value, length, Q.mul_ma)
    d_ilm = generate_sequence(init_value, length, Q.mul_ilm)
    ax.plot(d[0, :], d[1, :], 'g', label='*')
    ax.plot(d_ma[0, :], d_ma[1, :], 'b', label='ma')
    ax.plot(d_ilm[0, :], d_ilm[1, :], 'r', label='ilm')
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y(x)={:.3f}^x$'.format(init_value))
    ax.legend()
    ax.grid()


if __name__ == '__main__':
    """
    #    http://eprints.fri.uni-lj.si/1751/1/MICPRO2011-An_iterative_logarithmic_multiplier.pdf
    #    v clanku na zgornjem naslovu:
    #    Example 1(Mitchell’s multiplication of 234 and 198)
    #    Example 2(Proposed multiplication of 234 and 198 with threecorrection terms)
    a, b = 234, 198
    print("  *", a*b)
    print(" ma", ma(a, b))
    print("ilm", ilm(a, b))
    """

    integer_portion, fraction_portion = 3, 20

    Q.set_portions(integer_portion, fraction_portion)

    fig, ((ax11, ax12, ax13), (ax21, ax22, ax23)) = plt.subplots(2, 3)
    subplot_gen_seq(0.999, 5000, ax11)
    subplot_gen_seq(0.995, 700, ax12)
    subplot_gen_seq(0.99, 400, ax13)
    subplot_gen_seq(0.985, 300, ax21)
    subplot_gen_seq(0.98, 220, ax22)
    subplot_gen_seq(0.975, 200, ax23)

    fig.suptitle('Primerjava mnozilnikov')
    fig.set_size_inches(9, 6)
    fig.tight_layout()
    plt.show()

