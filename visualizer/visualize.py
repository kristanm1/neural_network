import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import locale


def load(path):
    with open(path, 'r') as file:
        return np.array([[float(x) for x in line.rstrip().split(' ')] for line in file.readlines()])

def load_mult_stat(method, layers, db, model, seed, base=''):
    return load('/'.join([base, method , str(layers), db, model, str(seed), 'mult_stat.txt']))

def load_mult_stats(method, layers, db, model, seeds, base='../proben1_results'):
    stats = np.zeros((len(seeds), 2))
    for i, seed in enumerate(seeds):
        stats[i, :] = load_mult_stat(method, layers, db, model, seed, base=base)
    return stats

def load_optimization_data(method, layers, db, model, seed):
    if db:
        path = '/'.join(['../proben1_results', method , str(layers), db, model, str(seed)])
    else:
        path = '/'.join(['../mnist_results', method , str(layers), model, str(seed)])
    p1, p2, p3 = path + '/parameters.txt', path + '/evaluations.txt', path + '/act_der_file.txt'
    with open(p1, 'r') as par:
        return par.readlines(), load(p2), load(p3), load(path + '/betas.txt') if method == 'adam' else None


def plot_gd(layers, db, model, seed):
    fig, (ax_mse, ax_mis, ax_act, ax_der, ax_wei) = plt.subplots(1, 5)
    fig.set_size_inches(16, 3)
    fig.set_tight_layout(True)
    if db:
        fig.suptitle('{:s} {:s} {:s}({:d}) {:d}'.format('gd', db, model, layers, seed), fontsize=15.6)
    else:
        if model == 'PV':
            label = 'double'
        else:
            temp = model.split('_')
            label = 'Q{:d}.{:d}'.format(int(temp[1]), int(temp[2]))
            if len(temp) >= 4:
                label += '-{:s}'.format(temp[3])
        fig.suptitle('{:s} {:s}'.format('GD', label), fontsize=15.6)

    par, eval, ad, betas = load_optimization_data('gd', layers, db, model, seed)
    
    x = np.arange(eval.shape[0])

    valid_err = eval[:, 0]
    ax_mse.set_ylabel(r'$\overline{SE}$', fontsize=15.6)
    ax_mse.plot(x, valid_err, linewidth=1.0)
    ax_mse.tick_params(axis='both', labelsize=15)

    valid_mis = eval[:, 1]
    ax_mis.set_ylabel(r'$\overline{FNR}$', fontsize=15.6)
    ax_mis.plot(x, valid_mis, linewidth=1.0)
    ax_mis.tick_params(axis='both', labelsize=15)

    wei_mse, wei_std = eval[:, 2], eval[:, 3]
    ax_wei.set_ylabel(r'$\overline{uteži}$', fontsize=15.6)
    ax_wei.plot(x, wei_mse, linewidth=1.0)
    ax_wei.fill_between(x, wei_mse-wei_std, wei_mse+wei_std, alpha=0.3)
    ax_wei.tick_params(axis='both', labelsize=15)

    x = np.arange(ad.shape[0])

    act_mse, act_std = ad[:, 0], ad[:, 1]
    ax_act.set_ylabel(r'$\overline{z}$', fontsize=15.6)
    ax_act.plot(x, act_mse, linewidth=1.0)
    ax_act.fill_between(x, act_mse-act_std, act_mse+act_std, alpha=0.3)
    ax_act.tick_params(axis='both', labelsize=15)

    der_mse, der_std = ad[:, 4], ad[:, 5]
    ax_der.set_ylabel(r'$\overline{\nabla}$', fontsize=15.6)
    ax_der.plot(x, der_mse, linewidth=1.0)
    ax_der.fill_between(x, der_mse-der_std, der_mse+der_std, alpha=0.3)
    ax_der.tick_params(axis='both', labelsize=15)


def plot_adam(layers, db, model, seed):
    
    fig, (ax_mse, ax_mis, ax_act, ax_bet, ax_der, ax_wei) = plt.subplots(1, 6)
    fig.set_size_inches(17, 3)
    fig.set_tight_layout(True)
    if db:
        fig.suptitle('{:s} {:s} {:s}({:d}) {:d}'.format('adam', db, model, layers, seed), fontsize=14)
    else:
        if model == 'PV':
            label = 'double'
        else:
            temp = model.split('_')
            label = 'Q{:d}.{:d}'.format(int(temp[1]), int(temp[2]))
            if len(temp) >= 4:
                label += '-{:s}'.format(temp[3])
        fig.suptitle('{:s} {:s}'.format('ADAM', label), fontsize=15.6)

    par, eval, ad, betas = load_optimization_data('adam', layers, db, model, seed)

    x = np.arange(eval.shape[0])

    valid_err = eval[:, 0]
    ax_mse.set_ylabel(r'$\overline{SE}$', fontsize=15.6)
    ax_mse.plot(x, valid_err, linewidth=1.0)

    valid_mis = eval[:, 1]
    ax_mis.set_ylabel(r'$\overline{FNR}$', fontsize=15.6)
    ax_mis.plot(x, valid_mis, linewidth=1.0)

    wei_mse, wei_std = eval[:, 2], eval[:, 3]
    ax_wei.set_ylabel(r'$\overline{uteži}$', fontsize=15.6)
    ax_wei.plot(x, wei_mse, linewidth=1.0)
    ax_wei.fill_between(x, wei_mse-wei_std, wei_mse+wei_std, alpha=0.3)

    ax_bet.plot(x, betas[:, 0], label=r'$\beta_1$')
    ax_bet.plot(x, betas[:, 1], label=r'$\beta_2$')
    ax_bet.legend(loc='best', fontsize=11)

    x = np.arange(ad.shape[0])

    act_mse, act_std = ad[:, 0], ad[:, 1]
    ax_act.set_ylabel(r'$\overline{z}$', fontsize=15.6)
    ax_act.plot(x, act_mse, linewidth=1.0)
    ax_act.fill_between(x, act_mse-act_std, act_mse+act_std, alpha=0.3)

    der_mse, der_std = ad[:, 4], ad[:, 5]
    ax_der.set_ylabel(r'$\overline{\nabla}$', fontsize=15.6)
    ax_der.plot(x, der_mse, linewidth=1.0)
    ax_der.fill_between(x, der_mse-der_std, der_mse+der_std, alpha=0.3)


def plot_cgm(layers, db, model, seed):

    fig, (ax_mse, ax_mis, ax_act, ax_der, ax_wei) = plt.subplots(1, 5)
    fig.set_size_inches(16, 3)
    fig.set_tight_layout(True)
    if db:
        fig.suptitle('{:s} {:s} {:s}({:d}) {:d}'.format('cgm', db, model, layers, seed), fontsize=14)
    else:
        if model == 'PV':
            label = 'double'
        else:
            temp = model.split('_')
            label = 'Q{:d}.{:d}'.format(int(temp[1]), int(temp[2]))
            if len(temp) >= 4:
                label += '-{:s}'.format(temp[3])
        fig.suptitle('{:s} {:s}'.format('CGM', label), fontsize=15.6)

    par, eval, ad, betas = load_optimization_data('cgm', layers, db, model, seed)
    
    x = np.arange(eval.shape[0])

    valid_err = eval[:, 0]
    ax_mse.set_ylabel(r'$SE$', fontsize=15.6)
    ax_mse.plot(x, valid_err, linewidth=1.0)

    valid_mis = eval[:, 1]
    ax_mis.set_ylabel(r'$FNR$', fontsize=15.6)
    ax_mis.plot(x, valid_mis, linewidth=1.0)

    wei_mse, wei_std = eval[:, 2], eval[:, 3]
    ax_wei.set_ylabel(r'$\overline{uteži}$', fontsize=15.6)
    ax_wei.plot(x, wei_mse, linewidth=1.0)
    ax_wei.fill_between(x, wei_mse-wei_std, wei_mse+wei_std, alpha=0.3)

    x = np.arange(ad.shape[0])

    act_mse, act_std = ad[:, 0], ad[:, 1]
    ax_act.set_ylabel(r'$\overline{z}$', fontsize=15.6)
    ax_act.plot(x, act_mse, linewidth=1.0)
    ax_act.fill_between(x, act_mse-act_std, act_mse+act_std, alpha=0.3)

    der_mse, der_std = ad[:, 4], ad[:, 5]
    ax_der.set_ylabel(r'$\overline{\nabla}$', fontsize=15.6)
    ax_der.plot(x, der_mse, linewidth=1.0)
    ax_der.fill_between(x, der_mse-der_std, der_mse+der_std, alpha=0.3)


def plot_bfgs(layers, db, model, seed):

    fig, (ax_mse, ax_mis, ax_act, ax_der, ax_wei) = plt.subplots(1, 5)
    fig.set_size_inches(16, 3)
    fig.set_tight_layout(True)
    fig.suptitle('{:s} {:s} {:s}({:d}) {:d}'.format('bfgs', db, model, layers, seed), fontsize=14)

    par, eval, ad, betas = load_optimization_data('bfgs', layers, db, model, seed)
    
    x = np.arange(eval.shape[0])

    valid_err = eval[:, 0]
    ax_mse.set_ylabel(r'$SE$', fontsize=14)
    ax_mse.plot(x, valid_err, linewidth=1.0)

    valid_mis = eval[:, 1]
    ax_mis.set_ylabel(r'$FNR$', fontsize=14)
    ax_mis.plot(x, valid_mis, linewidth=1.0)

    wei_mse, wei_std = eval[:, 2], eval[:, 3]
    ax_wei.set_ylabel(r'$\overline{uteži}$')
    ax_wei.plot(x, wei_mse, linewidth=1.0)
    ax_wei.fill_between(x, wei_mse-wei_std, wei_mse+wei_std, alpha=0.3)

    x = np.arange(ad.shape[0])

    act_mse, act_std = ad[:, 0], ad[:, 1]
    ax_act.set_ylabel(r'$\overline{z}$')
    ax_act.plot(x, act_mse, linewidth=1.0)
    ax_act.fill_between(x, act_mse-act_std, act_mse+act_std, alpha=0.3)

    der_mse, der_std = ad[:, 4], ad[:, 5]
    ax_der.set_ylabel(r'$\overline{\nabla}$')
    ax_der.plot(x, der_mse, linewidth=1.0)
    ax_der.fill_between(x, der_mse-der_std, der_mse+der_std, alpha=0.3)


def load_results(method, layers, db, model, seeds):
    ress = []
    for seed in seeds:
        if db:
            path = '/'.join(['../proben1_results', method , str(layers), db, model, str(seed)])
        else:
            path = '/'.join(['../mnist_results', method , str(layers), model, str(seed)])
        with open(path + '/result.txt', 'r') as file:
            ress.append([float(x) for x in file.readline().rstrip().split(' ')])
    return np.array(ress)


def set_axis(ax, xticks, ylabel, yscale, fontsize, rotation):
    ax.set_xticks(xticks)
    ax.set_ylabel(ylabel, fontsize=fontsize)
    ax.set_yscale(yscale)
    if yscale.lower() == 'linear':
        ax.ticklabel_format(axis='y', useLocale=True)
    ax.tick_params(axis='y', labelsize=18)
    ax.yaxis.tick_right()
    #ax.grid(which='major', axis='y', linestyle='--')
    ax.set_yticks([], minor=True)
    ax.set_axisbelow(True)
    ax.set_xticklabels(xticks, rotation=rotation)



def plot_results_method(method, dif_layers, db, models, seeds, ncol=2):

    fig, (ax_mis, ax_epo, ax_mul) = plt.subplots(1, 3)
    fig.set_size_inches(16.5, 2.5)
    
    first = True
    data_model = []
    data_layers = []

    for i, layers in enumerate(dif_layers):
        for j, model in enumerate(models):
            res = load_results(method, layers, db, model, seeds)
            if first:
                epo_data = res[:, 1]
                mis_data = res[:, 3]
                mul_data = res[:, 5]
                first = False
            else:
                epo_data = np.hstack((epo_data, res[:, 1]))
                mis_data = np.hstack((mis_data, res[:, 3]))
                mul_data = np.hstack((mul_data, res[:, 5]))
            temp = model.split('_')
            if len(temp) == 1:
                data_model += ['DOUBLE']*len(seeds)
            elif len(temp) == 3:
                data_model += ['TM']*len(seeds)
            elif len(temp) == 4:
                if temp[3] == 'ilm':
                    data_model += ['ILM']*len(seeds)
                elif temp[3] == 'ma':
                    data_model += ['MPM']*len(seeds)
            data_layers += [i+1]*len(seeds)

    data = pd.DataFrame({
        'Epoh': epo_data,
        'FNR': mis_data,
        'Št. produktov': mul_data,
        'Množilnik': data_model,
        'Arhitektura': data_layers
    })

    sns.set_context("notebook", font_scale=0.8, rc={"lines.linewidth": 1.75})

    sns.boxplot(data=data, x='Množilnik', y='FNR', hue='Arhitektura', ax=ax_mis, showfliers = False)
    sns.boxplot(data=data, x='Množilnik', y='Epoh', hue='Arhitektura', ax=ax_epo, showfliers = False)
    sns.boxplot(data=data, x='Množilnik', y='Št. produktov', hue='Arhitektura', ax=ax_mul, showfliers = False)
    
    ax_epo.set_yscale('log')
    ax_mul.set_yscale('log')

    lsize = 17
    tsize = 14

    xticks = ['DOUBLE', 'TM', 'ILM', 'MPM'] if len(models) == 4 else ['DOUBLE', 'TM', 'ILM']

    ax_mis.tick_params(which='both', labelsize=tsize)
    ax_mis.xaxis.label.set_size(lsize)
    ax_mis.xaxis.set_label_position('top') 
    ax_mis.xaxis.label.set_visible(False)
    ax_mis.yaxis.label.set_size(lsize)
    ax_mis.yaxis.tick_right()
    ax_mis.legend(fontsize=lsize, title='Arhitektura', title_fontsize=lsize, ncol=ncol, loc='center', bbox_to_anchor=(-0.3125, 0.5))

    ax_epo.tick_params(which='both', labelsize=tsize)
    ax_epo.xaxis.label.set_size(lsize)
    ax_epo.xaxis.set_label_position('top')
    ax_epo.xaxis.label.set_visible(False)
    ax_epo.yaxis.label.set_size(lsize)
    ax_epo.yaxis.tick_right()
    ax_epo.get_legend().remove()

    ax_mul.tick_params(which='both', labelsize=tsize)
    ax_mul.xaxis.label.set_size(lsize)
    ax_mul.xaxis.set_label_position('top') 
    ax_mul.xaxis.label.set_visible(False)
    ax_mul.yaxis.label.set_size(lsize)
    ax_mul.yaxis.tick_right()
    ax_mul.get_legend().remove()

    fig.set_tight_layout(True)


def plot_results_model(methods, dif_layers, db, model, seeds, ncol=2):

    fig, (ax_mis, ax_epo, ax_mul) = plt.subplots(1, 3)
    fig.set_size_inches(16.5, 2.5)
    
    first = True
    mis_method = []
    mis_layers = []
    for i, layers in enumerate(dif_layers):
        for j, method in enumerate(methods):
            res = load_results(method, layers, db, model, seeds)
            if first:
                epo_data = res[:, 1]
                mis_data = res[:, 3]
                mul_data = res[:, 5]
                first = False
            else:
                epo_data = np.hstack((epo_data, res[:, 1]))
                mis_data = np.hstack((mis_data, res[:, 3]))
                mul_data = np.hstack((mul_data, res[:, 5]))
            mis_method += [method.upper()]*len(seeds)
            mis_layers += [i+1]*len(seeds)
    
    
    data = pd.DataFrame({
        'Epoh': epo_data,
        'FNR': mis_data,
        'Št. produktov': mul_data,
        'Učni algoritem': mis_method,
        'Arhitektura': mis_layers
    })

    sns.set_context("notebook", font_scale=0.8, rc={"lines.linewidth": 1.75})

    sns.boxplot(data=data, x='Učni algoritem', y='FNR', hue='Arhitektura', ax=ax_mis, showfliers = False)
    sns.boxplot(data=data, x='Učni algoritem', y='Epoh', hue='Arhitektura', ax=ax_epo, showfliers = False)
    sns.boxplot(data=data, x='Učni algoritem', y='Št. produktov', hue='Arhitektura', ax=ax_mul, showfliers = False)

    ax_epo.set_yscale('log')
    ax_mul.set_yscale('log')

    lsize = 17
    tsize = 14

    ax_mis.tick_params(which='both', labelsize=tsize)
    ax_mis.xaxis.label.set_size(lsize)
    ax_mis.xaxis.set_label_position('top') 
    ax_mis.xaxis.label.set_visible(False)
    ax_mis.yaxis.label.set_size(lsize)
    ax_mis.yaxis.tick_right()
    ax_mis.legend(fontsize=lsize, title='Arhitektura', title_fontsize=lsize, ncol=ncol, loc='center', bbox_to_anchor=(-0.3125, 0.5))

    ax_epo.tick_params(which='both', labelsize=tsize)
    ax_epo.xaxis.label.set_size(lsize)
    ax_epo.xaxis.set_label_position('top')
    ax_epo.xaxis.label.set_visible(False)
    ax_epo.yaxis.label.set_size(lsize)
    ax_epo.yaxis.tick_right()
    ax_epo.get_legend().remove()

    ax_mul.tick_params(which='both', labelsize=tsize)
    ax_mul.xaxis.label.set_size(lsize)
    ax_mul.xaxis.set_label_position('top') 
    ax_mul.xaxis.label.set_visible(False)
    ax_mul.yaxis.label.set_size(lsize)
    ax_mul.yaxis.tick_right()
    ax_mul.get_legend().remove()

    fig.set_tight_layout(True)


def plot_results_method_mnist(method, dif_layers, models, seeds, ncol=2):

    fig, (ax_mis, ax_epo, ax_mul) = plt.subplots(1, 3)
    fig.set_size_inches(16.5, 2.5)
    
    first = True
    data_model = []
    data_layers = []

    for i, layers in enumerate(dif_layers):
        for j, model in enumerate(models):
            res = load_results(method, layers, False, model, seeds)
            if first:
                epo_data = res[:, 1]
                mis_data = res[:, 3]
                mul_data = res[:, 5]
                first = False
            else:
                epo_data = np.hstack((epo_data, res[:, 1]))
                mis_data = np.hstack((mis_data, res[:, 3]))
                mul_data = np.hstack((mul_data, res[:, 5]))
            temp = model.split('_')
            if len(temp) == 1:
                data_model += ['DOUBLE']*len(seeds)
            elif len(temp) == 3:
                data_model += ['TM']*len(seeds)
            elif len(temp) == 4:
                if temp[3] == 'ilm':
                    data_model += ['ILM']*len(seeds)
                elif temp[3] == 'ma':
                    data_model += ['MPM']*len(seeds)
            data_layers += [i+1]*len(seeds)

    data = pd.DataFrame({
        'Epoh': epo_data,
        'FNR': mis_data,
        'Št. produktov': mul_data,
        'Format': data_model,
        'Arhitektura': data_layers
    })

    sns.set_context("notebook", font_scale=0.8, rc={"lines.linewidth": 1.75})

    sns.boxplot(data=data, x='Format', y='FNR', hue='Arhitektura', ax=ax_mis, showfliers = False)
    sns.boxplot(data=data, x='Format', y='Epoh', hue='Arhitektura', ax=ax_epo, showfliers = False)
    sns.boxplot(data=data, x='Format', y='Št. produktov', hue='Arhitektura', ax=ax_mul, showfliers = False)

    ax_epo.set_yscale('log')
    ax_mul.set_yscale('log')

    lsize = 17
    tsize = 14

    ax_mis.tick_params(which='both', labelsize=tsize)
    ax_mis.xaxis.label.set_size(lsize)
    ax_mis.xaxis.set_label_position('top') 
    ax_mis.xaxis.label.set_visible(False)
    ax_mis.yaxis.label.set_size(lsize)
    ax_mis.yaxis.tick_right()
    ax_mis.legend(fontsize=lsize, title='Arhitektura', title_fontsize=lsize, ncol=ncol, loc='center', bbox_to_anchor=(-0.3125, 0.5))

    ax_epo.tick_params(which='both', labelsize=tsize)
    ax_epo.xaxis.label.set_size(lsize)
    ax_epo.xaxis.set_label_position('top')
    ax_epo.xaxis.label.set_visible(False)
    ax_epo.yaxis.label.set_size(lsize)
    ax_epo.yaxis.tick_right()
    ax_epo.get_legend().remove()

    ax_mul.tick_params(which='both', labelsize=tsize)
    ax_mul.xaxis.label.set_size(lsize)
    ax_mul.xaxis.set_label_position('top') 
    ax_mul.xaxis.label.set_visible(False)
    ax_mul.yaxis.label.set_size(lsize)
    ax_mul.yaxis.tick_right()
    ax_mul.get_legend().remove()

    fig.set_tight_layout(True)


def plot_results_model_mnist(methods, dif_layers, model, seeds, ncol=2):

    fig, (ax_mis, ax_epo, ax_mul) = plt.subplots(1, 3)
    fig.set_size_inches(16.5, 2.5)
    
    first = True
    mis_method = []
    mis_layers = []
    for i, layers in enumerate(dif_layers):
        for j, method in enumerate(methods):
            res = load_results(method, layers, False, model, seeds)
            if first:
                epo_data = res[:, 1]
                mis_data = res[:, 3]
                mul_data = res[:, 5]
                first = False
            else:
                epo_data = np.hstack((epo_data, res[:, 1]))
                mis_data = np.hstack((mis_data, res[:, 3]))
                mul_data = np.hstack((mul_data, res[:, 5]))
            mis_method += [method.upper()]*len(seeds)
            mis_layers += [i+1]*len(seeds)
    
    
    data = pd.DataFrame({
        'Epoh': epo_data,
        'FNR': mis_data,
        'Št. produktov': mul_data,
        'Učni algoritem': mis_method,
        'Arhitektura': mis_layers
    })

    sns.set_context("notebook", font_scale=0.8, rc={"lines.linewidth": 1.75})

    sns.boxplot(data=data, x='Učni algoritem', y='FNR', hue='Arhitektura', ax=ax_mis, showfliers = False)
    sns.boxplot(data=data, x='Učni algoritem', y='Epoh', hue='Arhitektura', ax=ax_epo, showfliers = False)
    sns.boxplot(data=data, x='Učni algoritem', y='Št. produktov', hue='Arhitektura', ax=ax_mul, showfliers = False)

    ax_epo.set_yscale('log')
    ax_mul.set_yscale('log')

    lsize = 17
    tsize = 14

    ax_mis.tick_params(which='both', labelsize=tsize)
    ax_mis.xaxis.label.set_size(lsize)
    ax_mis.xaxis.set_label_position('top') 
    ax_mis.xaxis.label.set_visible(False)
    ax_mis.yaxis.label.set_size(lsize)
    ax_mis.yaxis.tick_right()
    ax_mis.legend(fontsize=lsize, title='Arhitektura', title_fontsize=lsize, ncol=ncol, loc='center', bbox_to_anchor=(-0.3125, 0.5))

    ax_epo.tick_params(which='both', labelsize=tsize)
    ax_epo.xaxis.label.set_size(lsize)
    ax_epo.xaxis.set_label_position('top')
    ax_epo.xaxis.label.set_visible(False)
    ax_epo.yaxis.label.set_size(lsize)
    ax_epo.yaxis.tick_right()
    ax_epo.get_legend().remove()

    ax_mul.tick_params(which='both', labelsize=tsize)
    ax_mul.xaxis.label.set_size(lsize)
    ax_mul.xaxis.set_label_position('top') 
    ax_mul.xaxis.label.set_visible(False)
    ax_mul.yaxis.label.set_size(lsize)
    ax_mul.yaxis.tick_right()
    ax_mul.get_legend().remove()

    fig.set_tight_layout(True)


seeds = [x for x in range(1000, 30001, 1000)]

# Proben1 32 tabulated points!

dbs = ['cancer1', 'card1', 'diabetes1', 'gene1', 'heart1', 'heartc1', 'horse1']

methods = ['gd', 'adam', 'cgm', 'bfgs']
'''
res = load_results('bfgs', 5, dbs[2], 'FV_16_16_ma', seeds)
print(dbs[2], '16_16', np.average(res[:, 3]), np.std(res[:, 3]))

#res = load_results('bfgs', 5, dbs[2], 'FV_12_12_ma', seeds)
#print(dbs[2], '12_12', np.average(res[:, 3]), np.std(res[:, 3]))

#res = load_results('bfgs', 5, dbs[3], 'FV_16_16_ma', seeds)
#print(dbs[3], '16_16', np.average(res[:, 3]), np.std(res[:, 3]))

#res = load_results('bfgs', 5, dbs[3], 'FV_12_12_ma', seeds)
#print(dbs[3], '12_12', np.average(res[:, 3]), np.std(res[:, 3]))

#res = load_results('cgm', 100, False, 'FV_16_16_ma', seeds)
#print('mnist', '16_16', np.average(res[:, 3]), np.std(res[:, 3]))

#res = load_results('cgm', 100, False, 'FV_12_12_ma', seeds)
#print('mnist', '12_12', np.average(res[:, 3]), np.std(res[:, 3]))
'''
#'''
model = 'FV_16_16'
#plot_results_method(methods[2], [3, 4, 5], dbs[2], ['PV', model, model+'_ilm'], seeds, ncol=1)
#plt.savefig('slike_proben1/{:s}_{:s}_{:s}.png'.format(dbs[2], methods[2], model))
#plt.show()

#plot_results_method(methods[3], [3, 4, 5], dbs[2], ['PV', model, model+'_ilm'], seeds, ncol=1)
#plt.savefig('slike_proben1/{:s}_{:s}_{:s}.png'.format(dbs[2], methods[3], model))
#plt.show()

#plot_results_method(methods[2], [3, 4, 5], dbs[3], ['PV', model, model+'_ilm', model+'_ma'], seeds, ncol=1)
#plt.savefig('slike_proben1/{:s}_{:s}_{:s}.png'.format(dbs[3], methods[2], model))
#plt.show()

#plot_results_method(methods[3], [3, 4, 5], dbs[3], ['PV', model, model+'_ilm', model+'_ma'], seeds, ncol=1)
#plt.savefig('slike_proben1/{:s}_{:s}_{:s}.png'.format(dbs[3], methods[3], model))
#plt.show()
#'''

'''
for db in dbs[2:4]:
    plot_results_model(methods, [3, 4, 5], db, 'PV', seeds, ncol=1)
    plt.savefig('slike_proben1/{:s}_PV.png'.format(db))
    for color, method in zip(['tab:blue', 'tab:orange', 'tab:green', 'tab:red'], methods):
        plot_results_method(method, [3, 4, 5], db, ['PV', 'FV_16_16', 'FV_16_16_ilm', 'FV_16_16_ma'], seeds, ncol=1)
        plt.savefig('slike_proben1/{:s}_{:s}_FV_16_16.png'.format(db, method))
        plot_results_method(method, [3, 4, 5], db, ['PV', 'FV_12_12', 'FV_12_12_ilm', 'FV_12_12_ma'], seeds, ncol=1)
        plt.savefig('slike_proben1/{:s}_{:s}_FV_12_12.png'.format(db, method, ))
        plot_results_method(method, [3, 4, 5], db, ['PV', 'FV_8_8'  , 'FV_8_8_ilm'  , 'FV_8_8_ma']  , seeds, ncol=1)
        plt.savefig('slike_proben1/{:s}_{:s}_FV_8_8.png'.format(db, method, ))
    plt.close('all')
'''

'''
#MIN-MAX !!!
methods = ['gd', 'adam', 'cgm', 'bfgs']
models = ['FV_16_16', 'FV_12_12', 'FV_8_8']

data_min = []
data_max = []
data_db = []
data_layers = []
data_method = []
data_model = []
data_mul = []

model = 'FV_16_16_ma'
print(model)

for method in methods:
    for layers in [3, 4, 5]:
        for db in dbs[2:4]:
            stats = load_mult_stats(method, layers, db, model, seeds)
            data_min += list(stats[:, 0])
            data_max += list(stats[:, 1])
            data_db += [db]*len(seeds)
            data_layers += [layers]*len(seeds)
            data_method += [method.upper()]*len(seeds)
            data_model += ['DOUBLE']*len(seeds)
            data_mul += ['DOUBLE']*len(seeds)
    
    if not method == 'bfgs':
        db = 'mnist'
        for layers in [30, 60, 100]:
            stats = load_mult_stats(method, layers, '', model, seeds, base='../mnist_results')
            data_min += list(stats[:, 0])
            data_max += list(stats[:, 1])
            data_db += [db]*len(seeds)
            data_layers += [layers]*len(seeds)
            data_method += [method.upper()]*len(seeds)
            data_model += ['DOUBLE']*len(seeds)
            data_mul += ['DOUBLE']*len(seeds)


data = pd.DataFrame({
    'Min': data_min,
    'Max': data_max,
    'Nabor': data_db,
    'Arhitek.': data_layers,
    'Učni algoritem': data_method,
    'Format': data_model,
    'Množilnik': data_mul
})

data_o = data

for db in ['diabetes1', 'gene1', 'mnist']:
    data = data_o.loc[data_o['Nabor'] == db]

    ext_gd = (np.min(data.loc[data['Učni algoritem'] == 'GD']['Min']), np.max(data.loc[data['Učni algoritem'] == 'GD']['Max']))
    ext_adam = (np.min(data.loc[data['Učni algoritem'] == 'ADAM']['Min']), np.max(data.loc[data['Učni algoritem'] == 'ADAM']['Max']))
    ext_cgm = (np.min(data.loc[data['Učni algoritem'] == 'CGM']['Min']), np.max(data.loc[data['Učni algoritem'] == 'CGM']['Max']))
    ext_bfgs = (np.min(data.loc[data['Učni algoritem'] == 'BFGS']['Min']), np.max(data.loc[data['Učni algoritem'] == 'BFGS']['Max']))

    print(db)
    print('{:.1f} {:.1f}'.format(ext_gd[0], ext_gd[1]))
    print('{:.1f} {:.1f}'.format(ext_adam[0], ext_adam[1]))
    print('{:.1f} {:.1f}'.format(ext_cgm[0], ext_cgm[1]))
    print('{:.1f} {:.1f}'.format(ext_bfgs[0], ext_bfgs[1]))
    print()

'''

'''
#model = 'PV'
#model = 'FV_16_16'
#model = 'FV_12_12'
model = 'FV_8_8'
stats_gd = load_mult_stats('gd', 3, db, model, seeds)
stats_adam = load_mult_stats('adam', 3, db, model, seeds)
stats_cgm = load_mult_stats('cgm', 3, db, model, seeds)
stats_bfgs = load_mult_stats('bfgs', 3, db, model, seeds)

print(db, model)
print('gd', model, np.min(stats_gd[:, 0]), np.max(stats_gd[:, 1]))
print('adam', model, np.min(stats_adam[:, 0]), np.max(stats_adam[:, 1]))
print('cgm', model, np.min(stats_cgm[:, 0]), np.max(stats_cgm[:, 1]))
print('bfgs', model, np.min(stats_bfgs[:, 0]), np.max(stats_bfgs[:, 1]))
'''


#db, model=dbs[4], 'FV_16_16'
#plot_gd(3, db, model, 1000); plot_gd(4, db, model, 1000); plot_gd(5, db, model, 1000)
#plot_adam(3, db, model, 1000); plot_adam(4, db, model, 1000); plot_adam(5, db, model, 1000)
#plot_cgm(3, db, model, 1000); plot_cgm(4, db, model, 1000); plot_cgm(5, db, model, 1000)
#plot_bfgs(3, db, model, 1000); plot_bfgs(4, db, model, 1000); plot_bfgs(5, db, model, 1000)

#plot_gd(3, dbs[4], 'FV_16_16', 1000)
#plt.show()

'''
with open('q.txt', 'r') as f:
    data = np.array([[float(x) for x in line.rstrip().split(' ')] for line in f.readlines()])
    min = data[-1, :]
    data = data[:-1, :]
    plt.plot(data[:, 0], data[:, 1], 'r-')
    plt.plot(min[0], min[1], 'bx')
    plt.show()
'''

# mnist

#plot_gd(30, False, 'PV', 1000)
#plot_gd(30, False, 'FV_12_12', 1000)
#plot_gd(30, False, 'FV_12_12_ilm', 1000)
#plot_gd(30, False, 'FV_12_12_ma', 1000)
#plt.show()

#plot_adam(30, False, 'PV', 2000)
#plot_adam(30, False, 'FV_12_20', 2000)
#plot_adam(30, False, 'FV_12_20_ilm', 2000)
#plot_adam(30, False, 'FV_12_20_ma', 2000)
#plt.show()

#plot_cgm(30, False, 'PV', 1000)
#plot_cgm(30, False, 'FV_12_12', 1000)
#plot_cgm(30, False, 'FV_12_12_ilm', 1000)
#plot_cgm(30, False, 'FV_12_12_ma', 1000)
#plt.show()


#plot_results_model_mnist(['gd', 'adam', 'cgm'], [30, 60, 100], 'PV', seeds, ncol=1)
#plt.savefig('slike_mnist/mnist_PV.png')

#method = 'cgm'
#plot_results_method_mnist(method, [30, 60, 100], ['PV', 'FV_16_16', 'FV_16_16_ilm', 'FV_16_16_ma'][:-1], seeds, ncol=1)
#plt.savefig('slike_mnist/mnist_{:s}_FV_16_16.png'.format(method))
#plot_results_method_mnist(method, [30, 60, 100], ['PV', 'FV_12_12', 'FV_12_12_ilm', 'FV_12_12_ma'][:-1], seeds, ncol=1)
#plt.savefig('slike_mnist/mnist_{:s}_FV_12_12.png'.format(method))
#plot_results_method_mnist(method, [30, 60, 100], ['PV', 'FV_8_8', 'FV_8_8_ilm', 'FV_8_8_ma'][:-1], seeds, ncol=1)
#plt.savefig('slike_mnist/mnist_{:s}_FV_8_8.png'.format(method))

