import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm


#https://en.wikipedia.org/wiki/Test_functions_for_optimization

def rosenbrock(x):
    f = 0.0
    for i in range(x.size - 1):
        f += 100*(x[i+1]-x[i]**2)**2 + (1-x[i])**2
    return f

def grad_rosenbrock(x):
    grad = np.zeros(x.size)
    grad[0] = 200*(x[1]-x[0]**2)*(-2*x[0]) - 2*(1-x[0])
    for i in range(1, x.size - 1):
        grad[i] = 200*(x[i] - x[i-1]**2) + 200*(x[i+1] - x[i]**2)*(-2*x[i]) - 2*(1 - x[i])
    grad[-1] = 200*(x[-1] - x[-2]**2)
    return grad



def backtracking_scalar(phi, phi0, derphi0, c1=1e-4, alpha=1.0, amin=0.0):
    while alpha > amin:
        phi_a = phi(alpha)
        if phi_a <= phi0 + c1*alpha*derphi0:
            return alpha
        alpha *= 0.7
    return alpha


def backtracking(f, g0, x, dir, c1=1e-4, alpha=1.0, amin=0.0):
    derphi0 = np.dot(g0, dir)
    if derphi0 >= 0:
        return 0.0
    phi0 = f(x)
    phi = lambda a: f(x + a*dir)
    return backtracking_scalar(phi, phi0, derphi0, c1=c1, alpha=alpha, amin=amin)


def test_line_search():
    n = 2
    x0 = np.random.random(n)
    x0[0], x0[1] = 4.0*x0[0] - 2.0, 3.0*x0[1]
    #x0 = np.array([-0.19406826, 0.04077598]) # expend
    x0 = np.array([-1.27501547, 0.86173742]) # contract
    dir = -grad_rosenbrock(x0)
    print('x0', x0)
    print('dir', dir)

    fig = plt.figure()
    fig.set_size_inches(12, 6)
    ax = fig.add_subplot(1, 1, 1)

    t_min_b = backtracking(rosenbrock, grad_rosenbrock, x0, dir, ax=ax)

    t = np.linspace(0.0, 0.01, num=100)
    q = np.array([rosenbrock(x0 + ti*dir) for ti in t])
    ax.plot(t, q, 'r-')
    ax.plot(t_min_b, rosenbrock(x0 + t_min_b*dir), 'b.')
    
    plt.tight_layout()
    plt.show()


def gd(x0, f, df, consecutive_allowed, alpha, ax=None):
    x, x_opt = np.copy(x0), np.copy(x0)

    iterations = 0
    consecutive = 0
    min_fval = f(x)
    running = True

    while running:

        # stopping criteria
        if consecutive >= consecutive_allowed:
            running = False
            break

        # update
        dx = -df(x)
        x += alpha*dx
        iterations += 1

        # evaluate
        fval = f(x)
        if fval < min_fval:
            x_opt = np.copy(x)
            min_fval = fval
            consecutive = 0
        else:
            consecutive += 1
        
    if ax:
        ax.plot(*x_opt, 'b.')
    
    return iterations, x_opt


def agd(x0, f, df, consecutive_allowed, ax=None):
    x, x_opt = np.copy(x0), np.copy(x0)

    iterations = 0
    consecutive = 0
    min_fval = f(x)
    running = True

    while running:

        # update
        dx = df(x)
        t = backtracking(f, dx, x, -dx, alpha=0.01)
        if t > 0.0:
            x -= t*dx
        iterations += 1

        # evaluate
        fval = f(x)
        if fval < min_fval:
            x_opt = np.copy(x)
            min_fval = fval
            consecutive = 0
        else:
            consecutive += 1
        # stopping criteria
        if consecutive >= consecutive_allowed:
            running = False
            break
    if ax:
        ax.plot(*x_opt, 'b.')
    return iterations, x_opt


def adam(x0, f, df, consecutive_allowed, alpha, beta1, beta2, epsilon, ax=None):
    x, x_opt, b1, b2, m, v = np.copy(x0), np.copy(x0), beta1, beta2, np.zeros(x0.size), np.zeros(x0.size)

    iterations = 0
    consecutive = 0
    min_fval = f(x)
    running = True

    while running:

        # stopping criteria
        if consecutive >= consecutive_allowed:
            running = False
            break

        # update
        grad = df(x)
        m = beta1*m + (1 - beta1)*grad
        v = beta2*v + (1 - beta2)*grad*grad
        a = alpha * np.sqrt(1 - b2)/(1 - b1)
        x -= a*m/(np.sqrt(v) + epsilon)
        b1 *= beta1
        b2 *= beta2
        iterations += 1

        #evaluate
        fval = f(x)
        if fval < min_fval:
            x_opt = np.copy(x)
            min_fval = fval
            consecutive = 0
        else:
            consecutive += 1
        
    if ax:
        ax.plot(*x_opt, 'b.')
    return iterations, x_opt


def cgm(x0, f, df, consecutive_allowed, ax=None):
    x, x_opt, s_upd = np.copy(x0), np.copy(x0), 0
    
    iterations = 0
    consecutive = 0
    min_fval = f(x)
    running = True

    while running:

        # stopping criteria
        if consecutive >= consecutive_allowed:
            running = False
            break

        # update
        dx = df(x)
        if s_upd == 0:
            # reset conjugate direction
            s = -dx
            normo = np.dot(dx, dx)
            if normo > 0.0:
                t = backtracking(f, dx, x, s)
                if t > 0.0:
                    x += t*s
                    s_upd = 1
        else:
            # update conjugate direction
            norm = np.dot(dx, dx)
            if norm == 0.0:
                s_upd = 0
            else:
                beta = norm/normo
                s = beta*s - dx
                s_upd += 1
                t = backtracking(f, dx, x, s)
                if t == 0.0:
                    s_upd = 0
                else:
                    x += t*s
                    normo = norm

        iterations += 1

        #evaluate
        fval = f(x)
        if fval < min_fval:
            x_opt = np.copy(x)
            min_fval = fval
            consecutive = 0
        else:
            consecutive += 1
    
    if ax:
        ax.plot(*x_opt, 'b.')
    return iterations, x_opt
            

def bfgs(x0, f, df, consecutive_allowed, ax=None):
    x, x_opt, B_upd = np.copy(x0), np.copy(x0), 0
    
    iterations = 0
    consecutive = 0
    min_fval = f(x)
    running = True

    while running:

        # stopping criteria
        if consecutive >= consecutive_allowed:
            running = False
            break

        # update
        if B_upd == 0:
            dx = df(x)
            B = np.identity(x.size)
            s = -dx
            t = backtracking(f, dx, x, s)
            if t > 0.0:
                s *= t
                x += s
                B_upd = 1
        else:
            dxn = df(x)
            y = dxn - dx
            sy = np.dot(s, y)
            if sy <= 0.0:
                B_upd = 0
            else:
                B += ((sy + np.dot(y, np.dot(B, y)))*np.outer(s, s)/sy - (np.outer(np.dot(B, y), s) + np.outer(s, np.dot(y, B))))/sy
                B_upd += 1
                s = -np.dot(B, dxn)
                t = backtracking(f, dxn, x, s)
                if t > 0.0:
                    s *= t
                    x += s
                    dx = dxn
                else:
                    B_upd = 0
        iterations += 1
        
        #evaluate
        fval = f(x)
        if fval < min_fval:
            x_opt = np.copy(x)
            min_fval = fval
            consecutive = 0
        else:
            consecutive += 1
    
    if ax:
        ax.plot(*x_opt, 'b.')
    return iterations, x_opt



def optimize_rosenbrock():
    n = 4

    f, df = rosenbrock, grad_rosenbrock
    x0 = np.random.random(n)
    ax_con = None
    if n == 2:
        x0[0], x0[1] = 4.0*x0[0] - 2.0, 3.0*x0[1]
        #x0 = np.array([-1.0, 2.5])

        fig = plt.figure()
        fig.set_size_inches(12, 6)
        ax_wir = fig.add_subplot(1, 2, 1, projection='3d')
        ax_con = fig.add_subplot(1, 2, 2)

        x_span = np.linspace(-2.0, 2.0, num=10)
        y_span = np.linspace(0.0, 3.0, num=10)
        x, y = np.meshgrid(x_span, y_span)
        z = np.zeros(x.shape)
        w = np.zeros(2)
        for i, xi in enumerate(x_span):
            for j, yj in enumerate(y_span):
                w[0] = xi
                w[1] = yj
                z[j, i] = f(w)

        ax_wir.plot_surface(x, y, z, cmap=cm.coolwarm)
        ax_con.contour(x, y, z, levels=15)
        #ax_con.plot(*x0, 'rx')
        #ax_con.annotate('start', x0)
    else:
        x0 = 4*x0 - 2

    x0 = np.array([-0.1, 2.5, 0.5, -0.1])
    #x0 = np.array([-0.5, 2.5])

    np.set_printoptions(precision=6)
    print('x0', x0)

    it_gd, x_gd = gd(x0, rosenbrock, grad_rosenbrock, 100, 0.00075, ax=ax_con); print('  gd {:6d}'.format(it_gd), x_gd)
    it_agd, x_agd = agd(x0, rosenbrock, grad_rosenbrock, 100, ax=ax_con); print(' agd {:6d}'.format(it_agd), x_agd)
    it_adam, x_adam = adam(x0, rosenbrock, grad_rosenbrock, 100, 0.005, 0.9, 0.999, 1e-8, ax=ax_con); print('adam {:6d}'.format(it_adam), x_adam)
    it_cgm, x_cgm = cgm(x0, rosenbrock, grad_rosenbrock, 10, ax=ax_con); print(' cgm {:6d}'.format(it_cgm), x_cgm)
    it_bfgs, x_bfgs = bfgs(x0, rosenbrock, grad_rosenbrock, 10, ax=ax_con); print('bfgs {:6d}'.format(it_bfgs), x_bfgs)

    if n == 2:
        plt.tight_layout()
        plt.show()



if __name__ == '__main__':
    #test_line_search()
    #optimize_rosenbrock()

    B = np.identity(5)
    B[0, 1] = 2.0
    B[2, 0] = -1.0
    B[3, 2] = 3.0

    s = np.array([1.0, 2.0, -2.0, 0.0, 1.0])
    y = np.array([1.0, -1.0, 2.0, -3.0, 2.0])

    Bn1 = np.matmul(np.matmul(np.identity(5) - np.outer(s, y)/np.dot(y, s), B), np.identity(5) - np.outer(y, s)/np.dot(y, s)) + np.outer(s, s)/np.dot(y, s)
    Bn2 = B + (np.dot(s, y) + np.dot(y, np.dot(B, y)))*np.outer(s, s)/(np.dot(s, y)**2) - (np.outer(np.dot(B, y), s) + np.outer(s, np.dot(y, B)))/np.dot(s, y)
    
    #print(Bn1)
    print(Bn2)
    #print(np.outer(np.dot(B, y), s) + np.outer(s, np.dot(y, B)))

    print(np.sum(np.sum(Bn1-Bn2)))

    pass

