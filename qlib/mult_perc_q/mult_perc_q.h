#ifndef _MULT_PERC_Q
#define _MULT_PERC_Q

    #include "../q/q.h"
    #include "../tab_func_q/tab_func_q.h"
    #include "../database_q/database_q.h"
    #include "../../lib/mult_perc/mult_perc.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct mult_perc_q;
    struct activ_holdr_q;
    struct deriv_holdr_q;

    // activation function
    extern long (*sigma_q)(long);
    extern long (*dsigma_q)(long);

    void set_sigmoid_q();

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // neurons activation and sigma(activation)
    struct activ_holdr_q {
        int number_of_layers;
        int number_of_neurons;
        int *neurons_per_layer;
        long *data;
        long **p_act;
        long **p_sig;

        // statistics
        long number_of_updates;
        double *p_stat;
    };

    struct activ_holdr_q *alloc_activ_holdr_q(int number_of_layers, int neurons_per_layer[]);
    void dealloc_activ_holdr_q(struct activ_holdr_q *a_q);
    void print_activ_holdr_q(struct activ_holdr_q *a_q);

    void reset_statistics_activ_holdr_q(struct activ_holdr_q *a_q);
    void update_statistics_activ_holdr_q(struct activ_holdr_q *a_q);
    void print_statistics_activ_holdr_q(struct activ_holdr_q *a_q, FILE *file);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // gradient
    struct deriv_holdr_q {
        int number_of_layers;
        int number_of_neurons;
        int number_of_weights;
        int *neurons_per_layer;
        long *data;
        long **p_row;
        long ***p_mat;

        // statistics
        long number_of_updates;
        double *p_stat;
    };

    struct deriv_holdr_q *alloc_deriv_holdr_q(int size, int layers[]);
    void dealloc_deriv_holdr_q(struct deriv_holdr_q *d_q);
    void print_deriv_holdr_q(struct deriv_holdr_q *d_q);
    void init_rand_deriv_holdr_q(struct deriv_holdr_q *d_q, double scale);
    void set_to_zero_deriv_holdr_q(struct deriv_holdr_q *d_q);
    void copy_weights_deriv_holdr_q(struct deriv_holdr_q *src_q, struct deriv_holdr_q *dst_q);
    int is_zero_deriv_holdr_q(struct deriv_holdr_q *d_q);

    void reset_statistics_deriv_holdr_q(struct deriv_holdr_q *d_q);
    void update_statistics_deriv_holdr_q(struct deriv_holdr_q *d_q);
    void print_statistics_deriv_holdr_q(struct deriv_holdr_q *d_q, FILE *file);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // multilayer perceptron
    struct mult_perc_q {
        struct deriv_holdr_q *weights;        // weights and biases
    };

    struct mult_perc_q *alloc_mult_perc_q(int number_of_layers, int neurons_per_layer[]);
    struct mult_perc_q *alloc_mult_perc_mult_perc_q(struct mult_perc *mlp);
    void dealloc_mult_perc_q(struct mult_perc_q *mlp_q);
    void print_mult_perc_q(struct mult_perc_q *mlp_q);
    void init_rand_mult_perc_q(struct mult_perc_q *mlp_q, double scale);
    void copy_weights_mult_perc_q(struct mult_perc_q *src_q, struct mult_perc_q *dst_q);

    void evaluate_example_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, long in_q[], long out_q[], long *error_q, int *misses);
    void evaluate_example_mult_perc_double_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, long in_q[], long out_q[], double *error, int *misses);

    // calculate SE (squared error) and TPR (true positive rate: only for classification) on dataset batch
    void evaluate_batch_mult_perc_q(
        struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, int batch_start, int batch_size, long *error_q, int *misses
    );

    // calculate SE (squared error) and TPR (true positive rate: only for classification) on dataset batch
    // error has bigger range than [-2^integer_portion, 2^integer_portion]! (integer_portion is bigger, fraction_portion is the same!)
    void evaluate_batch_mult_perc_double_q(
        struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, int batch_start, int batch_size, double *error, int *misses
    );

    // calculte MSE (mean squared error) and TPR (true positive rate: only for classification) on dataset
    void evaluate_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, long *error_q, int *misses);

    // calculte error (accumulate squared error) and misses in fixed point (misses: only for classification) on database
    void evaluate_mult_perc_double_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, double *error, int *misses);

    // forward pass:
    // arguments
    // - struct mult_perc_q *mlp_q: multilayer perceptron
    // - struct activ_holdr_q *a_q: memory to save neurons actiovations and sigma (activations)
    // - long in[]: input vector
    void forward_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, long in_q[]);

    // numerical derivative:
    long numeric_grad_mult_perc_q(
        struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, int layer_idx, int neuron_idx, int weight_idx, long dweig_q, long in_q[], long out_q[]
    );

    // backpropagation:
    // arguments
    // - struct mult_perc_q *mlp_q: multilayer perceptron
    // - struct activ_holdr_q *a_q: memory to save neurons actiovations and sigma(activations)
    // - struct deriv_holdr_q *d_q: memory to save neurons partial derivatives
    // - long in[]: input vector
    // - long out[]: output vector
    void backpropagation_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct deriv_holdr_q *d_q, long in_q[], long out_q[]);

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif