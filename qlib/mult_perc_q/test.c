#include "mult_perc_q.h"

void test_activ_holdr_q(int size, int *layers) {
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(size, layers);
    for(int i = 0; i < 2*a_q->number_of_neurons - a_q->neurons_per_layer[0]; i++) {
        a_q->data[i] = double_to_q(i + 1);
    }
    print_activ_holdr_q(a_q);
    dealloc_activ_holdr_q(a_q);
}

void test_activ_holdr_q_stat(int size, int *layers) {
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(size, layers);
    reset_statistics_activ_holdr_q(a_q);
    for(int i = 0; i < a_q->number_of_neurons - a_q->neurons_per_layer[0]; i++) {
        a_q->p_act[0][i] = int_to_q(i + 1);
    }
    update_statistics_activ_holdr_q(a_q);
    print_activ_holdr_q(a_q);
    print_statistics_activ_holdr_q(a_q, stdout);
    dealloc_activ_holdr_q(a_q);
}

void test_mult_perc_q(int size, int *layers) {
    printf("%d\n", size);
    struct mult_perc_q *mlp_q = alloc_mult_perc_q(size, layers);
    for(int i = 0; i < mlp_q->weights->number_of_weights; i++) {
        mlp_q->weights->data[i] = int_to_q(i + 1);
    }
    print_mult_perc_q(mlp_q);
    dealloc_mult_perc_q(mlp_q);
}

void test_mult_perc_q_stat() {
    int size = 2, layers[] = {4, 2};
    struct mult_perc_q *mlp_q = alloc_mult_perc_q(size, layers);
    reset_statistics_deriv_holdr_q(mlp_q->weights);
    for(int i = 0; i < mlp_q->weights->number_of_weights; i++) {
        mlp_q->weights->data[i] = int_to_q(i + 1);
    }
    update_statistics_deriv_holdr_q(mlp_q->weights);
    print_deriv_holdr_q(mlp_q->weights);
    print_statistics_deriv_holdr_q(mlp_q->weights, stdout);
    dealloc_mult_perc_q(mlp_q);
}

void test_backprop_proben1(int db) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);

    struct database_q *train_q = load_database_database_q(train);
    struct database_q *valid_q = load_database_database_q(valid);
    struct database_q *test_q = load_database_database_q(test);
    
    srand(1000);

    struct mult_perc *mlp = alloc_mult_perc(proben1_number_of_layers, proben1_neurons_per_layer[db]);
    struct activ_holdr *a = alloc_activ_holdr(proben1_number_of_layers, proben1_neurons_per_layer[db]);
    struct deriv_holdr *d = alloc_deriv_holdr(proben1_number_of_layers, proben1_neurons_per_layer[db]);
    init_rand_mult_perc(mlp, 1.0);

    struct mult_perc_q *mlp_q = alloc_mult_perc_mult_perc_q(mlp);
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(proben1_number_of_layers, proben1_neurons_per_layer[db]);
    struct deriv_holdr_q *d_q = alloc_deriv_holdr_q(proben1_number_of_layers, proben1_neurons_per_layer[db]);
    
    set_sigmoid();
    set_sigmoid_q();

    int record = 0;
    backpropagation_mult_perc(mlp, a, d, get_input_database(train, record), get_output_database(train, record));
    backpropagation_mult_perc_q(mlp_q, a_q, d_q, get_input_database_q(train_q, record), get_output_database_q(train_q, record));

    for(int i = 0; i < d->number_of_weights; i++) {
        double temp = q_to_double(d_q->data[i]);
        printf("%3d %+.10f %+.10f %+e\n", i, d->data[i], temp, d->data[i]-temp);
    }

    dealloc_mult_perc(mlp);
    dealloc_activ_holdr(a);
    dealloc_deriv_holdr(d);

    dealloc_mult_perc_q(mlp_q);
    dealloc_activ_holdr_q(a_q);
    dealloc_deriv_holdr_q(d_q);

    dealloc_database(train);
    dealloc_database(valid);
    dealloc_database(test);

    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);

}

int main(int argc, char *argv[]) {

    set_portions_q(10, 22);
    set_tab_q(64);
    set_sigmoid_q();

    //int size = 3, layers[] = {4, 6, 4};
    //test_activ_holdr_q(size, layers);
    //test_activ_holdr_q_stat(size, layers);

    //test_mult_perc_q(size, layers);
    //test_mult_perc_q_stat();

    test_backprop_proben1(0);

    return 0;
}