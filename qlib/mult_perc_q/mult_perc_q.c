#include "mult_perc_q.h"


long (*sigma_q)(long);
long (*dsigma_q)(long);

void set_sigmoid_q() {
    sigma_q = logistic_function_q;
    dsigma_q = logistic_function_derivative_q;
}

struct activ_holdr_q *alloc_activ_holdr_q(int number_of_layers, int neurons_per_layer[]) {
    struct activ_holdr_q *a_q = (struct activ_holdr_q*) malloc(sizeof(struct activ_holdr_q));
    a_q->number_of_layers = number_of_layers;
    a_q->neurons_per_layer = (int*) malloc(sizeof(int) * number_of_layers);
    a_q->number_of_neurons = 0;
    for(int i = 0; i < number_of_layers; i++) {
        a_q->neurons_per_layer[i] = neurons_per_layer[i];
        a_q->number_of_neurons += neurons_per_layer[i];
    }
    a_q->data = (long*) malloc(sizeof(long) * (2 * a_q->number_of_neurons - neurons_per_layer[0]));
    a_q->p_stat = (double*) malloc(sizeof(double) * 4);
    a_q->p_act = (long**) malloc(sizeof(long*) * (2 * number_of_layers - 1));
    a_q->p_sig = a_q->p_act + number_of_layers - 1;
    a_q->p_sig[0] = a_q->data + a_q->number_of_neurons - neurons_per_layer[0];
    int offset = 0;
    for(int i = 1; i < number_of_layers; i++) {
        a_q->p_act[i - 1] = a_q->data + offset;
        a_q->p_sig[i] = a_q->data + a_q->number_of_neurons + offset;
        offset += neurons_per_layer[i];
    }
    return a_q;
}

void dealloc_activ_holdr_q(struct activ_holdr_q *a_q) {
    free(a_q->neurons_per_layer);
    free(a_q->p_stat);
    free(a_q->data);
    free(a_q->p_act);
    free(a_q);
}

void print_activ_holdr_q(struct activ_holdr_q *a_q) {
    printf(".--------------------------------------------.\n");
    printf("| layer | neuron |      z      |   sigma(z)  |\n");
    printf("|-------|--------|-------------|-------------|\n");
    for(int i = 0; i < a_q->number_of_layers; i++) {
        if(i > 0) {
            for(int j = 0; j < a_q->neurons_per_layer[i]; j++) {
                printf(
                    "|  %2d   |  %3d   | %11.4e | %11.4e |\n",
                    i, j, q_to_double(a_q->p_act[i-1][j]), q_to_double(a_q->p_sig[i][j])
                );
            }
        } else {
            for(int j = 0; j < a_q->neurons_per_layer[i]; j++) {
                printf("|  %2d   |  %3d   |             | %11.4e |\n", i, j, q_to_double(a_q->p_sig[i][j]));
            }
        }
        if(i < a_q->number_of_layers - 1) {
            printf("|--------------------------------------------|\n");
        } else {
            printf("'--------------------------------------------'\n");
        }
    }
    printf("\n");
}

void reset_statistics_activ_holdr_q(struct activ_holdr_q *a_q) {
    a_q->number_of_updates = 0L;
    a_q->p_stat[0] = 0.0;
    a_q->p_stat[1] = 0.0;
    a_q->p_stat[2] = 1.0/0.0;
    a_q->p_stat[3] = -1.0/0.0;
}

void update_statistics_activ_holdr_q(struct activ_holdr_q *a_q) {
    for(int i = 0; i < a_q->number_of_neurons - a_q->neurons_per_layer[0]; i++) {
        statistics(a_q->p_stat, ++a_q->number_of_updates, q_to_double(a_q->p_act[0][i]));
    }
}

void print_statistics_activ_holdr_q(struct activ_holdr_q *a_q, FILE *file) {
    fprintf(file, "%e %e %e %e\n", a_q->p_stat[0], sqrt(a_q->p_stat[1]/a_q->number_of_updates), a_q->p_stat[2], a_q->p_stat[3]);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

struct deriv_holdr_q *alloc_deriv_holdr_q(int number_of_layers, int neurons_per_layer[]) {
    struct deriv_holdr_q *d_q = (struct deriv_holdr_q*) malloc(sizeof(struct deriv_holdr_q));
    d_q->number_of_layers = number_of_layers;
    d_q->neurons_per_layer = (int*) malloc(sizeof(int) * number_of_layers);
    d_q->number_of_neurons = 0;
    d_q->number_of_weights = 0;
    for(int i = 0; i < number_of_layers; i++) {
        d_q->neurons_per_layer[i] = neurons_per_layer[i];
        d_q->number_of_neurons += neurons_per_layer[i];
        if(i > 0) {
            d_q->number_of_weights += (neurons_per_layer[i - 1] + 1) * neurons_per_layer[i];
        }
    }
    d_q->data = (long*) malloc(sizeof(long) * d_q->number_of_weights);
    d_q->p_row = (long**) malloc(sizeof(long*) * (d_q->number_of_neurons - neurons_per_layer[0]));
    d_q->p_mat = (long***) malloc(sizeof(long**) * (number_of_layers - 1));
    d_q->p_stat = (double*) malloc(sizeof(double) * 4);
    int number_of_neurons = 0, number_of_rows = 0, number_of_weights = 0;
    for(int i = 1; i < number_of_layers; i++) {
        int weights_in_layer = 0;
        for(int j = 0; j < neurons_per_layer[i]; j++) {
            d_q->p_row[number_of_rows++] = d_q->data + number_of_weights + weights_in_layer;
            weights_in_layer += neurons_per_layer[i - 1] + 1;
        }
        d_q->p_mat[i - 1] = d_q->p_row + number_of_neurons;
        number_of_neurons += neurons_per_layer[i];
        number_of_weights += weights_in_layer;
    }
    return d_q;
}

void dealloc_deriv_holdr_q(struct deriv_holdr_q *d_q) {
    free(d_q->neurons_per_layer);
    free(d_q->data);
    free(d_q->p_row);
    free(d_q->p_mat);
    free(d_q->p_stat);
    free(d_q);
}

void print_deriv_holdr_q(struct deriv_holdr_q *d_q) {
    printf(".---------------------------------------.\n");
    printf("| layer | neuron | weight |    value    |\n");
    printf("|-------|--------|--------|-------------|\n");
    for(int i = 1; i < d_q->number_of_layers; i++) {
        for(int j = 0; j < d_q->neurons_per_layer[i]; j++) {
            for(int k = 0; k < d_q->neurons_per_layer[i - 1] + 1; k++) {
                printf("|  %2d   |  %3d   |  %3d   | %11.4e |\n", i, j, k, q_to_double(d_q->p_mat[i - 1][j][k]));
            }
            if(j < d_q->neurons_per_layer[i] - 1) {
                printf("|---------------------------------------|\n");
            } else if(i < d_q->number_of_layers - 1) {
                printf("|:::::::::::::::::::::::::::::::::::::::|\n");
            } else {
                printf("'---------------------------------------'\n");
            }
        }
    }
    printf("\n");
}

void init_rand_deriv_holdr_q(struct deriv_holdr_q *d_q, double scale) {
    for(int i = 0; i < d_q->number_of_weights; i++) {
        d_q->data[i] = double_to_q(scale*(2.0*(((double) rand())/RAND_MAX) - 1.0));
    }
}

void set_to_zero_deriv_holdr_q(struct deriv_holdr_q *d_q) {
    for(int i = 0; i < d_q->number_of_weights; i++) {
        d_q->data[i] = 0L;
    }
}

void copy_weights_deriv_holdr_q(struct deriv_holdr_q *src_q, struct deriv_holdr_q *dst_q) {
    for(int i = 0; i < dst_q->number_of_weights; i++) {
        dst_q->data[i] = src_q->data[i];
    }
}

int is_zero_deriv_holdr_q(struct deriv_holdr_q *d_q) {
    for(int i = 0; i < d_q->number_of_weights; i++) {
        if(d_q->data[i] != 0L) {
            return 0;
        }
    }
    return 1;
}

void reset_statistics_deriv_holdr_q(struct deriv_holdr_q *d_q) {
    d_q->number_of_updates = 0L;
    d_q->p_stat[0] = 0.0;
    d_q->p_stat[1] = 0.0;
    d_q->p_stat[2] = 1.0/0.0;
    d_q->p_stat[3] = -1.0/0.0;
}

void update_statistics_deriv_holdr_q(struct deriv_holdr_q *d_q) {
    for(int i = 0; i < d_q->number_of_weights; i++) {
        statistics(d_q->p_stat, ++d_q->number_of_updates, q_to_double(d_q->data[i]));
    }
}

void print_statistics_deriv_holdr_q(struct deriv_holdr_q *d_q, FILE *file) {
    fprintf(file, "%e %e %e %e\n", d_q->p_stat[0], sqrt(d_q->p_stat[1]/d_q->number_of_updates), d_q->p_stat[2], d_q->p_stat[3]);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

struct mult_perc_q *alloc_mult_perc_q(int number_of_layers, int neurons_per_layer[]) {
    struct mult_perc_q *mlp_q = (struct mult_perc_q*) malloc(sizeof(struct mult_perc_q));
    mlp_q->weights = alloc_deriv_holdr_q(number_of_layers, neurons_per_layer);
    return mlp_q;
}

struct mult_perc_q *alloc_mult_perc_mult_perc_q(struct mult_perc *mlp) {
    struct mult_perc_q *mlp_q = alloc_mult_perc_q(mlp->weights->number_of_layers, mlp->weights->neurons_per_layer);
    for(int i = 0; i < mlp_q->weights->number_of_weights; i++) {
        mlp_q->weights->data[i] = double_to_q(mlp->weights->data[i]);
    }
    return mlp_q;
}

void dealloc_mult_perc_q(struct mult_perc_q *mlp_q) {
    dealloc_deriv_holdr_q(mlp_q->weights);
    free(mlp_q);
}

void print_mult_perc_q(struct mult_perc_q *mlp_q) {
    print_deriv_holdr_q(mlp_q->weights);
}

void init_rand_mult_perc_q(struct mult_perc_q *mlp_q, double scale) {
    init_rand_deriv_holdr_q(mlp_q->weights, scale);
}

void copy_weights_mult_perc_q(struct mult_perc_q *src_q, struct mult_perc_q *dst_q) {
    copy_weights_deriv_holdr_q(src_q->weights, dst_q->weights);
}

void evaluate_example_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, long in_q[], long out_q[], long *error_q, int *misses) {
    int out_size = a_q->neurons_per_layer[a_q->number_of_layers - 1];
    forward_mult_perc_q(mlp_q, a_q, in_q);
    *misses = long_pointer_argmax(out_q, out_size) == long_pointer_argmax(a_q->p_sig[a_q->number_of_layers - 1], out_size) ? 0 : 1;
    long cost_q = 0L;
    for(int i = 0; i < out_size; i++) {
        long temp_q = sub_q(a_q->p_sig[a_q->number_of_layers - 1][i], out_q[i]);
        cost_q = add_q(cost_q, apx_mul_q(temp_q, temp_q));
    }
    *error_q = cost_q;
}

void evaluate_example_mult_perc_double_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, long in_q[], long out_q[], double *error, int *misses) {
    int out_size = a_q->neurons_per_layer[a_q->number_of_layers - 1];
    forward_mult_perc_q(mlp_q, a_q, in_q);
    *misses = long_pointer_argmax(out_q, out_size) == long_pointer_argmax(a_q->p_sig[a_q->number_of_layers - 1], out_size) ? 0 : 1;
    double cost = 0.0;
    for(int i = 0; i < out_size; i++) {
        double temp = q_to_double(sub_q(a_q->p_sig[a_q->number_of_layers - 1][i], out_q[i]));
        cost += temp*temp;
    }
    *error = cost;
}

void evaluate_batch_mult_perc_q(
    struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, int batch_start, int batch_size, long *error_q, int *misses
) {
    long cost_q = 0L;
    int fn = 0;
    int batch = 0;
    for(int record = batch_start; record < data_q->size && batch < batch_size; record++, batch++) {
        long *in_q = get_input_database_q(data_q, record);
        long *out_q = get_output_database_q(data_q, record);
        long temp_cost_q;
        int temp_fn;
        evaluate_example_mult_perc_q(mlp_q, a_q, in_q, out_q, &temp_cost_q, &temp_fn);
        cost_q = add_q(cost_q, temp_cost_q);
        fn += temp_fn;
    }
    *error_q = cost_q;
    *misses = fn;
}

void evaluate_batch_mult_perc_double_q(
    struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, int batch_start, int batch_size, double *error, int *misses
) {
    double cost = 0.0;
    int fn = 0;
    int batch = 0;
    for(int record = batch_start; record < data_q->size && batch < batch_size; record++, batch++) {
        long *in_q = get_input_database_q(data_q, record);
        long *out_q = get_output_database_q(data_q, record);
        double temp_cost;
        int temp_fn;
        evaluate_example_mult_perc_double_q(mlp_q, a_q, in_q, out_q, &temp_cost, &temp_fn);
        cost += temp_cost;
        fn += temp_fn;
    }
    *error = cost;
    *misses = fn;
}

void evaluate_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, long *error_q, int *misses) {
    evaluate_batch_mult_perc_q(mlp_q, a_q, data_q, 0, data_q->size, error_q, misses);
}

void evaluate_mult_perc_double_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct database_q *data_q, double *error, int *misses) {
    evaluate_batch_mult_perc_double_q(mlp_q, a_q, data_q, 0, data_q->size, error, misses);
}

void forward_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, long in_q[]) {
    for(int i = 0; i < mlp_q->weights->neurons_per_layer[0]; i++) {
        a_q->p_sig[0][i] = in_q[i];
    }
    for(int i = 1; i < mlp_q->weights->number_of_layers; i++) {
        for(int j = 0; j < mlp_q->weights->neurons_per_layer[i]; j++) {
            a_q->p_act[i-1][j] = mlp_q->weights->p_mat[i-1][j][0];            
            for(int k = 1; k < mlp_q->weights->neurons_per_layer[i-1]+1; k++) {
                a_q->p_act[i-1][j] = add_q(a_q->p_act[i-1][j], apx_mul_q(mlp_q->weights->p_mat[i-1][j][k], a_q->p_sig[i-1][k-1]));
            }
            a_q->p_sig[i][j] = sigma_q(a_q->p_act[i - 1][j]);
        }
    }
}

long numeric_grad_mult_perc_q(
    struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, int layer_idx, int neuron_idx, int weight_idx, long dweig_q, long in_q[], long out_q[]
) {
    long weig = mlp_q->weights->p_mat[layer_idx][neuron_idx][weight_idx];
    mlp_q->weights->p_mat[layer_idx][neuron_idx][weight_idx] = sub_q(mlp_q->weights->p_mat[layer_idx][neuron_idx][weight_idx], dweig_q);
    long error1_q, error2_q;
    int misses;
    evaluate_example_mult_perc_q(mlp_q, a_q, in_q, out_q, &error1_q, &misses);
    mlp_q->weights->p_mat[layer_idx][neuron_idx][weight_idx] = add_q(weig, dweig_q);
    evaluate_example_mult_perc_q(mlp_q, a_q, in_q, out_q, &error2_q, &misses);
    return nr_div_q(sub_q(error2_q, error1_q), dweig_q << 1);
}

void backpropagation_mult_perc_q(struct mult_perc_q *mlp_q, struct activ_holdr_q *a_q, struct deriv_holdr_q *d_q, long in_q[], long out_q[]) {
    forward_mult_perc_q(mlp_q, a_q, in_q);
    int l = mlp_q->weights->number_of_layers - 1;
    for(int i = 0; i < mlp_q->weights->neurons_per_layer[l]; i++) {
        d_q->p_mat[l-1][i][0] = apx_mul_q(sub_q(a_q->p_sig[l][i], out_q[i]), dsigma_q(a_q->p_sig[l - 1][i]));
        for(int j = 1; j < mlp_q->weights->neurons_per_layer[l-1]+1; j++) {
            d_q->p_mat[l-1][i][j] = apx_mul_q(d_q->p_mat[l-1][i][0], a_q->p_sig[l-1][j-1]);
        }
    }
    for(l--; l > 0; l--) {
        for(int i = 0; i < mlp_q->weights->neurons_per_layer[l]; i++) {
            long sum_q = 0L;
            for(int j = 0; j < mlp_q->weights->neurons_per_layer[l+1]; j++) {
                sum_q = add_q(sum_q, apx_mul_q(d_q->p_mat[l][j][0], mlp_q->weights->p_mat[l][j][i+1]));
            }
            d_q->p_mat[l-1][i][0] = apx_mul_q(sum_q, dsigma_q(a_q->p_act[l - 1][i]));
            for(int j = 1; j < mlp_q->weights->neurons_per_layer[l-1]+1; j++) {
                d_q->p_mat[l-1][i][j] = apx_mul_q(d_q->p_mat[l-1][i][0], a_q->p_sig[l-1][j-1]);
            }
        }
    }
}
