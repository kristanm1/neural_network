#include "q.h"


int integer_portion;
int fractional_portion;

long sat_min_q;
long sat_max_q;
long round_value;

long const_div1;
long const_div2;

long const_one;
long const_half;
long const_two_and_half;
long const_fifth;

// backtracing

long backtracing_alpha_shrink_q;

//proben1

long proben1_gd_learning_rates_q[proben1_num_dbs];

long proben1_adam_alphas_q[proben1_num_dbs];
long proben1_adam_beta1_q;
long proben1_adam_beta2_q;
long proben1_adam_epsilon_q;

long proben1_cgm_b_alpha_q[proben1_num_dbs];

long proben1_b_c1_q;

// mnist

long mnist_gd_learning_rate_q;

long mnist_adam_alpha_q;
long mnist_adam_beta1_q;
long mnist_adam_beta2_q;
long mnist_adam_epsilon_q;

long mnist_cgm_b_alpha_q;

long mnist_b_c1_q;

//

struct tab_func_q *tab_logistic_function_q;
struct tab_func_q *tab_logistic_function_derivative_q;

void set_portions_q(int ip, int fp) {
    integer_portion = ip;
    fractional_portion = fp;

    sat_min_q = -1L << (ip + fp - 1);
    sat_max_q = ~sat_min_q;
    round_value = (fp > 0) ? 1L << (fp - 1) : 0L;

    const_div1 = double_to_q(48.0/17.0);
    const_div2 = double_to_q(32.0/17.0);

    const_one = double_to_q(1.0);
    const_half = double_to_q(0.5);
    const_two_and_half = double_to_q(2.5);
    const_fifth = double_to_q(0.2);

    // backtracing

    backtracing_alpha_shrink_q = double_to_q(BACKTRACING_ALPHA_SHRINK);

    // proben1

    proben1_adam_beta1_q = double_to_q(proben1_adam_beta1);
    proben1_adam_beta2_q = double_to_q(proben1_adam_beta2);
    proben1_adam_epsilon_q = double_to_q(proben1_adam_epsilon);
    if(proben1_adam_epsilon_q == 0L) {
        proben1_adam_epsilon_q = 1L;
    }

    proben1_b_c1_q = double_to_q(proben1_b_c1);
    if(proben1_b_c1_q == 0L) {
        proben1_b_c1_q = 1L;
    }

    for(int i = 0; i < proben1_num_dbs; i++) {
        proben1_gd_learning_rates_q[i] = double_to_q(proben1_gd_learning_rates[i]);
        if(proben1_gd_learning_rates_q[i] == 0L) {
            proben1_gd_learning_rates_q[i] = 1L;
        }

        proben1_adam_alphas_q[i] = double_to_q(proben1_adam_alphas[i]);
        if(proben1_adam_alphas_q[i] == 0L) {
            proben1_adam_alphas_q[i] = 1L;
        }

        proben1_cgm_b_alpha_q[i] = double_to_q(proben1_cgm_b_alpha[i]);
        if(proben1_cgm_b_alpha_q[i] == 0L) {
            proben1_cgm_b_alpha_q[i] = 1L;
        }
    }

    // mnist
    
    mnist_gd_learning_rate_q = double_to_q(mnist_gd_learning_rate);
    if(mnist_gd_learning_rate_q == 0L) {
        mnist_gd_learning_rate_q = 1L;
    }

    mnist_adam_alpha_q = double_to_q(mnist_adam_alpha);
    if(mnist_adam_alpha_q == 0L) {
        mnist_adam_alpha_q = 1L;
    }
    mnist_adam_beta1_q = double_to_q(mnist_adam_beta1);
    mnist_adam_beta2_q = double_to_q(mnist_adam_beta2);
    mnist_adam_epsilon_q = double_to_q(mnist_adam_epsilon);
    if(mnist_adam_epsilon_q == 0L) {
        mnist_adam_epsilon_q = 1L;
    }

    mnist_b_c1_q = double_to_q(mnist_b_c1);
    mnist_cgm_b_alpha_q = double_to_q(mnist_cgm_b_alpha);
    if(mnist_cgm_b_alpha_q == 0L) {
        mnist_cgm_b_alpha_q = 1L;
    }
    
}

void set_tab_q(int number_of_points) {
    if(tab_logistic_function_q) {
        dealloc_tab_func_q(tab_logistic_function_q);
    }
    if(tab_logistic_function_derivative_q) {
        dealloc_tab_func_q(tab_logistic_function_derivative_q);
    }
    tab_logistic_function_q = alloc_tab_func_q(number_of_points, 0.0, 6.0, logistic_function);
    tab_logistic_function_derivative_q = alloc_tab_func_q(number_of_points, 0.0, 6.0, logistic_function_derivative);
}

double q_to_double(long x) {
    return ((double) x) / (1L << fractional_portion);
}

long double_to_q(double x) {
    long y = (long) (x * (1L << fractional_portion));
    if(y > sat_max_q) {
        return sat_max_q;
    } else if(y < sat_min_q) {
        return sat_min_q;
    }
    return y;
}

long int_to_q(int x) {
    long y = ((long) x) << fractional_portion;
    if(y > sat_max_q) {
        return sat_max_q;
    } else if(y < sat_min_q) {
        return sat_min_q;
    }
    return y;
}

long add_q(long a, long b) {
    long c = a + b;
    if(c > sat_max_q) {
        return sat_max_q;
    } else if(c < sat_min_q) {
        return sat_min_q;
    }
    return c;
}

long sub_q(long a, long b) {
    long c = a - b;
    if(c > sat_max_q) {
        return sat_max_q;
    } else if(c < sat_min_q) {
        return sat_min_q;
    }
    return c;
}

long mul_q(long a, long b) {
    long c = (a*b + round_value) >> fractional_portion;
    if(c > sat_max_q) {
        return sat_max_q;
    } else if(c < sat_min_q) {
        return sat_min_q;
    }
    return c;
}

void reset_mult_stat_q() {
    mult_stat_q[0] = sat_max_q;
    mult_stat_q[1] = sat_min_q;
}

long apx_mul_q(long a, long b) {
    char sa = 1, sb = 1;
    if(a < 0L) {
        sa = -1;
        a = -a;
    }
    if(b < 0L) {
        sb = -1;
        b = -b;
    }
    mult_counter_q++;
    if(a == const_one) {
        return sa * sb * b;
    }
    if(b == const_one) {
        return sa * sb * a;
    }
    long c = (sa*sb*a*b + round_value) >> fractional_portion;
    //long c = (sa*sb*((long) ma((unsigned long) a, (unsigned long) b)) + round_value) >> fractional_portion;
    //long c = (sa*sb*((long) ilm_1corr((unsigned long) a, (unsigned long) b)) + round_value) >> fractional_portion;
    //long c = (sa*sb*((long) ilm_2corr((unsigned long) a, (unsigned long) b)) + round_value) >> fractional_portion;
    if(c > sat_max_q) {
        c = sat_max_q;
    } else if(c < sat_min_q) {
        c = sat_min_q;
    }
    if(c < mult_stat_q[0]) {
        mult_stat_q[0] = c;
    }
    if(mult_stat_q[1] < c) {
        mult_stat_q[1] = c;
    }
    return c;
}

long div_q(long a, long b) {
    if(a == 0L && b == 0L) {
        printf("ERROR: q.c | div_q\n");
        return 0L;
    } else if(b == const_one) {
        return a;
    } else if(a == b) {
        return const_one;
    } else if(b == 0L) {
        return (a > 0L) ? sat_max_q : sat_min_q;
    } else if(a == 0L) {
        return 0L;
    }
    long temp = a << fractional_portion;
    if(((temp >> 63) & 1) == ((b >> 63) & 1)) {
        temp += b >> 1;
    } else {
        temp -= b >> 1;
    }
    long c = temp / b;
    if(c > sat_max_q) {
        return sat_max_q;
    } else if(c < sat_min_q) {
        return sat_min_q;
    }
    return c;
}

long nr_div_q(long a, long b) {
    if(b == 0L) {
        //printf("ERROR: q.c | nr_div_q\n");
        return 0L;
    } else if(b == const_one) {
        return a;
    } else if(a == b) {
        return const_one;
    } else if(a == 0L) {
        return 0L;
    }
    char sa = 1, sb = 1;
    if(a < 0L) {
        sa = -1;
        a = -a;
    }
    if(b < 0L) {
        sb = -1;
        b = -b;
    }
    while(b > const_one) {
        a >>= 1;
        b >>= 1;
    }
    while(b < const_half) {
        a <<= 1;
        b <<= 1;
    }
    if(a > sat_max_q) {
        a = sat_max_q;
    } else if(a < sat_min_q) {
        a = sat_min_q;
    }
    long d = sub_q(const_div1, apx_mul_q(const_div2, b));
    d = add_q(d, apx_mul_q(d, sub_q(const_one, apx_mul_q(b, d))));
    d = add_q(d, apx_mul_q(d, sub_q(const_one, apx_mul_q(b, d))));
    return sa*sb*apx_mul_q(a, d);
}

long sqrt_q(long a) {
    if(a < 0L) {
        printf("ERROR: q.c | sqrt_q\n");
        return 0L;
    } else if(a == 0L || a == const_one) {
        return a;
    }
    long b = a;
    int k = 0;
    long temp = b;
    while((temp >>= 1) > 0L) {
        k++;
    }
    k = k - fractional_portion;
    if(k & 1) {
        k += 1;
    }
    k >>= 1;
    if(k > 0) {
        b >>= k;
    } else {
        b <<= -k;
    }
    b = apx_mul_q(const_half, add_q(b, nr_div_q(a, b)));
    b = apx_mul_q(const_half, add_q(b, nr_div_q(a, b)));
    return b;
}

long logistic_function_q(long x) {
    if(x < 0L) {
        x = -x;
        if(x > tab_logistic_function_q->x[tab_logistic_function_q->size - 1]) {
            return 0L;
        }
        int i = cls_int_tab_func_q(x, tab_logistic_function_q);
        return sub_q(const_one, tab_logistic_function_q->y[i]);
    } else {
        if(x > tab_logistic_function_q->x[tab_logistic_function_q->size - 1]) {
            return const_one;
        }
        int i = cls_int_tab_func_q(x, tab_logistic_function_q);
        return tab_logistic_function_q->y[i];
    }
}

long logistic_function_derivative_q(long x) {
    if(x < 0L) {
        x = -x;
    }
    if(x > tab_logistic_function_derivative_q->x[tab_logistic_function_derivative_q->size - 1]) {
        return 0L;
    }
    int i = cls_int_tab_func_q(x, tab_logistic_function_derivative_q);
    return tab_logistic_function_derivative_q->y[i];
}


