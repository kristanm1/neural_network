#include "q.h"


void test_set_porions_q(int ip, int fp) {
    set_portions_q(ip, fp);

    printf("integer portion: %d\n", integer_portion);
    printf("fractional portion: %d\n", fractional_portion);

    printf("ONE: %f\n", q_to_double(const_one));
    printf("HALF: %f\n", q_to_double(const_half));
    printf("TWO and HALF: %f\n", q_to_double(const_two_and_half));
    
    char *min = dec_to_bin((unsigned long) sat_min_q);
    printf("min| %11.6f: %s\n", q_to_double(sat_min_q), min + 32);
    free(min);

    char *max = dec_to_bin((unsigned long) sat_max_q);
    printf("max| %11.6f: %s\n", q_to_double(sat_max_q), max + 32);
    free(max);

    printf("proben1\n");
    for(int i = 0; i < proben1_num_dbs; i++) {
        printf(" gd : %f %f\n", proben1_gd_learning_rates[i], q_to_double(proben1_gd_learning_rates_q[i]));
        printf("adam: %f %f\n", proben1_adam_alphas[i], q_to_double(proben1_adam_alphas_q[i]));
        printf(" cgm: %f %f\n\n", proben1_cgm_b_alpha[i], q_to_double(proben1_cgm_b_alpha_q[i]));
    }

    printf("beta1: %f %f\n", proben1_adam_beta1, q_to_double(proben1_adam_beta1_q));
    printf("beta2: %f %f\n", proben1_adam_beta2, q_to_double(proben1_adam_beta2_q));
    printf("epsilon: %f %f\n", proben1_adam_epsilon, q_to_double(proben1_adam_epsilon_q));

    printf("c1: %f %f\n", proben1_b_c1, q_to_double(proben1_b_c1_q));
    
}

void test_operators(int argc, char *argv[]) {
    if(argc > 4) {
        int ip = atoi(argv[1]);
        int fp = atoi(argv[2]);
        double a = atof(argv[3]);
        double b = atof(argv[4]);

        set_portions_q(ip, fp);

        double c;
        long qa = double_to_q(a);
        long qb = double_to_q(b);
        long qc;

        c = a + b;
        qc = add_q(qa, qb);
        printf("+ diff: %f | %f %f\n", c-q_to_double(qc), c, q_to_double(qc));

        c = a - b;
        qc = sub_q(qa, qb);
        printf("- diff: %f | %f %f\n", c-q_to_double(qc), c, q_to_double(qc));
        
        c = a * b;
        qc = mul_q(qa, qb);
        printf("* diff: %f | %f %f\n", c-q_to_double(qc), c, q_to_double(qc));
    }
}

void test_apx_mul(int argc, char *argv[]) {
    if(argc > 4) {
        int ip = atoi(argv[1]);
        int fp = atoi(argv[2]);
        double a = atof(argv[3]);
        double b = atof(argv[4]);
        
        set_portions_q(ip, fp);

        long qa = double_to_q(a);
        long qb = double_to_q(b);
        long qc, qc_apx;

        qc = mul_q(qa, qb);
        qc_apx = apx_mul_q(qa, qb);

        printf("%ld %ld\n", qc, qc_apx);
        printf("%10.6f %10.6f\n", q_to_double(qc), q_to_double(qc_apx));
    }
}

void test_div(int argc, char *argv[]) {
    if(argc > 4) {
        int ip = atoi(argv[1]);
        int fp = atoi(argv[2]);
        double a = atof(argv[3]);
        double b = atof(argv[4]);
        double c = a/b;
        
        set_portions_q(ip, fp);

        long qa = double_to_q(a);
        long qb = double_to_q(b);
        long qc, qc_nr;

        qc = div_q(qa, qb);
        qc_nr = nr_div_q(qa, qb);

        printf("%10.6f %10.6f %10.6f\n", c, q_to_double(qc), q_to_double(qc_nr));

    }
}

void test_sqrt(int argc, char *argv[]) {
    if(argc > 3) {
        int ip = atoi(argv[1]);
        int fp = atoi(argv[2]);
        double a = atof(argv[3]);
        double b;
        
        set_portions_q(ip, fp);

        long qa = double_to_q(a);
        long qb;

        b = sqrt(a);
        qb = sqrt_q(qa);

        printf("%10.6f %10.6f\n", b, q_to_double(qb));

    }
}

void test_activation_function(int ip, int fp, int number_of_points) {

    char sigmoid_filename[128];
    char dsigmoid_filename[128];
    sprintf(sigmoid_filename, "sigmoid_%d_%d_%d.txt", ip, fp, number_of_points);
    sprintf(dsigmoid_filename, "dsigmoid_%d_%d_%d.txt", ip, fp, number_of_points);

    set_portions_q(ip, fp);
    set_tab_q(number_of_points);

    FILE *f = fopen(sigmoid_filename, "w");
    FILE *df = fopen(dsigmoid_filename, "w");

    double x0 = -6.0, xn = 6.0;
    double dx = 0.01;

    for(double x = x0; x < xn; x += dx) {
        long x_q = double_to_q(x);
        fprintf(f, "%e %e %e %e\n", x, logistic_function(x), q_to_double(x_q), q_to_double(logistic_function_q(x_q)));
        fprintf(df, "%e %e %e %e\n", x, logistic_function_derivative(x), q_to_double(x_q), q_to_double(logistic_function_derivative_q(x_q)));
    }

    fclose(f);
    fclose(df);

}

void test_div_function(double nominator, int ip, int fp) {

    char sigmoid_filename[128];

    set_portions_q(ip, fp);

    long nominator_q = double_to_q(nominator);

    sprintf(sigmoid_filename, "div_%g_%d_%d.txt", nominator, ip, fp);
    //sprintf(sigmoid_filename, "div_%g_%d_%d_ma.txt", nominator, ip, fp);
    //sprintf(sigmoid_filename, "div_%g_%d_%d_ilm.txt", nominator, ip, fp);
    FILE *f = fopen(sigmoid_filename, "w");

    for(long x_q = const_one; x_q < sat_max_q; x_q += const_half + (rand() % (1 << fp))) {
        double x = q_to_double(x_q);
        fprintf(f, "%e %e %e\n", x, nominator/x, q_to_double(nr_div_q(nominator_q, x_q)));
    }

    fclose(f);

}

void test_sqrt_function(int ip, int fp) {

    char sigmoid_filename[128];

    set_portions_q(ip, fp);

    sprintf(sigmoid_filename, "sqrt_%d_%d.txt", ip, fp);
    //sprintf(sigmoid_filename, "sqrt_%d_%d_ma.txt", ip, fp);
    //sprintf(sigmoid_filename, "sqrt_%d_%d_ilm.txt", ip, fp);
    FILE *f = fopen(sigmoid_filename, "w");

    for(long x_q = 0; x_q < sat_max_q; x_q += const_half + (rand() % (1 << fp))) {
        double x = q_to_double(x_q);
        fprintf(f, "%e %e %e\n", x, sqrt(x), q_to_double(sqrt_q(x_q)));
    }

    fclose(f);

}


int main(int argc, char *argv[]) {

    //test_set_porions_q(10, 12);

    //test_operators(argc, argv);
    //test_apx_mul(argc, argv);
    //test_div(argc, argv);
    //test_sqrt(argc, argv);

    //test_activation_function(8, 8, 32);

    test_div_function(128.0, 8, 8);
    //test_div_function(-1.0, 8, 8);

    //test_sqrt_function(8, 8);

    return 0;
}