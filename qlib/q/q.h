#ifndef _Q
#define _Q

    #include "../../lib/utils/utils.h"
    #include "../../lib/PROBEN_loader/proben_loader.h"
    #include "../../lib/optimizer/optimizer.h"
    #include "../tab_func_q/tab_func_q.h"


    // https://en.wikipedia.org/wiki/Q_(number_format)

    extern int integer_portion;
    extern int fractional_portion;

    extern long sat_max_q;
    extern long sat_min_q;
    extern long round_value;

    extern long const_div1;
    extern long const_div2;

    extern long const_one;
    extern long const_half;
    extern long const_two_and_half;
    extern long const_fifth;

    // backtracing

    extern long backtracing_alpha_shrink_q;

    // proben1

    extern long proben1_gd_learning_rates_q[];

    extern long proben1_adam_alphas_q[];
    extern long proben1_adam_beta1_q;
    extern long proben1_adam_beta2_q;
    extern long proben1_adam_epsilon_q;
    
    extern long proben1_cgm_b_alpha_q[];

    extern long proben1_b_c1_q;

    // mnist

    extern long mnist_gd_learning_rate_q;

    extern long mnist_adam_alpha_q;
    extern long mnist_adam_beta1_q;
    extern long mnist_adam_beta2_q;
    extern long mnist_adam_epsilon_q;

    extern long mnist_b_c1_q;
    extern long mnist_cgm_b_alpha_q;

    //

    extern long mult_counter_q;

    extern long mult_stat_counter;
    extern long mult_stat_q[];

    extern struct tab_func_q *tab_logistic_function_q;
    extern struct tab_func_q *tab_logistic_function_derivative_q;

    // ip > 0 && fp >= 0 && ip + fp <= 32
    void set_portions_q(int ip, int fp);
    void set_tab_q(int number_of_points);

    double q_to_double(long x);
    long double_to_q(double x);
    long int_to_q(int x);
    long add_q(long a, long b);
    long sub_q(long a, long b);
    long mul_q(long a, long b);

    void reset_mult_stat();
    long apx_mul_q(long a, long b);

    long div_q(long a, long b);
    long nr_div_q(long a, long b);
    long sqrt_q(long a);

    long logistic_function_q(long x);
    long logistic_function_derivative_q(long x);


#endif