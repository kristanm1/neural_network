import locale
import numpy as np
import matplotlib.pyplot as plt


def load(filename):
    with open(filename) as file:
        data = []
        for line in file.readlines():
            data.append([float(x) for x in line.rstrip().split(' ')])
        return np.array(data)


def plot_sigmoid(ip, fp, number_of_points):
    data = load('sigmoid_{:d}_{:d}_{:d}.txt'.format(ip, fp, number_of_points))
    ddata = load('dsigmoid_{:d}_{:d}_{:d}.txt'.format(ip, fp, number_of_points))

    fig1, ax1 = plt.subplots(1, 1)
    fig1.set_size_inches(5, 4.5)
    #ax1.set_title('Logistična funkcija', size=30)
    ax1.plot(data[:, 0], data[:, 1], 'k--', linewidth=0.75)
    ax1.plot(data[:, 2], data[:, 3], 'k-')
    ax1.tick_params(axis='both', which='major', labelsize=15)
    ax1.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()

    fig2, ax2 = plt.subplots(1, 1)
    fig2.set_size_inches(5, 4.5)
    #ax2.set_title('Odvod logistične funkcije', size=30)
    ax2.plot(ddata[:, 0], ddata[:, 1], 'k--', linewidth=0.75)
    ax2.plot(ddata[:, 2], ddata[:, 3], 'k-')
    ax2.tick_params(axis='both', which='major', labelsize=15)
    ax2.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()

    plt.show()


def plot_div(nominator, ip, fp):

    data = load('div_{:g}_{:d}_{:d}.txt'.format(nominator, ip, fp))    
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(5, 5)
    ax.plot(data[:, 0], data[:, 1], 'k--', linewidth=1.0)
    ax.plot(data[:, 0], data[:, 2], 'k-', linewidth=1.75)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()
    plt.savefig('div_{:g}_{:d}_{:d}.png'.format(nominator, ip, fp))
    plt.close('all')

    data_ma = load('div_{:g}_{:d}_{:d}_ma.txt'.format(nominator, ip, fp))
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(5, 5)
    ax.plot(data_ma[:, 0], data_ma[:, 1], 'k--', linewidth=1.0)
    ax.plot(data_ma[:, 0], data_ma[:, 2], 'k-', linewidth=1.75)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()
    plt.savefig('div_{:g}_{:d}_{:d}_ma.png'.format(nominator, ip, fp))
    plt.close('all')

    data_ilm = load('div_{:g}_{:d}_{:d}_ilm.txt'.format(nominator, ip, fp))
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(5, 5)
    ax.plot(data_ilm[:, 0], data_ilm[:, 1], 'k--', linewidth=1.0)
    ax.plot(data_ilm[:, 0], data_ilm[:, 2], 'k-', linewidth=1.75)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()
    plt.savefig('div_{:g}_{:d}_{:d}_ilm.png'.format(nominator, ip, fp))
    plt.close('all')


def plot_sqrt(ip, fp):

    data = load('sqrt_{:d}_{:d}.txt'.format(ip, fp))
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(5, 5)
    ax.plot(data[:, 0], data[:, 1], 'k--', linewidth=1.0)
    ax.plot(data[:, 0], data[:, 2], 'k-', linewidth=1.75)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()
    plt.savefig('sqrt_{:d}_{:d}.png'.format(ip, fp))
    plt.close('all')

    data_ma = load('sqrt_{:d}_{:d}_ma.txt'.format(ip, fp))
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(5, 5)
    ax.plot(data_ma[:, 0], data_ma[:, 1], 'k--', linewidth=1.0)
    ax.plot(data_ma[:, 0], data_ma[:, 2], 'k-', linewidth=1.75)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()
    plt.savefig('sqrt_{:d}_{:d}_ma.png'.format(ip, fp))
    plt.close('all')

    data_ilm = load('sqrt_{:d}_{:d}_ilm.txt'.format(ip, fp))
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(5, 5)
    ax.plot(data_ilm[:, 0], data_ilm[:, 1], 'k--', linewidth=1.0)
    ax.plot(data_ilm[:, 0], data_ilm[:, 2], 'k-', linewidth=1.75)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.ticklabel_format(axis='both', useLocale=True)
    plt.tight_layout()
    plt.savefig('sqrt_{:d}_{:d}_ilm.png'.format(ip, fp))
    plt.close('all')

    


#plot_sigmoid(8, 8, 32);

#plot_div(1.0, 8, 8)
#plot_div(128.0, 8, 8)

#plot_sqrt(8, 8)
