#include "tab_func_q.h"


double poly(double x) {
    return 2.0*x*x - 3.0*x - 1.0;
}

void test_tab_func_q(double (*f)(double)) {

    struct tab_func_q *tab_q = alloc_tab_func_q(11, 0.0, 10.0, f);

    print_tab_func_q(tab_q);

    double x = 3.7;
    long x_q = double_to_q(x);
    int i = cls_int_tab_func_q(x_q, tab_q);
    printf("\n%10.4f %10.4f\n", x ,f(x));
    printf("%10.4f %10.4f\n", q_to_double(tab_q->x[i]), q_to_double(tab_q->y[i]));

    dealloc_tab_func_q(tab_q);

}

int main(int argc, char *argv[]) {

    set_portions_q(10, 22);
    test_tab_func_q(poly);

    return 0;
}
