#ifndef _TAB_FUNC_Q
#define _TAB_FUNC_Q

    #include "../q/q.h"


    // tabulated function structure
    struct tab_func_q {
        int size;       // number of points in tabel
        long *x;        // x coordinares of points
        long *y;        // y coordinares of points
    };

    struct tab_func_q *alloc_tab_func_q(int size, double low, double high, double (*func) (double));
    void dealloc_tab_func_q(struct tab_func_q *tab_q);
    void print_inf_tab_func_q(struct tab_func_q *tab);
    void print_tab_func_q(struct tab_func_q *tab_q);

    // return index of point with nearest smaller x coordinate compared to arg. x
    // cls ... closest smaller
    int cls_int_tab_func_q(long x, struct tab_func_q *tab_q);

#endif