#include "tab_func_q.h"


struct tab_func_q *alloc_tab_func_q(int size, double low, double high, double (*func) (double)) {
    struct tab_func_q *tab_q = (struct tab_func_q*) malloc(sizeof(struct tab_func_q));
    tab_q->size = size;
    tab_q->x = (long*) malloc(sizeof(long) * (2 * size));
    tab_q->y = tab_q->x + size;
    double dx = (high - low)/(size - 1);
    double x = low;
    for(int i = 0; i < size; i++) {
        double temp = (x + (i*dx + low))/2.0;
        tab_q->x[i] = double_to_q(temp);
        tab_q->y[i] = double_to_q(func(temp));
        x += dx;
    }
    return tab_q;
}

void dealloc_tab_func_q(struct tab_func_q *tab_q) {
    free(tab_q->x);
    free(tab_q);
}

void print_inf_tab_func_q(struct tab_func_q *tab_q) {
    printf("tab_func_q:\n");
    printf(" size: %d [%g, %g]\n", tab_q->size, q_to_double(tab_q->x[0]), q_to_double(tab_q->y[-1]));
}

void print_tab_func_q(struct tab_func_q *tab_q) {
    printf("     x:     |    y:  \n");
    for(int i = 0; i < tab_q->size; i++) {
        printf("%11.5f | %11.5f\n", q_to_double(tab_q->x[i]), q_to_double(tab_q->y[i]));
    }
}

int cls_int_tab_func_q(long x_q, struct tab_func_q *tab_q) {
    long diff;
    diff = sub_q(x_q, tab_q->x[0]);
    if(-1L <= diff && diff <= 1L) {
        return 0;
    }
    diff = sub_q(x_q, tab_q->y[-1]);
    if(-1L <= diff && diff <= 1L) {
        return tab_q->size - 1;
    }
    int low = 0;
    int mid = 1;
    int high = tab_q->size;
    while(low + 1 < high) {
        mid = (low + high) >> 1;
        if(tab_q->x[mid] < x_q) {
            low = mid;
        } else {
            high = mid;
        }
    }
    return low;
}
