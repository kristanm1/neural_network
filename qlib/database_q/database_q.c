#include "database_q.h"


struct database_q *alloc_database_q(int size, int in_size, int out_size) {
    struct database_q *data_q = (struct database_q*) malloc(sizeof(struct database_q));
    data_q->size = size;
    data_q->in_size = in_size;
    data_q->out_size = out_size;
    data_q->order = (int*) malloc(sizeof(int)*size);
    data_q->array = (long*) malloc(sizeof(long)*size*(in_size + out_size));
    data_q->input = (long**) malloc(sizeof(long*)*2*size);
    data_q->output = data_q->input + size;
    int offset_in = 0;
    int offset_out = size * in_size;
    for(int i = 0; i < size; i++) {
        data_q->order[i] = i;
        data_q->input[i] = data_q->array + offset_in;
        data_q->output[i] = data_q->array + offset_out;
        offset_in += in_size;
        offset_out += out_size;
    }
    return data_q;
}

struct database_q *load_database_database_q(struct database *data) {
    struct database_q *data_q = alloc_database_q(data->size, data->in_size, data->out_size);
    for(int i = 0; i < data_q->size*(data_q->in_size + data_q->out_size); i++) {
        data_q->array[i] = double_to_q(data->array[i]);
    }
    return data_q;
}

void dealloc_database_q(struct database_q *data_q) {
    free(data_q->order);
    free(data_q->array);
    free(data_q->input);
    free(data_q);
}

long *get_input_database_q(struct database_q *data_q, int rec) {
    return data_q->input[data_q->order[rec]];
}

long *get_output_database_q(struct database_q *data_q, int rec) {
    return data_q->output[data_q->order[rec]];
}

void print_inf_database_q(struct database_q *data_q) {
    printf("database info:\n size: %d\n in_size: %d\n out_size: %d\n", data_q->size, data_q->in_size, data_q->out_size);
}

void print_class_database_q(struct database_q *data_q) {
    int count[data_q->out_size];
    for(int i = 0; i < data_q->out_size; i++) {
        count[i] = 0;
    }
    for(int i = 0; i < data_q->size; i++) {
        count[long_pointer_argmax(get_output_database_q(data_q, i), data_q->out_size)]++;
    }
    printf("class: number of records\n");
    for(int i = 0; i < data_q->out_size; i++) {
        printf("%2d: %d\n", i, count[i]);
    }
}

void print_rec_database_q(struct database_q *data_q) {
    long *temp;
    for(int i = 0; i < data_q->size; i++) {
        printf("%2d: %2d | ", i, data_q->order[i]);
        temp = get_input_database_q(data_q, i);
        for(int j = 0; j < data_q->in_size; j++) {
            printf("%5.2f ", q_to_double(temp[j]));
        }
        printf("| ");
        temp = get_output_database_q(data_q, i);
        for(int j = 0; j < data_q->out_size; j++) {
            printf("%5.2f ", q_to_double(temp[j]));
        }
        printf("\n");
    }
}

void shuffle_database_q(struct database_q *data_q) {
    for(int i = data_q->size-1; i > 0; i--) {
        int j = rand() % (i + 1);
        if(j < i) {
            int temp = data_q->order[i];
            data_q->order[i] = data_q->order[j];
            data_q->order[j] = temp;
        }
    }
}
