#ifndef _Q_DATABASE
#define _Q_DATABASE

    #include "../q/q.h"
    #include "../../lib/database/database.h"


    struct database_q {
        int size;           // number of records (record is pair of vectors: [input vector, output vector])
        int in_size;        // size of input vector
        int out_size;       // size of output vector
        int *order;         // record order
        long *array;        // data
        long **input;       // input vectors (pointers to data)
        long **output;      // output vectors (pointers to data)
    };

    struct database_q *alloc_database_q(int size, int in_size, int out_size);
    struct database_q *load_database_database_q(struct database *data);
    void dealloc_database_q(struct database_q *data_q);
    long *get_input_database_q(struct database_q *data_q, int rec);
    long *get_output_database_q(struct database_q *data_q, int rec);
    void print_inf_database_q(struct database_q *data_q);
    void print_class_database_q(struct database_q *data_q);
    void print_rec_database_q(struct database_q *data_q);
    void shuffle_database_q(struct database_q *data_q);

#endif