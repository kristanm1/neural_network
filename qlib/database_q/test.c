#include "database_q.h"
#include "../mult_perc_q/mult_perc_q.h"


void avg_abs_err(struct database *const data) {
    struct database_q *const data_q = load_database_database_q(data);
    double in_err = 0.0, out_err = 0.0;
    const double *temp;
    const long *temp_q;
    for(int i = 0; i < data->size; i++) {
        temp = get_input_database(data, i);
        temp_q = get_input_database_q(data_q, i);
        double err = 0.0;
        for(int j = 0; j < data->in_size; j++) {
            double e = temp[j] - q_to_double(temp_q[j]);
            if(e < 0) {
                err -= e;
            } else {
                err += e;
            }
        }
        in_err += err/data->in_size;

        temp = get_output_database(data, i);
        temp_q = get_output_database_q(data_q, i);
        err = 0.0;
        for(int j = 0; j < data->out_size; j++) {
            double e = temp[j] - q_to_double(temp_q[j]);
            if(e < 0) {
                err -= e;
            } else {
                err += e;
            }
        }
        out_err += err/data->out_size;
    }
    printf("abs. err. | in: %f out: %f\n", in_err, out_err);
}

void test_xor_err() {
    struct database *const data = load_xor_database();
    avg_abs_err(data);
    dealloc_database(data);
}

void test_mnist_err() {
    char images_filename[] = "../../db/MNIST_unzipped/train-images.idx3-ubyte";
    char labels_filename[] = "../../db/MNIST_unzipped/train-labels.idx1-ubyte";
    struct database *const data = load_mnist_database(images_filename, labels_filename);
    avg_abs_err(data);
    dealloc_database(data);
}

void test_records(struct database *const data, struct database_q *const data_q, int n) {
    const double *const in = get_input_database(data, n);
    const long *const in_q = get_input_database_q(data_q, n);
    for(int i = 0; i < data->in_size; i++) {
        printf("%.8f %.8f\n", in[i], q_to_double(in_q[i]));
    }
    printf("\n");
    const double *const out = get_output_database(data, n);
    const long *const out_q = get_output_database_q(data_q, n);
    for(int i = 0; i < data->out_size; i++) {
        printf("%.8f %.8f\n", out[i], q_to_double(out_q[i]));
    }
}

void test_xor() {
    struct database *const data = load_xor_database();
    struct database_q *const data_q = load_database_database_q(data);

    test_records(data, data_q, 3);

    dealloc_database(data);
    dealloc_database_q(data_q);
}

void test_evaluate_mult_perc() {

    char *images_filename = "../../db/MNIST_unzipped/t10k-images.idx3-ubyte";
    char *labels_filename = "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte";

    struct database *const data = load_mnist_database(images_filename, labels_filename);
    struct database_q *const data_q = load_database_database_q(data);

    int size = 3, layers[] = {28*28, 3, 10};
    struct mult_perc *const mlp = alloc_mult_perc(size, layers);
    struct activ_holdr *a = alloc_activ_holdr(size, layers);
    init_rand_mult_perc(mlp, 1.0);
    struct mult_perc_q *const mlp_q = alloc_mult_perc_mult_perc_q(mlp);
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(size, layers);


    double mse, tpr;

    printf("   MSE           TPR   \n");
    evaluate_mult_perc(mlp, a, data, &mse, &tpr);
    printf("%.8f | %.8f\n", mse, tpr);

    evaluate_mult_perc_q(mlp_q, a_q, data_q, &mse, &tpr);
    printf("%.8f | %.8f\n", mse, tpr);
    

    dealloc_activ_holdr(a);
    dealloc_activ_holdr_q(a_q);
    dealloc_mult_perc(mlp);
    dealloc_mult_perc_q(mlp_q);
    dealloc_database(data);
    dealloc_database_q(data_q);

}

int main(int argc, char *argv[]) {
    int tab_func_size = 10000;
    double tab_func_low = -8.0, tab_func_high = 8.0;
    set_num_preserved_bits(52);
    set_portions_q(4, 12);
    set_tab_func(tab_func_size, tab_func_low, tab_func_high);
    set_tab_func_q(tab_func_size, tab_func_low, tab_func_high);

    //test_xor_err();
    //test_mnist_err();

    //test_xor();

    test_evaluate_mult_perc();

    return 0;
}
