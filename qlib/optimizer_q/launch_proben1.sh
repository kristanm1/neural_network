#!/bin/bash

method=$1
ip=$2
fp=$3

for ((db=2; db<=4; db++))
do
    for ((layers=3; layers<=5; layers++))
    do
        for ((seed=1000; seed<=30000; seed+=1000))
        do
            sbatch run_proben1.sh $db $seed $layers $method $ip $fp
        done
    done
done

