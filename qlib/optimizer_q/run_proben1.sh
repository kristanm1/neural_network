#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --job-name=PROBEN1_q
#SBATCH --mem-per-cpu=128
#SBATCH --time=6:00:00

db=$1
seed=$2
layers=$3
method=$4
ip=$5
fp=$6

./test 0 $db $seed $layers $method $ip $fp

