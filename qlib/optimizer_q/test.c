#include "optimizer_q.h"
#include "../database_q/database_q.h"
#include "../mult_perc_q/mult_perc_q.h"
#include "../../lib/PROBEN_loader/proben_loader.h"


char model[128] = "/FV_%d_%d_ilm";


void optimize_proben1_gd_q(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);

    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);
    srand(seed);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(number_of_layers, neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/gd"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    long learning_rate_q = proben1_gd_learning_rates_q[db];
    struct optim_q *opt_q = alloc_gd_optim_q(batch_size, consecutive_allowed, learning_rate_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(number_of_layers, neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_activ_holdr_q(a_q);

    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);

}

void optimize_proben1_adam_q(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);
    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);

    srand(seed);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(number_of_layers, neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/adam"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    long alpha_q = proben1_adam_alphas_q[db];
    long beta1_q = proben1_adam_beta1_q;
    long beta2_q = proben1_adam_beta2_q;
    long epsilon_q = proben1_adam_epsilon_q;
    struct optim_q *opt_q = alloc_adam_optim_q(batch_size, consecutive_allowed, alpha_q, beta1_q, beta2_q, epsilon_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(number_of_layers, neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_activ_holdr_q(a_q);

    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);

}

void optimize_proben1_cgm_q(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);
    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);

    srand(seed);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(number_of_layers, neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/cgm"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    long c1_q = proben1_b_c1_q;
    long alpha_q = proben1_cgm_b_alpha_q[db];
    struct optim_q *opt_q = alloc_cgm_optim_q(batch_size, consecutive_allowed, c1_q, alpha_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(number_of_layers, neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_activ_holdr_q(a_q);

    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);

}

void optimize_proben1_bfgs_q(int db, int seed) {

    struct database *train, *valid, *test;
    load_proben_database(proben1_db_paths[db], &train, &valid, &test);
    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);

    srand(seed);

    int number_of_layers = proben1_number_of_layers;
    int *neurons_per_layer = proben1_neurons_per_layer + number_of_layers*db;

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(number_of_layers, neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, proben1_init_weights);

    char path[] = "../../proben1_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/bfgs"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", proben1_number_of_layers); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%s", proben1_db_names[db]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = proben1_batch_sizes[db];
    int consecutive_allowed = proben1_consecutive_allowed[db];
    long c1_q = proben1_b_c1_q;
    long alpha_q = proben1_cgm_b_alpha_q[db];
    struct optim_q *opt_q = alloc_bfgs_optim_q(batch_size, consecutive_allowed, c1_q, alpha_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(number_of_layers, neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_activ_holdr_q(a_q);

    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);

}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

void optimize_mnist_gd_q(int seed) {
    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(seed);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, mnist_init_weights);

    char path[] = "../../mnist_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/gd"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", mnist_neurons_per_layer[1]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = mnist_batch_size;
    int consecutive_allowed = mnist_consecutive_allowed;
    long learning_rate_q = mnist_gd_learning_rate_q;

    struct optim_q *opt_q = alloc_gd_optim_q(batch_size, consecutive_allowed, learning_rate_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mnist_number_of_layers, mnist_neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);
    dealloc_activ_holdr_q(a_q);
    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
}

void optimize_mnist_adam_q(int seed) {
    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(seed);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, mnist_init_weights);

    char path[] = "../../mnist_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/adam"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", mnist_neurons_per_layer[1]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = mnist_batch_size;
    int consecutive_allowed = mnist_consecutive_allowed;
    long alpha_q = mnist_adam_alpha_q;
    long beta1_q = mnist_adam_beta1_q;
    long beta2_q = mnist_adam_beta2_q;
    long epsilon_q = mnist_adam_epsilon_q;

    struct optim_q *opt_q = alloc_adam_optim_q(batch_size, consecutive_allowed, alpha_q, beta1_q, beta2_q, epsilon_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mnist_number_of_layers, mnist_neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);
    dealloc_activ_holdr_q(a_q);
    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
}

void optimize_mnist_cgm_q(int seed) {
    struct database *mnist_train = load_mnist_database("../../db/MNIST_unzipped/train-images.idx3-ubyte", "../../db/MNIST_unzipped/train-labels.idx1-ubyte");
    struct database *train, *valid;
    srand(seed);
    shuffle_database(mnist_train);
    split_uniform_class_subset_database(mnist_train, &valid, &train, 1.0/6.0);
    struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
    struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
    dealloc_database(mnist_train);
    struct database *test = load_mnist_database("../../db/MNIST_unzipped/t10k-images.idx3-ubyte", "../../db/MNIST_unzipped/t10k-labels.idx1-ubyte");
    struct database_q *test_q = load_database_database_q(test); dealloc_database(test);

    struct mult_perc_q *mlp_q = alloc_mult_perc_q(mnist_number_of_layers, mnist_neurons_per_layer);
    init_rand_mult_perc_q(mlp_q, mnist_init_weights);

    char path[] = "../../mnist_results", temp[512];
    int p_temp;
    p_temp = sprintf(temp, "%s", path); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/cgm"); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", mnist_neurons_per_layer[1]); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, model, integer_portion, fractional_portion); mkdir_ifnex(temp);
    p_temp += sprintf(temp + p_temp, "/%d", seed); mkdir_ifnex(temp);

    int batch_size = mnist_batch_size;
    int consecutive_allowed = mnist_consecutive_allowed;
    long c1_q = mnist_b_c1_q;
    long alpha_q = mnist_cgm_b_alpha_q;

    struct optim_q *opt_q = alloc_cgm_optim_q(batch_size, consecutive_allowed, c1_q, alpha_q);
    if(seed <= 3000) {
        opt_q->write_to_files = 1;
        opt_q->path = temp;
        opt_q->path_offset = p_temp;
    }

    reset_mult_stat_q();

    opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);

    sprintf(temp + p_temp, "/mult_stat.txt");
    FILE *mult_stat_file = fopen(temp, "w");
    fprintf(mult_stat_file, "%e %e\n", q_to_double(mult_stat_q[0]), q_to_double(mult_stat_q[1]));
    fclose(mult_stat_file);

    double error;
    int misses;
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mnist_number_of_layers, mnist_neurons_per_layer);
    evaluate_mult_perc_double_q(mlp_q, a_q, test_q, &error, &misses);

    sprintf(temp + p_temp, "/result.txt");
    FILE *result_file = fopen(temp, "w");
    fprintf(result_file, "%ld %d %e %e %e %ld\n", opt_q->iterations, opt_q->epochs, error, ((double) misses)/test_q->size, opt_q->duration, opt_q->multiplications);
    fclose(result_file);
    dealloc_database_q(train_q);
    dealloc_database_q(valid_q);
    dealloc_database_q(test_q);
    dealloc_activ_holdr_q(a_q);
    dealloc_optim_q(opt_q);
    dealloc_mult_perc_q(mlp_q);
}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

void proben1_args_parser(int argc, char *argv[]) {
    int db = atoi(argv[2]);
    int seed = atoi(argv[3]);
    int layers = atoi(argv[4]);
    int method = atoi(argv[5]);
    int ip = atoi(argv[6]);
    int fp = atoi(argv[7]);

    if(layers == 3) {
        proben1_number_of_layers = 3;
        proben1_neurons_per_layer = (int*) proben1_3_layer;
    } else if(layers == 4) {
        proben1_number_of_layers = 4;
        proben1_neurons_per_layer = (int*) proben1_4_layer;
    } else if(layers == 5) {
        proben1_number_of_layers = 5;
        proben1_neurons_per_layer = (int*) proben1_5_layer;
    } else {
        return;
    }

    printf("db: %s\n", proben1_db_names[db]);
    printf("seed: %d\n", seed);
    printf("layers: %d\n", layers);
    printf("ip: %d\n", ip);
    printf("fp: %d\n", fp);

    set_portions_q(ip, fp);
    set_tab_q(32);
    set_sigmoid_q();

    if(method == 0) {
        printf("gd\n");
        optimize_proben1_gd_q(db, seed);
    } else if(method == 1) {
        printf("adam\n");
        optimize_proben1_adam_q(db, seed);
    } else if(method == 2) {
        printf("cgm\n");
        optimize_proben1_cgm_q(db, seed);
    } else if(method == 3) {
        printf("bfgs\n");
        optimize_proben1_bfgs_q(db, seed);
    }
}

void mnist_args_parser(int argc, char *argv[]) {
    int seed = atoi(argv[2]);
    int hidden_neurons = atoi(argv[3]);
    int method = atoi(argv[4]);
    int ip = atoi(argv[5]);
    int fp = atoi(argv[6]);

    set_portions_q(ip, fp);
    set_tab_q(32);
    set_sigmoid_q();
    mnist_neurons_per_layer[1] = hidden_neurons;

    printf("seed: %d\n", seed);
    printf("hidden neurons: %d\n", hidden_neurons);
    printf("method: %d\n", method);
    printf("ip: %d\n", ip);
    printf("fp: %d\n", fp);

    if(method == 0) {
        printf("gd\n");
        optimize_mnist_gd_q(seed);
    } else if(method == 1) {
        printf("adam\n");
        optimize_mnist_adam_q(seed);
    } else if(method == 2) {
        printf("cgm\n");
        optimize_mnist_cgm_q(seed);
    }
}

void args_parser(int argc, char *argv[]) {
    if(argc >= 6) {
        int db = atoi(argv[1]);

        if(db == 0) {
            if(argc >= 7) {
                proben1_args_parser(argc, argv);
            }
        } else if(db == 1) {
            mnist_args_parser(argc, argv);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------


int main(int argc, char *argv[]) {

/*    
    int db = 0, seed = 1000;
    proben1_number_of_layers = 5;
    proben1_neurons_per_layer = (int*) proben1_5_layer;
    set_portions_q(11, 21);
    set_tab_q(32);
    set_sigmoid_q();

    optimize_proben1_gd_q(db, seed);
    optimize_proben1_adam_q(db, seed);
    optimize_proben1_cgm_q(db, seed);
    optimize_proben1_bfgs_q(db, seed);
*/    

    // argv[0] = 0 -> db, seed, layers, method, ip, fp
    // argv[1] = 1 -> seed, num.neurons, method, ip, fp
    args_parser(argc, argv);

    return 0;
}
