#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --job-name=MNIST_q
#SBATCH --mem-per-cpu=4096
#SBATCH --time=5-00

seed=$1
num_neurons=$2
method=$3
ip=$4
fp=$5

./test 1 $seed $num_neurons $method $ip $fp

