#include "optimizer_q.h"


long mult_counter_q;

long mult_stat_q[2];


long evaluate_ls_q(
    struct mult_perc_q *mlp_q, struct mult_perc_q *temp_mlp_q, struct activ_holdr_q *a_q, struct deriv_holdr_q *d_q, long t_q,
    struct database_q *data_q, int batch_start, int batch_size
) {
    for(int i = 0; i < mlp_q->weights->number_of_weights; i++) {
        temp_mlp_q->weights->data[i] = add_q(mlp_q->weights->data[i], apx_mul_q(t_q, d_q->data[i]));
    }
    long error_q;
    int misses;
    evaluate_batch_mult_perc_q(temp_mlp_q, a_q, data_q, batch_start, batch_size, &error_q, &misses);
    return error_q;
}

long backtracing_q(
    struct mult_perc_q *mlp_q, struct mult_perc_q *temp_mlp_q, struct activ_holdr_q *a_q, struct deriv_holdr_q *dir_q, struct deriv_holdr_q *grad_q,
    struct database_q *valid_q, int batch_start, int batch_size,
    long c1_q, long alpha_q
) {
    long derphi0_q = 0L, alphao_q = -1L;
    for(int i = 0; i < dir_q->number_of_weights; i++) {
        derphi0_q = add_q(derphi0_q, apx_mul_q(dir_q->data[i], grad_q->data[i]));
    }
    if(derphi0_q < 0L) {
        long phi0_q;
        int misses;
        evaluate_batch_mult_perc_q(mlp_q, a_q, valid_q, batch_start, batch_size, &phi0_q, &misses);
        while(alpha_q > 0L && alphao_q != alpha_q) {
            long phi_a_q = evaluate_ls_q(mlp_q, temp_mlp_q, a_q, dir_q, alpha_q, valid_q, batch_start, batch_size);
            if(phi_a_q <= add_q(phi0_q, apx_mul_q(c1_q, apx_mul_q(alpha_q, derphi0_q)))) {
                return alpha_q;
            }
            alphao_q = alpha_q;
            alpha_q = apx_mul_q(alpha_q, backtracing_alpha_shrink_q);
        }
    }
    return 0L;
}

void dealloc_optim_q(struct optim_q *opt_q) {
    free(opt_q);
}

void gd_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q) {
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *d_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *g_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct mult_perc_q *opt_mlp_q = alloc_mult_perc_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    long min_valid_error_q;
    int min_valid_misses;
    evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &min_valid_error_q, &min_valid_misses);
    copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    if(opt_q->write_to_files) {
        char mode[] = "w";
        sprintf(opt_q->path + opt_q->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt_q->path, mode);
        fprintf(parameters_file, "gradient descent\n");
        fprintf(parameters_file, "batch size: %d\n", opt_q->batch_size);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt_q->consecutive_allowed);
        fprintf(parameters_file, "learning rate: %e\n", q_to_double(opt_q->alpha_q));
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp_q->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp_q->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp_q->weights->neurons_per_layer[i]);
        }
        fprintf(parameters_file, "tabulated points: %d\n", tab_logistic_function_q->size);
        fclose(parameters_file);
        sprintf(opt_q->path + opt_q->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt_q->path, mode);
        reset_statistics_deriv_holdr_q(mlp_q->weights);
        update_statistics_deriv_holdr_q(mlp_q->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            q_to_double(min_valid_error_q), ((double) min_valid_misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
            sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
        );
        sprintf(opt_q->path + opt_q->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt_q->path, mode);
    }
    mult_counter_q = 0L;
    int consecutive = 0;
    long error_q;
    int misses;
    clock_t start_time = clock();

    long iteration = 0;
    int epochs = 0;

    while(consecutive < opt_q->consecutive_allowed) {

        reset_statistics_activ_holdr_q(a_q);
        reset_statistics_deriv_holdr_q(d_q);
        reset_statistics_deriv_holdr_q(mlp_q->weights);

        // epoch
        shuffle_database_q(train_data_q);
        for(int batch_end = opt_q->batch_size; batch_end <= train_data_q->size; batch_end += opt_q->batch_size) {
            // compute batch gradient
            set_to_zero_deriv_holdr_q(g_q);
            for(int record = batch_end-opt_q->batch_size; record < batch_end; record++) {
                backpropagation_mult_perc_q(mlp_q, a_q, d_q, get_input_database_q(train_data_q, record), get_output_database_q(train_data_q, record));
                update_statistics_activ_holdr_q(a_q);
                update_statistics_deriv_holdr_q(d_q);
                for(int i = 0; i < g_q->number_of_weights; i++) {
                    g_q->data[i] = sub_q(g_q->data[i], d_q->data[i]);
                }
            }
            // update
            for(int i = 0; i < mlp_q->weights->number_of_weights; i++) {
                mlp_q->weights->data[i] = add_q(mlp_q->weights->data[i], apx_mul_q(opt_q->alpha_q, g_q->data[i]));
            }
            update_statistics_deriv_holdr_q(mlp_q->weights);
            iteration++;
        }

        // evaluate
        evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &error_q, &misses);
        if(opt_q->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a_q->p_stat[0], sqrt(a_q->p_stat[1]/a_q->number_of_updates), a_q->p_stat[2], a_q->p_stat[3],
                d_q->p_stat[0], sqrt(d_q->p_stat[1]/d_q->number_of_updates), d_q->p_stat[2], d_q->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                q_to_double(error_q), ((double) misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
                sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
            );
            fflush(act_der_file);
            fflush(evaluations_file);
        }
        if(error_q < min_valid_error_q) {
            consecutive = 0;
            copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
            min_valid_error_q = error_q;
            min_valid_misses = misses;
        } else {
            consecutive++;
        }
        //printf("%6ld %4d %3d %f %f %f\n", iteration, epochs, consecutive, q_to_double(min_valid_error_q), q_to_double(error_q), ((double) misses)/valid_data_q->size);
        epochs++;
    }
    opt_q->iterations = iteration;
    opt_q->epochs = epochs;
    opt_q->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt_q->multiplications = mult_counter_q;
    copy_weights_mult_perc_q(opt_mlp_q, mlp_q);
    if(opt_q->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
    }
    dealloc_activ_holdr_q(a_q);
    dealloc_deriv_holdr_q(d_q);
    dealloc_deriv_holdr_q(g_q);
    dealloc_mult_perc_q(opt_mlp_q);
}

struct optim_q *alloc_gd_optim_q(int batch_size, int consecutive_allowed, long learning_rate_q) {
    struct optim_q *opt_q = (struct optim_q*) malloc(sizeof(struct optim_q));
    opt_q->optimizer_q = gd_optim_q;
    opt_q->batch_size = batch_size;
    opt_q->consecutive_allowed = consecutive_allowed;
    opt_q->alpha_q = learning_rate_q;
    opt_q->write_to_files = 0;
    return opt_q;
}

void adam_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q) {
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *d_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *g_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *m_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *v_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct mult_perc_q *opt_mlp_q = alloc_mult_perc_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    set_to_zero_deriv_holdr_q(m_q);
    set_to_zero_deriv_holdr_q(v_q);
    mult_counter_q = 0L;
    long min_valid_error_q;
    int min_valid_misses;
    evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &min_valid_error_q, &min_valid_misses);
    copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    FILE *betas_file = 0;
    if(opt_q->write_to_files) {
        char mode[] = "w";
        sprintf(opt_q->path + opt_q->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt_q->path, mode);
        fprintf(parameters_file, "adam\n");
        fprintf(parameters_file, "batch size: %d\n", opt_q->batch_size);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt_q->consecutive_allowed);
        fprintf(parameters_file, "alpha: %e\n", q_to_double(opt_q->alpha_q));
        fprintf(parameters_file, "beta1: %e\n", q_to_double(opt_q->beta_q));
        fprintf(parameters_file, "beta2: %e\n", q_to_double(opt_q->gamma_q));
        fprintf(parameters_file, "epsilon: %e\n", q_to_double(opt_q->delta_q));
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp_q->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp_q->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp_q->weights->neurons_per_layer[i]);
        }
        fprintf(parameters_file, "tabulated points: %d\n", tab_logistic_function_q->size);
        fclose(parameters_file);
        sprintf(opt_q->path + opt_q->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt_q->path, mode);
        reset_statistics_deriv_holdr_q(mlp_q->weights);
        update_statistics_deriv_holdr_q(mlp_q->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            q_to_double(min_valid_error_q), ((double) min_valid_misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
            sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
        );
        sprintf(opt_q->path + opt_q->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt_q->path, mode);
        sprintf(opt_q->path + opt_q->path_offset, "/betas.txt"); betas_file = fopen(opt_q->path, mode);
        fprintf(betas_file, "%e %e\n", q_to_double(opt_q->beta_q), q_to_double(opt_q->gamma_q));
    }
    int consecutive = 0;
    long error_q;
    int misses;
    long beta1_q = opt_q->beta_q;
    long beta2_q = opt_q->gamma_q;
    clock_t start_time = clock();

    long iteration = 0;
    int epochs = 0;

    while(consecutive < opt_q->consecutive_allowed) {

        reset_statistics_activ_holdr_q(a_q);
        reset_statistics_deriv_holdr_q(d_q);
        reset_statistics_deriv_holdr_q(mlp_q->weights);

        // epoch
        shuffle_database_q(train_data_q);
        for(int batch_end = opt_q->batch_size; batch_end <= train_data_q->size; batch_end += opt_q->batch_size) {
            // compute batch gradient
            set_to_zero_deriv_holdr_q(g_q);
            for(int record = batch_end - opt_q->batch_size; record < batch_end; record++) {
                backpropagation_mult_perc_q(mlp_q, a_q, d_q, get_input_database_q(train_data_q, record), get_output_database_q(train_data_q, record));
                if(opt_q->write_to_files) {
                    update_statistics_activ_holdr_q(a_q);
                    update_statistics_deriv_holdr_q(d_q);
                }
                for(int i = 0; i < d_q->number_of_weights; i++) {
                    g_q->data[i] = add_q(g_q->data[i], d_q->data[i]);
                }
            }

            // update
            long alpha_q = apx_mul_q(opt_q->alpha_q, nr_div_q(sqrt_q(sub_q(const_one, beta2_q)), sub_q(const_one, beta1_q)));
            for(int i = 0; i < mlp_q->weights->number_of_weights; i++) {
                m_q->data[i] = add_q(apx_mul_q(opt_q->beta_q, m_q->data[i]), apx_mul_q(sub_q(const_one, opt_q->beta_q), g_q->data[i]));
                v_q->data[i] = add_q(apx_mul_q(opt_q->gamma_q, v_q->data[i]), apx_mul_q(sub_q(const_one, opt_q->gamma_q), apx_mul_q(g_q->data[i], g_q->data[i])));
                mlp_q->weights->data[i] = sub_q(mlp_q->weights->data[i], apx_mul_q(alpha_q, nr_div_q(m_q->data[i], add_q(sqrt_q(v_q->data[i]), opt_q->delta_q))));
            }
            beta1_q = apx_mul_q(beta1_q, opt_q->beta_q);
            beta2_q = apx_mul_q(beta2_q, opt_q->gamma_q);
            update_statistics_deriv_holdr_q(mlp_q->weights);
            iteration++;
        }

        // evaluate
        evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &error_q, &misses);
        if(opt_q->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a_q->p_stat[0], sqrt(a_q->p_stat[1]/a_q->number_of_updates), a_q->p_stat[2], a_q->p_stat[3],
                d_q->p_stat[0], sqrt(d_q->p_stat[1]/d_q->number_of_updates), d_q->p_stat[2], d_q->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                q_to_double(error_q), ((double) misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
                sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
            );
            fprintf(betas_file, "%e %e\n", q_to_double(beta1_q), q_to_double(beta2_q));
            fflush(act_der_file);
            fflush(evaluations_file);
            fflush(betas_file);
        }
        if(error_q < min_valid_error_q) {
            copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
            min_valid_error_q = error_q;
            min_valid_misses = misses;
            consecutive = 0;
        } else {
            consecutive++;
        }
        //printf("%6ld %2d %3d %f %f %f\n", iteration, epochs, consecutive, q_to_double(min_valid_error_q), q_to_double(error_q), ((double) misses)/valid_data_q->size);
        epochs++;
    }
    opt_q->iterations = iteration;
    opt_q->epochs = epochs;
    opt_q->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt_q->multiplications = mult_counter_q;
    copy_weights_mult_perc_q(opt_mlp_q, mlp_q);
    if(opt_q->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
        fclose(betas_file);
    }
    dealloc_activ_holdr_q(a_q);
    dealloc_deriv_holdr_q(d_q);
    dealloc_deriv_holdr_q(g_q);
    dealloc_deriv_holdr_q(m_q);
    dealloc_deriv_holdr_q(v_q);
    dealloc_mult_perc_q(opt_mlp_q);
}

struct optim_q *alloc_adam_optim_q(int batch_size, int consecutive_allowed, long alpha_q, long beta1_q, long beta2_q, long epsilon_q) {
    struct optim_q *opt_q = (struct optim_q*) malloc(sizeof(struct optim_q));
    opt_q->optimizer_q = adam_optim_q;
    opt_q->batch_size = batch_size;
    opt_q->consecutive_allowed = consecutive_allowed;
    opt_q->alpha_q = alpha_q;
    opt_q->beta_q = beta1_q;
    opt_q->gamma_q = beta2_q;
    opt_q->delta_q = epsilon_q;
    opt_q->write_to_files = 0;
    return opt_q;
}

void cgm_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q) {
    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *d_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *g_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *s_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct mult_perc_q *opt_mlp_q = alloc_mult_perc_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct mult_perc_q *temp_mlp_q = alloc_mult_perc_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    mult_counter_q = 0L;
    long min_valid_error_q;
    int min_valid_misses;
    evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &min_valid_error_q, &min_valid_misses);
    copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    if(opt_q->write_to_files) {
        char mode[] = "w";
        sprintf(opt_q->path + opt_q->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt_q->path, mode);
        fprintf(parameters_file, "conjugate gradient descent\n");
        fprintf(parameters_file, "batch size: %d\n", opt_q->batch_size);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt_q->consecutive_allowed);
        fprintf(parameters_file, "b c1: %e\n", q_to_double(opt_q->alpha_q));
        fprintf(parameters_file, "b alpha: %e\n", q_to_double(opt_q->beta_q));
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp_q->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp_q->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp_q->weights->neurons_per_layer[i]);
        }
        fprintf(parameters_file, "tabulated points: %d\n", tab_logistic_function_q->size);
        fclose(parameters_file);
        sprintf(opt_q->path + opt_q->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt_q->path, mode);
        reset_statistics_deriv_holdr_q(mlp_q->weights);
        update_statistics_deriv_holdr_q(mlp_q->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            q_to_double(min_valid_error_q), ((double) min_valid_misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
            sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
        );
        sprintf(opt_q->path + opt_q->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt_q->path, mode);
    }

    int consecutive = 0;
    long error_q;
    int misses;
    clock_t start_time = clock();

    long iteration = 0;
    int epochs = 0;
    long normo_q;
    int s_reset = 1;

    while(consecutive < opt_q->consecutive_allowed) {

        reset_statistics_activ_holdr_q(a_q);
        reset_statistics_deriv_holdr_q(d_q);
        reset_statistics_deriv_holdr_q(mlp_q->weights);

        // epoch
        shuffle_database_q(train_data_q);
        for(int batch_end = opt_q->batch_size; batch_end <= train_data_q->size; batch_end += opt_q->batch_size) {
            // compute batch gradient
            set_to_zero_deriv_holdr_q(g_q);
            for(int record = batch_end-opt_q->batch_size; record < batch_end; record++) {
                backpropagation_mult_perc_q(mlp_q, a_q, d_q, get_input_database_q(train_data_q, record), get_output_database_q(train_data_q, record));
                if(opt_q->write_to_files) {
                    update_statistics_activ_holdr_q(a_q);
                    update_statistics_deriv_holdr_q(d_q);
                }
                for(int i = 0; i < g_q->number_of_weights; i++) {
                    g_q->data[i] = add_q(g_q->data[i], d_q->data[i]);
                }
            }
            // update
            if(s_reset == 1) {
                // reset conjugate direction
                normo_q = 0L;
                for(int i = 0; i < s_q->number_of_weights; i++) {
                    s_q->data[i] = -g_q->data[i];
                    normo_q = add_q(normo_q, apx_mul_q(g_q->data[i], g_q->data[i]));
                }
                if(normo_q > 0L) {
                    long t_q = backtracing_q(mlp_q, temp_mlp_q, a_q, s_q, g_q, train_data_q, batch_end-opt_q->batch_size, opt_q->batch_size, opt_q->alpha_q, opt_q->beta_q);
                    if(t_q > 0L) {
                        for(int i = 0; i < s_q->number_of_weights; i++) {
                            mlp_q->weights->data[i] = add_q(mlp_q->weights->data[i], apx_mul_q(t_q, s_q->data[i]));
                        }
                        s_reset = 0;
                    }
                    //printf("reset %e\n", q_to_double(t_q));
                } else {
                    //printf("reset\n");
                }
            } else {
                // update conjugate direction
                long norm_q = 0L;
                for(int i = 0; i < s_q->number_of_weights; i++) {
                    norm_q = add_q(norm_q, apx_mul_q(g_q->data[i], g_q->data[i]));
                }
                if(norm_q == 0L) {
                    s_reset = 1;
                    //printf("update\n");
                } else {
                    long beta_q = nr_div_q(norm_q, normo_q);
                    for(int i = 0; i < s_q->number_of_weights; i++) {
                        s_q->data[i] = sub_q(apx_mul_q(beta_q, s_q->data[i]), g_q->data[i]);
                    }
                    long t_q = backtracing_q(mlp_q, temp_mlp_q, a_q, s_q, g_q, train_data_q, batch_end-opt_q->batch_size, opt_q->batch_size, opt_q->alpha_q, opt_q->beta_q);
                    if(t_q == 0L) {
                        s_reset = 1;
                    } else {
                        for(int i = 0; i < s_q->number_of_weights; i++) {
                            mlp_q->weights->data[i] = add_q(mlp_q->weights->data[i], apx_mul_q(t_q, s_q->data[i]));
                        }
                        normo_q = norm_q;
                    }
                    //printf("update %e\n", q_to_double(t_q));
                }
            }
            update_statistics_deriv_holdr_q(mlp_q->weights);
            iteration++;

            //printf("%d %d\n", batch_end, train_data_q->size);
        }

        // evaluate
        evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &error_q, &misses);
        if(opt_q->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a_q->p_stat[0], sqrt(a_q->p_stat[1]/a_q->number_of_updates), a_q->p_stat[2], a_q->p_stat[3],
                d_q->p_stat[0], sqrt(d_q->p_stat[1]/d_q->number_of_updates), d_q->p_stat[2], d_q->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                q_to_double(error_q), ((double) misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
                sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
            );
            fflush(act_der_file);
            fflush(evaluations_file);
        }
        if(error_q < min_valid_error_q) {
            copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
            min_valid_error_q = error_q;
            min_valid_misses = misses;
            consecutive = 0;
        } else {
            consecutive++;
        }
        //printf("%6ld %2d %3d %f %f %f\n", iteration, epochs, consecutive, q_to_double(min_valid_error_q), q_to_double(error_q), ((double) misses)/valid_data_q->size);
        epochs++;
    }

    opt_q->iterations = iteration;
    opt_q->epochs = epochs;
    opt_q->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt_q->multiplications = mult_counter_q;
    copy_weights_mult_perc_q(opt_mlp_q, mlp_q);
    if(opt_q->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
    }
    dealloc_activ_holdr_q(a_q);
    dealloc_deriv_holdr_q(d_q);
    dealloc_deriv_holdr_q(g_q);
    dealloc_deriv_holdr_q(s_q);
    dealloc_mult_perc_q(opt_mlp_q);
    dealloc_mult_perc_q(temp_mlp_q);
}

struct optim_q *alloc_cgm_optim_q(int batch_size, int consecutive_allowed, long b_c1_q, long b_alpha_q) {
    struct optim_q *opt_q = (struct optim_q*) malloc(sizeof(struct optim_q));
    opt_q->optimizer_q = cgm_optim_q;
    opt_q->batch_size = batch_size;
    opt_q->consecutive_allowed = consecutive_allowed;
    opt_q->alpha_q = b_c1_q;
    opt_q->beta_q = b_alpha_q;
    opt_q->write_to_files = 0;
    return opt_q;
}

struct bfgs_mat_q *alloc_bfgs_mat_q(int number_of_weights) {
    struct bfgs_mat_q *mat_q = (struct bfgs_mat_q*) malloc(sizeof(struct bfgs_mat_q));
    mat_q->number_of_weights = number_of_weights;
    mat_q->number_of_elements = number_of_weights * number_of_weights;
    mat_q->data = (long*) malloc(sizeof(long)*(mat_q->number_of_elements));
    mat_q->p_stat = (double*) malloc(sizeof(long) * 4);
    mat_q->p_row = (long**) malloc(sizeof(long)*number_of_weights);
    for(int i = 0; i < number_of_weights; i++) {
        mat_q->p_row[i] = mat_q->data + i * number_of_weights;
    }
    return mat_q;
}

void set_identity_bfgs_mat_q(struct bfgs_mat_q *mat_q) {
    for(int i = 0; i < mat_q->number_of_weights; i++) {
        for(int j = 0; j < mat_q->number_of_weights; j++) {
            mat_q->p_row[i][j] = (i == j) ? const_one : 0L;
        }
    }
}

void dealloc_bfgs_mat_q(struct bfgs_mat_q *mat_q) {
    free(mat_q->data);
    free(mat_q->p_stat);
    free(mat_q->p_row);
    free(mat_q);
}

void print_bfgs_mat_q(struct bfgs_mat_q *mat_q) {
    for(int i = 0; i < mat_q->number_of_weights; i++) {
        for(int j = 0; j < mat_q->number_of_weights; j++) {
            printf("%11.4e ", q_to_double(mat_q->p_row[i][j]));
        }
        printf("\n");
    }
}

void update_bfgs_mat_q(struct bfgs_mat_q *src_q, long *s_q, long *y_q, long sy_q, struct bfgs_mat_q *dst_q) {

    long sy_sq_q = apx_mul_q(sy_q, sy_q);
    long yBy_q = 0L;
    long By_q[src_q->number_of_weights], yB_q[src_q->number_of_weights];
    for(int i = 0; i < src_q->number_of_weights; i++) {
        By_q[i] = 0L;
        yB_q[i] = 0L;
        for(int j = 0; j < src_q->number_of_weights; j++) {
            By_q[i] = add_q(By_q[i], apx_mul_q(src_q->p_row[i][j], y_q[j]));
            yB_q[i] = add_q(yB_q[i], apx_mul_q(src_q->p_row[j][i], y_q[j]));
        }
        yBy_q = add_q(yBy_q,  apx_mul_q(y_q[i], By_q[i]));
    }

    long sy_plus_yBy_q = add_q(sy_q, yBy_q);

    for(int i = 0; i < src_q->number_of_weights; i++) {
        for(int j = 0; j < src_q->number_of_weights; j++) {
            dst_q->p_row[i][j] = add_q(
                src_q->p_row[i][j],
                sub_q(
                    nr_div_q(
                        apx_mul_q(
                            sy_plus_yBy_q,
                            apx_mul_q(s_q[i], s_q[j])
                        ),
                        sy_sq_q
                    ),
                    nr_div_q(
                        add_q(
                            apx_mul_q(By_q[i], s_q[j]),
                            apx_mul_q(s_q[i], yB_q[j])
                        ),
                        sy_q
                    )
                )
            );
        }
    }
}

void reset_statistics_bfgs_mat_q(struct bfgs_mat_q *mat_q) {
    mat_q->number_of_updates = 0L;
    mat_q->p_stat[0] = 0L;
    mat_q->p_stat[1] = 0L;
    mat_q->p_stat[2] = sat_max_q;
    mat_q->p_stat[3] = -sat_min_q;
}

void update_statistics_bfgs_mat_q(struct bfgs_mat_q *mat_q) {
    for(int i = 0; i < mat_q->number_of_elements; i++) {
        statistics(mat_q->p_stat, ++mat_q->number_of_updates, q_to_double(mat_q->data[i]));
    }
}

void bfgs_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q) {

    struct activ_holdr_q *a_q = alloc_activ_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *d_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *g_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *gn_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *s_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct deriv_holdr_q *y_q = alloc_deriv_holdr_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct bfgs_mat_q *B_q = alloc_bfgs_mat_q(mlp_q->weights->number_of_weights);
    struct bfgs_mat_q *Bn_q = alloc_bfgs_mat_q(mlp_q->weights->number_of_weights);
    struct mult_perc_q *opt_mlp_q = alloc_mult_perc_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    struct mult_perc_q *temp_mlp_q = alloc_mult_perc_q(mlp_q->weights->number_of_layers, mlp_q->weights->neurons_per_layer);
    set_identity_bfgs_mat_q(B_q);
    mult_counter_q = 0L;

    long min_valid_error_q;
    int min_valid_misses;
    evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &min_valid_error_q, &min_valid_misses);
    copy_weights_mult_perc_q(mlp_q, opt_mlp_q);

    FILE *evaluations_file = 0;
    FILE *act_der_file = 0;
    if(opt_q->write_to_files) {
        char mode[] = "w";
        sprintf(opt_q->path + opt_q->path_offset, "/parameters.txt"); FILE *parameters_file = fopen(opt_q->path, mode);
        fprintf(parameters_file, "bfgs\n");
        fprintf(parameters_file, "batch size: %d\n", opt_q->batch_size);
        fprintf(parameters_file, "consecutive_allowed: %d\n", opt_q->consecutive_allowed);
        fprintf(parameters_file, "b c1: %e\n", q_to_double(opt_q->alpha_q));
        fprintf(parameters_file, "b amin: %e\n", q_to_double(opt_q->beta_q));
        fprintf(parameters_file, "layers: ");
        for(int i = 0; i < mlp_q->weights->number_of_layers; i++) {
            fprintf(parameters_file, i == mlp_q->weights->number_of_layers - 1 ? " %d\n" : " %d", mlp_q->weights->neurons_per_layer[i]);
        }
        fprintf(parameters_file, "tabulated points: %d\n", tab_logistic_function_q->size);
        fclose(parameters_file);
        sprintf(opt_q->path + opt_q->path_offset, "/evaluations.txt"); evaluations_file = fopen(opt_q->path, mode);
        reset_statistics_deriv_holdr_q(mlp_q->weights);
        update_statistics_deriv_holdr_q(mlp_q->weights);
        fprintf(
            evaluations_file, "%e %e %e %e %e %e\n",
            q_to_double(min_valid_error_q), ((double) min_valid_misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
            sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
        );
        sprintf(opt_q->path + opt_q->path_offset, "/act_der_file.txt"); act_der_file = fopen(opt_q->path, mode);
    }

    int consecutive = 0;
    long error_q;
    int misses;
    clock_t start_time = clock();

    int B_reset = 1;
    long iteration = 0;
    int epochs = 0;

    while(consecutive < opt_q->consecutive_allowed) {

        reset_statistics_activ_holdr_q(a_q);
        reset_statistics_deriv_holdr_q(d_q);
        reset_statistics_deriv_holdr_q(mlp_q->weights);

        // epoch
        shuffle_database_q(train_data_q);
        for(int batch_end = opt_q->batch_size; batch_end <= train_data_q->size; batch_end += opt_q->batch_size) {
            // update
            if(B_reset == 1) {
                // reset Hessian apx.
                // compute batch gradient
                set_to_zero_deriv_holdr_q(g_q);
                for(int record = 0; record < opt_q->batch_size; record++) {
                    backpropagation_mult_perc_q(mlp_q, a_q, d_q, get_input_database_q(train_data_q, record), get_output_database_q(train_data_q, record));
                    update_statistics_activ_holdr_q(a_q);
                    update_statistics_deriv_holdr_q(d_q);
                    for(int i = 0; i < d_q->number_of_weights; i++) {
                        g_q->data[i] = add_q(g_q->data[i], d_q->data[i]);
                    }
                }

                set_identity_bfgs_mat_q(B_q);
                for(int i = 0; i < s_q->number_of_weights; i++) {
                    s_q->data[i] = -g_q->data[i];
                }
                long t_q = backtracing_q(mlp_q, temp_mlp_q, a_q, s_q, g_q, train_data_q, batch_end-opt_q->batch_size, opt_q->batch_size, opt_q->alpha_q, opt_q->beta_q);
                if(t_q > 0L) {
                    for(int i = 0; i < s_q->number_of_weights; i++) {
                        s_q->data[i] = apx_mul_q(s_q->data[i], t_q);
                        mlp_q->weights->data[i] = add_q(mlp_q->weights->data[i], s_q->data[i]);
                    }
                    B_reset = 0;
                }
            } else {
                // update Hessian apx.
                // compute batch gradient
                set_to_zero_deriv_holdr_q(gn_q);
                for(int record = 0; record < opt_q->batch_size; record++) {
                    backpropagation_mult_perc_q(mlp_q, a_q, d_q, get_input_database_q(train_data_q, record), get_output_database_q(train_data_q, record));
                    update_statistics_activ_holdr_q(a_q);
                    update_statistics_deriv_holdr_q(d_q);
                    for(int i = 0; i < d_q->number_of_weights; i++) {
                        gn_q->data[i] = add_q(gn_q->data[i], d_q->data[i]);
                    }
                }

                long sy_q = 0L;
                for(int i = 0; i < s_q->number_of_weights; i++) {
                    y_q->data[i] = sub_q(gn_q->data[i], g_q->data[i]);
                    sy_q = add_q(sy_q, apx_mul_q(s_q->data[i], y_q->data[i]));
                }
                if(sy_q <= 0L) {
                    B_reset = 1;
                } else {
                    update_bfgs_mat_q(B_q, s_q->data, y_q->data, sy_q, Bn_q);
                    struct bfgs_mat_q *temp_B_q = B_q;
                    B_q = Bn_q;
                    Bn_q = temp_B_q;
                    for(int i = 0; i < s_q->number_of_weights; i++) {
                        s_q->data[i] = 0L;
                        for(int j = 0; j < s_q->number_of_weights; j++) {
                            s_q->data[i] = sub_q(s_q->data[i], apx_mul_q(B_q->p_row[i][j], gn_q->data[i]));
                        }
                    }
                    long t_q = backtracing_q(mlp_q, temp_mlp_q, a_q, s_q, g_q, train_data_q, batch_end-opt_q->batch_size, opt_q->batch_size, opt_q->alpha_q, const_one);
                    if(t_q == 0L) {
                        B_reset = 1;
                    } else {
                        for(int i = 0; i < s_q->number_of_weights; i++) {
                            s_q->data[i] = apx_mul_q(s_q->data[i], t_q);
                            mlp_q->weights->data[i] = add_q(mlp_q->weights->data[i], s_q->data[i]);
                        }
                        struct deriv_holdr_q *temp_g_q = g_q;
                        g_q = gn_q;
                        gn_q = temp_g_q;
                    }
                }   
            }
            update_statistics_deriv_holdr_q(mlp_q->weights);
            iteration++;
        }

        // evaluate
        evaluate_mult_perc_q(mlp_q, a_q, valid_data_q, &error_q, &misses);
        if(opt_q->write_to_files) {
            fprintf(
                act_der_file, "%e %e %e %e %e %e %e %e\n",
                a_q->p_stat[0], sqrt(a_q->p_stat[1]/a_q->number_of_updates), a_q->p_stat[2], a_q->p_stat[3],
                d_q->p_stat[0], sqrt(d_q->p_stat[1]/d_q->number_of_updates), d_q->p_stat[2], d_q->p_stat[3]
            );
            fprintf(
                evaluations_file, "%e %e %e %e %e %e\n",
                q_to_double(error_q), ((double) misses)/valid_data_q->size, mlp_q->weights->p_stat[0],
                sqrt(mlp_q->weights->p_stat[1]/mlp_q->weights->number_of_updates), mlp_q->weights->p_stat[2], mlp_q->weights->p_stat[3]
            );
        }
        if(error_q < min_valid_error_q) {
            copy_weights_mult_perc_q(mlp_q, opt_mlp_q);
            min_valid_error_q = error_q;
            min_valid_misses = misses;
            consecutive = 0;
        } else {
            consecutive++;
        }
        printf("%6ld %2d %3d %f %f %f\n", iteration, epochs, consecutive, q_to_double(min_valid_error_q), q_to_double(error_q), ((double) misses)/valid_data_q->size);
        epochs++;
    }

    opt_q->iterations = iteration;
    opt_q->epochs = epochs;
    opt_q->duration = ((double) (clock() - start_time)/CLOCKS_PER_SEC);
    opt_q->multiplications = mult_counter_q;
    
    copy_weights_mult_perc_q(opt_mlp_q, mlp_q);
    if(opt_q->write_to_files) {
        fclose(evaluations_file);
        fclose(act_der_file);
    }

    dealloc_activ_holdr_q(a_q);
    dealloc_deriv_holdr_q(d_q);
    dealloc_deriv_holdr_q(g_q);
    dealloc_deriv_holdr_q(gn_q);
    dealloc_deriv_holdr_q(s_q);
    dealloc_deriv_holdr_q(y_q);
    dealloc_bfgs_mat_q(B_q);
    dealloc_bfgs_mat_q(Bn_q);
    dealloc_mult_perc_q(opt_mlp_q);
    dealloc_mult_perc_q(temp_mlp_q);
}

struct optim_q *alloc_bfgs_optim_q(int batch_size, int consecutive_allowed, long b_c1_q, long b_alpha_q) {
    struct optim_q *opt_q = (struct optim_q*) malloc(sizeof(struct optim_q));
    opt_q->optimizer_q = bfgs_optim_q;
    opt_q->batch_size = batch_size;
    opt_q->consecutive_allowed = consecutive_allowed;
    opt_q->alpha_q = b_c1_q;
    opt_q->beta_q = b_alpha_q;
    opt_q->write_to_files = 0;
    return opt_q;
}
