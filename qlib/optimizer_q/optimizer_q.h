#ifndef _OPTIMIZER_Q
#define _OPTIMIZER_Q

    #include <time.h>
    #include "../database_q/database_q.h"
    #include "../mult_perc_q/mult_perc_q.h"


    struct mult_perc_q;
    struct activ_holdr_q;
    struct deriv_holdr_q;

// --------------------------------------------------------------------------------------------------------------------------------------------------

    extern long mult_counter_q;

    extern long mult_stat_counter;
    extern long mult_stat_q[];

// --------------------------------------------------------------------------------------------------------------------------------------------------

    long evaluate_ls_q(
        struct mult_perc_q *mlp_q, struct mult_perc_q *temp_mlp_q, struct activ_holdr_q *a_q, struct deriv_holdr_q *d_q, long t_q,
        struct database_q *valid_data_q,int batch_start, int batch_size
    );

    // backtracing Armijo
    long backtracing_q(
        struct mult_perc_q *mlp_q, struct mult_perc_q *temp_mlp_q, struct activ_holdr_q *a_q, struct deriv_holdr_q *dir_q, struct deriv_holdr_q *grad_q,
        struct database_q *valid_q, int batch_start, int batch_size,
        long c1_q, long alpha_q
    );

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct optim_q {
        // optimization algorithm
        void (*optimizer_q) (struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q);

        // common parameters
        int batch_size;
        int consecutive_allowed;

        // specific parameters
        long alpha_q;
        long beta_q;
        long gamma_q;
        long delta_q;

        char write_to_files;
        char *path;
        int path_offset;

        long iterations;
        int epochs;
        double duration;
        long multiplications;
    };

    // deallocate memory
    void dealloc_optim_q(struct optim_q *opt_q);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // gradient descent
    void gd_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q);
    struct optim_q *alloc_gd_optim_q(int batch_size, int consecutive_allowed, long learning_rate_q);
// --------------------------------------------------------------------------------------------------------------------------------------------------

    // adam
    void adam_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q);
    struct optim_q *alloc_adam_optim_q(int batch_size, int consecutive_allowed, long alpha_q, long beta1_q, long beta2_q, long epsilon_q);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    // cgm - Polak-Ribiere
    void cgm_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q);
    struct optim_q *alloc_cgm_optim_q(int batch_size, int consecutive_allowed, long b_c1_q, long b_alpha_q);

// --------------------------------------------------------------------------------------------------------------------------------------------------

    struct bfgs_mat_q {
        int number_of_weights;
        int number_of_elements;
        long *data;
        long **p_row;

        // statistics
        long number_of_updates;
        double *p_stat;
    };

    struct bfgs_mat_q *alloc_bfgs_mat_q(int number_of_weights);
    void set_identity_bfgs_mat_q(struct bfgs_mat_q *mat_q);
    void dealloc_bfgs_mat_q(struct bfgs_mat_q *mat_q);
    void print_bfgs_mat_q(struct bfgs_mat_q *mat_q);
    void update_bfgs_mat_q(struct bfgs_mat_q *src_q, long *s_q, long *y_q, long sy_q, struct bfgs_mat_q *dst_q);

    void reset_statistics_bfgs_mat_q(struct bfgs_mat_q *mat_q);
    void update_statistics_bfgs_mat_q(struct bfgs_mat_q *mat_q);

    // bfgs
    void bfgs_optim_q(struct mult_perc_q *mlp_q, struct database_q *train_data_q, struct database_q *valid_data_q, struct optim_q *opt_q);
    struct optim_q *alloc_bfgs_optim_q(int batch_size, int consecutive_allowed, long b_c1_q, long b_alpha);

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif