#!/bin/bash

ip=$1
fp=$2

for ((method=0; method<=2; method++))
do
    for ((seed=1000; seed<=30000; seed+=1000))
    do
        sbatch run_mnist.sh $seed 30 $method $ip $fp
        sbatch run_mnist.sh $seed 60 $method $ip $fp
        sbatch run_mnist.sh $seed 100 $method $ip $fp
    done
done
