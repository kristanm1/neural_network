#include "exprm_q.h"


void run_optimizer_q(
    int seed_count, int seeds[],
    struct mult_perc_q *const mlp_q, struct activ_holdr_q *const a_q,
    struct database_q *const train_q, struct database_q *const valid_q, struct database_q *const test_q,
    struct optim_q *const opt_q,
    FILE *file
) {
    char mse_avg[32], mse_std[32];
    char fnr_avg[32], fnr_std[32];
    int num_digits = 4;
    double test_mse[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double test_fnr[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double tot_updates[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double count_mult[] = {0.0, 0.0, 1.0/0.0, -1.0/0.0};
    double error;
    int misses;
    for(int i = 0; i < seed_count; i++) {
        srand(seeds[i]);
        init_rand_mult_perc_q(mlp_q, 0.1);        
        opt_q->optimizer_q(mlp_q, train_q, valid_q, opt_q);
        evaluate_mult_perc_q(mlp_q, a_q, test_q, &error, &misses);
        statistics(test_mse, i+1, error/test_q->size);
        statistics(test_fnr, i+1, 100.0*(((double) misses)/test_q->size));
        statistics(tot_updates, i+1, opt_q->total_updates);
        statistics(count_mult, i+1, mult_counter_q);
    }
    double_format(test_mse[0], mse_avg, num_digits);
    double_format(sqrt(test_mse[1]/(seed_count-1)), mse_std, num_digits);
    double_format(test_fnr[0], fnr_avg, num_digits);
    double_format(sqrt(test_fnr[1]/(seed_count-1)), fnr_std, num_digits);
    fprintf(file, "%s %s %s %s %.1f %.1f %.1f %.1f\n",
        mse_avg, mse_std,
        fnr_avg, fnr_std,
        tot_updates[0], sqrt(tot_updates[1]/(seed_count-1)),
        count_mult[0], sqrt(count_mult[1]/(seed_count-1))
    );
    fflush(file);
}
