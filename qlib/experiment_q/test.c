#include "exprm_q.h"


void hidden_to_layers(struct database_q *const base_q, int id, int layers[]) {
    layers[0] = base_q->in_size;
    layers[proben1_hidden_sizes[id]+1] = base_q->out_size;
    for(int i = 0; i < proben1_hidden_sizes[i]; i++) {
        layers[i+1] = proben1_hidden_layers[id][i];
    }
}

void experiment_proben1_gd(int db_id, int num_models, int models[][2]) {
    if(db_id < proben1_num_dbs) {
        char *base_path = "../../rezultati4/methods/gd", temp[256];
        int seed_count = 10, seeds[] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
        for(int i = 0; i < num_models; i++) {
            set_portions_q(models[i][0], models[i][1]);
            set_tab_func_q(50000, -32.0, 32.0);
            struct optim_q *const opt_q = alloc_gd_optim_q(
                proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
                proben1_gd_learning_rates_q[db_id]
            );
            struct database *train, *valid, *test;
            load_proben_database(proben1_db_paths[db_id], &train, &valid, &test);
            struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
            struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
            struct database_q *test_q = load_database_database_q(test); dealloc_database(test);
            int size = proben1_hidden_sizes[db_id] + 2;
            int layers[size]; hidden_to_layers(train_q, db_id, layers);
            struct mult_perc_q *const mlp_q = alloc_mult_perc_q(size, layers);
            struct activ_holdr_q *const a_q = alloc_activ_holdr_q(size, layers);
            sprintf(temp, "%s/%s/fixed_%d_%d_ilm.out", base_path, proben1_db_names[db_id], integer_portion, fractional_portion);
            FILE *file = fopen(temp, "w");
            run_optimizer_q(seed_count, seeds, mlp_q, a_q, train_q, valid_q, test_q, opt_q, file);
            fclose(file);
            dealloc_mult_perc_q(mlp_q);
            dealloc_activ_holdr_q(a_q);
            dealloc_database_q(train_q);
            dealloc_database_q(valid_q);
            dealloc_database_q(test_q);
            dealloc_optim_q(opt_q);
            free_tab_func_q();
        }
    }
}

void experiment_proben1_agd(int db_id, int num_models, int models[][2]) {
    if(db_id < proben1_num_dbs) {
        char *base_path = "../../rezultati4/methods/agd", temp[256];
        int seed_count = 10, seeds[] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
        for(int i = 0; i < num_models; i++) {
            set_portions_q(models[i][0], models[i][1]);
            set_tab_func_q(50000, -32.0, 32.0);
            struct optim_q *const opt_q = alloc_agd_optim_q(
                proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
                proben1_gss_init_step_sizes_q[db_id], proben1_gss_convr_tol_q
            );
            struct database *train, *valid, *test;
            load_proben_database(proben1_db_paths[db_id], &train, &valid, &test);
            struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
            struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
            struct database_q *test_q = load_database_database_q(test); dealloc_database(test);
            int size = proben1_hidden_sizes[db_id] + 2;
            int layers[size]; hidden_to_layers(train_q, db_id, layers);
            struct mult_perc_q *const mlp_q = alloc_mult_perc_q(size, layers);
            struct activ_holdr_q *const a_q = alloc_activ_holdr_q(size, layers);
            sprintf(temp, "%s/%s/fixed_%d_%d_ilm.out", base_path, proben1_db_names[db_id], integer_portion, fractional_portion);
            FILE *file = fopen(temp, "w");
            run_optimizer_q(seed_count, seeds, mlp_q, a_q, train_q, valid_q, test_q, opt_q, file);
            fclose(file);
            dealloc_mult_perc_q(mlp_q);
            dealloc_activ_holdr_q(a_q);
            dealloc_database_q(train_q);
            dealloc_database_q(valid_q);
            dealloc_database_q(test_q);
            dealloc_optim_q(opt_q);
            free_tab_func_q();
        }
    }
}

void experiment_proben1_adam(int db_id, int num_models, int models[][2]) {
    if(db_id < proben1_num_dbs) {
        char *base_path = "../../rezultati4/methods/adam", temp[256];
        int seed_count = 10, seeds[] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
        for(int i = 0; i < num_models; i++) {
            set_portions_q(models[i][0], models[i][1]);
            set_tab_func_q(50000, -32.0, 32.0);
            struct optim_q *const opt_q = alloc_adam_optim_q(
                proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
                proben1_adam_alphas_q[db_id], proben1_adam_beta1_q, proben1_adam_beta2_q, proben1_adam_epsilon_q
            );
            struct database *train, *valid, *test;
            load_proben_database(proben1_db_paths[db_id], &train, &valid, &test);
            struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
            struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
            struct database_q *test_q = load_database_database_q(test); dealloc_database(test);
            int size = proben1_hidden_sizes[db_id] + 2;
            int layers[size]; hidden_to_layers(train_q, db_id, layers);
            struct mult_perc_q *const mlp_q = alloc_mult_perc_q(size, layers);
            struct activ_holdr_q *const a_q = alloc_activ_holdr_q(size, layers);
            sprintf(temp, "%s/%s/fixed_%d_%d_ilm.out", base_path, proben1_db_names[db_id], integer_portion, fractional_portion);
            FILE *file = fopen(temp, "w");
            run_optimizer_q(seed_count, seeds, mlp_q, a_q, train_q, valid_q, test_q, opt_q, file);
            fclose(file);
            dealloc_mult_perc_q(mlp_q);
            dealloc_activ_holdr_q(a_q);
            dealloc_database_q(train_q);
            dealloc_database_q(valid_q);
            dealloc_database_q(test_q);
            dealloc_optim_q(opt_q);
            free_tab_func_q();
        }
    }
}

void experiment_proben1_cgm(int db_id, int num_models, int models[][2]) {
    if(db_id < proben1_num_dbs) {
        char *base_path = "../../rezultati4/methods/cgm", temp[256];
        int seed_count = 10, seeds[] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
        for(int i = 0; i < num_models; i++) {
            set_portions_q(models[i][0], models[i][1]);
            set_tab_func_q(50000, -32.0, 32.0);
            struct optim_q *const opt_q = alloc_cgm_optim_q(
                proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
                proben1_gss_init_step_sizes_q[db_id], proben1_gss_convr_tol_q
            );
            struct database *train, *valid, *test;
            load_proben_database(proben1_db_paths[db_id], &train, &valid, &test);
            struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
            struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
            struct database_q *test_q = load_database_database_q(test); dealloc_database(test);
            int size = proben1_hidden_sizes[db_id] + 2;
            int layers[size]; hidden_to_layers(train_q, db_id, layers);
            struct mult_perc_q *const mlp_q = alloc_mult_perc_q(size, layers);
            struct activ_holdr_q *const a_q = alloc_activ_holdr_q(size, layers);
            sprintf(temp, "%s/%s/fixed_%d_%d_ilm.out", base_path, proben1_db_names[db_id], integer_portion, fractional_portion);
            FILE *file = fopen(temp, "w");
            run_optimizer_q(seed_count, seeds, mlp_q, a_q, train_q, valid_q, test_q, opt_q, file);
            fclose(file);
            dealloc_mult_perc_q(mlp_q);
            dealloc_activ_holdr_q(a_q);
            dealloc_database_q(train_q);
            dealloc_database_q(valid_q);
            dealloc_database_q(test_q);
            dealloc_optim_q(opt_q);
            free_tab_func_q();
        }
    }
}

void experiment_proben1_bfgs(int db_id, int num_models, int models[][2]) {
    if(db_id < proben1_num_dbs) {
        char *base_path = "../../rezultati4/methods/bfgs", temp[256];
        int seed_count = 10, seeds[] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};
        for(int i = 0; i < num_models; i++) {
            set_portions_q(models[i][0], models[i][1]);
            set_tab_func_q(50000, -32.0, 32.0);
            struct optim_q *const opt_q = alloc_bfgs_optim_q(
                proben1_batch_size, proben1_max_updates, proben1_training_strip, proben1_max_conse_allowed,
                proben1_gss_init_step_sizes_q[db_id], proben1_gss_convr_tol_q
            );
            struct database *train, *valid, *test;
            load_proben_database(proben1_db_paths[db_id], &train, &valid, &test);
            struct database_q *train_q = load_database_database_q(train); dealloc_database(train);
            struct database_q *valid_q = load_database_database_q(valid); dealloc_database(valid);
            struct database_q *test_q = load_database_database_q(test); dealloc_database(test);
            int size = proben1_hidden_sizes[db_id] + 2;
            int layers[size]; hidden_to_layers(train_q, db_id, layers);
            struct mult_perc_q *const mlp_q = alloc_mult_perc_q(size, layers);
            struct activ_holdr_q *const a_q = alloc_activ_holdr_q(size, layers);
            sprintf(temp, "%s/%s/fixed_%d_%d_ilm.out", base_path, proben1_db_names[db_id], integer_portion, fractional_portion);
            FILE *file = fopen(temp, "w");
            run_optimizer_q(seed_count, seeds, mlp_q, a_q, train_q, valid_q, test_q, opt_q, file);
            fclose(file);
            dealloc_mult_perc_q(mlp_q);
            dealloc_activ_holdr_q(a_q);
            dealloc_database_q(train_q);
            dealloc_database_q(valid_q);
            dealloc_database_q(test_q);
            dealloc_optim_q(opt_q);
            free_tab_func_q();
        }
    }
}

int main(int argc, char *argv[]) {
    set_num_preserved_bits(52);

    int num_models = 11;
    int models[num_models][2];
    for(int i = 0; i < num_models; i++) {
        models[i][0] = 3;
        models[i][1] = 15 - i;
    }

    for(int db_id = 0; db_id < proben1_num_dbs; db_id++) {
        //experiment_proben1_gd(db_id, num_models, models);
        //experiment_proben1_agd(db_id, num_models, models);
        experiment_proben1_adam(db_id, num_models, models);
        //experiment_proben1_cgm(db_id, num_models, models);
        //experiment_proben1_bfgs(db_id, num_models, models);
        printf("DONE: %s\n", proben1_db_names[db_id]);
    }


    return 0;
}