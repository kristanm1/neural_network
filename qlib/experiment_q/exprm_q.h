#ifndef _EXPRM_Q
#define _EXPRM_Q

    #include "../optimizer_q/optimizer_q.h"
    #include "../database_q/database_q.h"
    #include "../mult_perc_q/mult_perc_q.h"


// --------------------------------------------------------------------------------------------------------------------------------------------------

    // seeds: pseudo random generator seeds <- random initializations of mlp
    // mlp_q: multilayer perceptron
    // databases_q: training, validation and test set
    // opt_q: optimizer
    void run_optimizer_q(
        int seed_count, int seeds[],
        struct mult_perc_q *const mlp_q, struct activ_holdr_q *const a_q,
        struct database_q *const train_q, struct database_q *const valid_q, struct database_q *const test_q,
        struct optim_q *const opt_q,
        FILE *file
    );

// --------------------------------------------------------------------------------------------------------------------------------------------------


#endif